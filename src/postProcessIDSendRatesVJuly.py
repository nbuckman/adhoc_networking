#!/usr/bin/env python
import cPickle as pickle
import numpy as np
import matplotlib.pyplot as plt
import time
import argparse
import pysftp
import sys

def cleanData2(rcvdP,sentP):
    #Desc:  Imports pickled data and outputs cleaner msg data
    #Input(2):  Pickle File Names of rcvd and sent results
    #Ouput(3):  Dict of rcvdMsgs, Dict of sentMsgs, MissionSpecs
    # sentP = pickle.load(open(sentPFilename,'rb'))
    # rcvdP = pickle.load(open(rcvdPFilename,'rb')), also delete any data from before 2 seconds
    missionSpecs = rcvdP[0].missionSpecs
    agentList = missionSpecs.activeAgents
    #agentList = [ord(i) for i in missionSpecs.activeAgents]  #NOTE!! For some reason this is returning x01
    #missionSpecs.activeAgents = agentList
    rcvdMsgs = {}
    sentMsgs = {}
    newRcvdMsgs = {}
    newSentMsgs = {}
    for result in rcvdP:
        rcvdMsgs[result.agentID] = result.rcvdMsgs
    for result in sentP:
        sentMsgs[result.agentID] = result.sentMsgs

    epochT = min([min([10**15]+[msg.time for msg in sentMsgs[j]]) for j in agentList]+[min([10**15]+[msg.time for msg in rcvdMsgs[j]]) for j in agentList])
    startTime = epochT + 2 # 2 is chosen since
    for agentID in agentList:
        for msg in sentMsgs[agentID]:
            msg.time -= startTime
        for msg in rcvdMsgs[agentID]:
            msg.time -= startTime
        newRcvdMsgs[agentID] = [msg for msg in rcvdMsgs[agentID] if msg.time>=0]
        newSentMsgs[agentID] = [msg for msg in sentMsgs[agentID] if msg.time>=0]
    return (newRcvdMsgs, newSentMsgs, missionSpecs)


def msgStats(rcvdMsgs,sentMsgs,missionSpecs,consecMsgs=0):
    #Desc:  Compute MsgStas
    #Input(3):  Cleans msg data
    #Ouput(2):  msgDelay[i] = list of msgDelays for agent i, msgP[i][j]: prob msg goes from i->j
    msgDelay = {}
    meanMsgDelay = dict()
    stdMsgDelay = dict()
    msgDelayStats = {}
    msgP = {}
    infoP = {}
    infoDelay = {}
    rawData = {}
    pEdge = {}
    tEdge = {}
    agentList = missionSpecs.activeAgents
    msgRcvdByIFromJNum = {}
    for i in agentList:
        tempDict = {}
        for j in agentList:
            tempDict[j]={}
        for msg in rcvdMsgs[i]:
            tempDict[msg.sender][msg.msgNum] = msg
        msgRcvdByIFromJNum[i] = tempDict
    for i in agentList:
        if len(sentMsgs[i])==0:
            continue
        msgDelay[i] = dict()
        meanMsgDelay[i] = dict()
        stdMsgDelay[i] = dict()
        msgDelayStats[i] = dict()
        msgP[i] = dict()
        infoP[i] = dict()
        infoDelay[i] = dict()
        rawData[i]=dict()
        rawDelayData = []
        for j in agentList:
            numMsgsReceived = 0
            numMsgsSent = 0
            currentJDelay = []
            infoJDelay = []
            infoSent = 0
            infoRcvd = 0
            if i==j:
                msgP[i][j] = 0
                msgDelay[i][j]=0
                meanMsgDelay[i][j] = None
                stdMsgDelay[i][j] = None
                infoP[i][j]=0
                infoDelay[i][j]=0
            else:
                consecutiveRcvdMsgCounter = 0
                for msg_i in sentMsgs[i]:
                    numMsgsSent+=1
                    # if numMsgsSent % consecMsgs == 0: #We have "newInfo"
                    #     infoSent += 1
                    #     consecutiveRcvdMsgCounter = 0
                    #     currentInfoReceivedTimes = []
                    if msg_i.msgNum in msgRcvdByIFromJNum[j][i].keys():
                        msg_j=msgRcvdByIFromJNum[j][i][msg_i.msgNum]
                        currentJDelay += [msg_j.time - msg_i.time]
                        #currentInfoReceivedTimes += [msg_j.time]
                        numMsgsReceived += 1
                        #consecutiveRcvdMsgCounter += 1
                        #rawDelayData += [msg_j.time - msg_i.time]
                    else:
                        pass
                        #consecutiveRcvdMsgCounter = 0
                        #rawDelayData += [None]
                    # if consecutiveRcvdMsgCounter == consecMsgs:
                    #     infoRcvd += 1
                    #     infoJDelay += [max(currentInfoReceivedTimes) - msg_i.time]
                # if numMsgsSent%consecMsgs!=0:
                #     infoSent -= 1  #The last info was not complete to start off with
                meanMsgDelay[i][j] = np.mean(currentJDelay)
                stdMsgDelay[i][j] = np.std(currentJDelay)
                msgP[i][j] = float(numMsgsReceived)/numMsgsSent
                # #msgDelayStats[i][j] = (np.mean(currentJDelay),np.std(currentJDelay))
                # msgP[i][j] = float(numMsgsReceived)/numMsgsSent
                # infoP[i][j] = float(infoRcvd)/infoSent
                # infoDelay[i][j] = infoJDelay
                #print i, "->", j
                #print "MSG_P", msgP[i][j]
                #print "INFO_P", infoP[i][j]
                rawData[i][j] = currentJDelay
    return (meanMsgDelay, stdMsgDelay, msgP,rawData)


def beliefStats(rcvdMsgs,sentMsgs,missionSpecs):
    #ID Beliefs:  List of current beliefs over time
    #First Belief: List of first time received info
    #Belief Delays:  List of (maxID, delay)
    #numBeliefSuccessRcvd:  Number of Beliefs successfully received from 1
    #pBelief:  Problability belief rcvd by agent J
    #delyaBeliefStats:  mean delay, std delay
    agentList = missionSpecs.activeAgents
    IDBeliefs = {}
    FirstBelief = {}
    for i in agentList:
        IDBeliefs[i] = sorted(rcvdMsgs[i]+sentMsgs[i],key=lambda x: x.time)
    for i in agentList:
        currentBelief = 0
        FirstBelief[i] = []
        for belief in IDBeliefs[i]:
            if belief.runningMax>currentBelief:
                currentBelief = belief.runningMax
                FirstBelief[i].append(belief)
    maxID = FirstBelief[1][-1].runningMax
    beliefDelays = {2:[],3:[],4:[],5:[],6:[]}
    numBeliefSuccessRcvd = {2:0,3:0,4:0,5:0,6:0}
    for belief in FirstBelief[1]:
        time1 = belief.time
        maxi = belief.runningMax
        for j in agentList[1:]:
            try:
                timej = next((x.time for x in FirstBelief[j] if x.runningMax==maxi))
                beliefDelays[j] += [(maxi,timej-time1)]
                numBeliefSuccessRcvd[j] += 1
            except StopIteration:
                pass
    pBelief = {}
    delayBeliefStats = {}
    numBeliefsSent = len(FirstBelief[1])
    print  "Agent J | (meanDelay (s), stdDelay) |  numRcvd | p Rcvd"
    for j in agentList[1:]:
        pBelief[j] = numBeliefSuccessRcvd[j]/float(numBeliefsSent)
        if len(beliefDelays[j])>0:
            delayBeliefStats[j] = (np.mean(zip(*beliefDelays[j])[1]),np.std(zip(*beliefDelays[j])[1]))
        else:
            delayBeliefStats[j] = (-.0001,-.00001)
        print "# %1d     | %.2f     | %.2f  | %8d   | %.2f" % (j,delayBeliefStats[j][0],delayBeliefStats[j][1], numBeliefSuccessRcvd[j], pBelief[j])
    return (IDBeliefs, FirstBelief, beliefDelays, numBeliefSuccessRcvd, pBelief, delayBeliefStats)

def addTrialResults(trialIJresult,experimentIJResults,msgLength,sSpeed):
    if msgLength not in experimentIJResults.keys():
        experimentIJResults[msgLength] = {}
    if sSpeed not in experimentIJResults[msgLength].keys():
        experimentIJResults[msgLength][sSpeed] = {}

    if type(trialIJresult.itervalues().next()) == dict: #THIS IS IJ dict
        for sender in trialIJresult.keys():
            if sender not in experimentIJResults[msgLength][sSpeed].keys():
                experimentIJResults[msgLength][sSpeed][sender] = {}
            for receiver in trialIJresult[sender].keys():
                if receiver not in experimentIJResults[msgLength][sSpeed][sender].keys():
                    experimentIJResults[msgLength][sSpeed][sender][receiver] = []
                experimentIJResults[msgLength][sSpeed][sender][receiver] += [trialIJresult[sender][receiver]]
    else: #This info is all sent from 1, so just indexed by receiver
        for receiver in trialIJresult.keys():
            if receiver not in experimentIJResults[msgLength][sSpeed].keys():
                experimentIJResults[msgLength][sSpeed][receiver] = []
            experimentIJResults[msgLength][sSpeed][receiver] += [trialIJresult[receiver]]
    return experimentIJResults

def printDict(d):
    agentList = sorted(d.keys())
    rows = [['%0.2f' % d[i][j] for j in agentList] for i in agentList]
    i = 1
    header = ['    ']+[str(j)+'r |' for j in agentList]
    print ' '.join(header)
    for row in rows:
        row = [str(i)+'s|'] + row
        i+=1
        print ' '.join(row)

##########################################################################################
##########################################################################################
download = False
if len(sys.argv)<2:
    print "Provide name of experiment directory to create"
elif len(sys.argv)<3:
    experimentName = sys.argv[1]
    missionLower = -1
    missionUpper = 9999999999999
else:
    #Make Directory with Experiment Name
    experimentName = sys.argv[1]
    missionLower = int(sys.argv[2])
    missionUpper = int(sys.argv[3])

IP_ADDRESSES = [
'128.31.38.137',
'128.31.37.149',
'128.31.34.234',
'128.31.35.224',
'128.31.35.47',
'128.31.37.77',
]

piResultsDirectory = '/home/pi/catkin_ws/src/adhoc_networking/results/'
if download:
    for IP in IP_ADDRESSES:
        print "Getting Results Data FROM:", IP
        with pysftp.Connection(IP, username='pi', password='uavswarm') as sftp:
            with sftp.cd(piResultsDirectory):             # temporarily chdir to public
                lDir = noamResultsDirectory + experimentName
                rDir = piResultsDirectory + experimentName
                print "DOWNLOADING"
                sftp.get_d(rDir,lDir)  # upload file to public/ on remote



noamResultsDirectory = '/home/nbuckman/DropboxMIT/ACL/results/'
missionDirectory = noamResultsDirectory + experimentName
entireMissionSpecsFileName = missionDirectory + "experimentSpecs.p"
entireMissionSpecs = pickle.load(open(entireMissionSpecsFileName,'rb'))

msgPResults = {}     #Speed, UpdateID, Sender, Receiver,  [Reults_trial_i]
msgDelayResults={} #Speed, UpdateID, Sender, Receiver, [DelayResults]
beliefPResults={} #Speed, UpdateID, Receiver, P
beliefDelayResults={} #Speed, UpdateID, Receiver, P
firstBeliefResults = {}

missedFiles = []
ctPResults = {}
ctDelayResults = {}
infoPResults={}
infoDelayResults={}
rawDataResults={}



maximumMsgLength = max([msp.msgLength for msp in entireMissionSpecs])
print "Maximum MSG Length is", maximumMsgLength
for currentMissionSpecs in entireMissionSpecs:
    currentMissionID = currentMissionSpecs.missionID
    if currentMissionID<missionLower or currentMissionID>missionUpper:
        print currentMissionID, "Out of range"
        continue
    print currentMissionID
    activeAgents = currentMissionSpecs.activeAgents
    sSpeed = currentMissionSpecs.sendRates[0]
    idSpeed = currentMissionSpecs.changeIDRates[0]
    msgLength = currentMissionSpecs.msgLength
    if msgLength == 500:
        continue
    connectionTest = False
    if len(set(currentMissionSpecs.sendRates)) > 1:
        connectionTest = True
    if not connectionTest:
        sentData = []
        rcvdData = []
        #GET THE RESULTS FILE FROM EACH AGENT
        for agentIDint in activeAgents:
            changeIDHz = currentMissionSpecs.changeIDRates[agentIDint-1]
            sendHz = currentMissionSpecs.sendRates[agentIDint-1] # 10hz
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            sentFileName = missionDirectory + "PreProcessedData/" + str(agentIDint) + 'sent' + '%02d' % currentMissionID + '.p'
            rcvdFileName = missionDirectory + "PreProcessedData/" + str(agentIDint) + 'rcvd' + '%02d' % currentMissionID + '.p'
            try:
                #print sentFileName
                sentP = pickle.load(open(sentFileName,'rb'))
                #print rcvdFileName
                rcvdP = pickle.load(open(rcvdFileName,'rb'))
                sentData += [sentP]
                rcvdData += [rcvdP]
            except IOError as e:
                print "Can't find", sentFileName
                missedFiles += [(agentIDint,currentMissionID)]
            except ValueError as v:
                print "There is an issue", sentFileName
                missedFiles += [(agentIDint,currentMissionID)]
        if len(sentData)!=len(activeAgents):
            sAgents = set([s.agentID for s in sentData])
            print "Didn't get sent data from", list(set(activeAgents) - sAgents)
            continue
        if len(rcvdData)!=len(activeAgents):
            rAgents = set([r.agentID for r in rcvdData])
            print "Didn't get rcvd data from", list(set(activeAgents) - rAgents)
            continue
        print "UpdateSpeed",idSpeed, "SendSpeed", sSpeed, "Hz", msgLength, "Bytes"
        numMsgsPerInfo = maximumMsgLength / msgLength #
        #Override
        numMsgsPerInfo = 1
        print "Required", numMsgsPerInfo, "MSGS per INFO"
        (rcvdMsgs, sentMsgs, missionSpecs) = cleanData2(rcvdData,sentData)
        (meanMsgDelayt, stdMsgDelayt, msgPt,rawDatat) = msgStats(rcvdMsgs,sentMsgs,missionSpecs,numMsgsPerInfo)
        msgPResults = addTrialResults(msgPt,msgPResults,msgLength,sSpeed)
        msgDelayResults = addTrialResults(meanMsgDelayt,msgDelayResults,msgLength,sSpeed)
        rawDataResults = addTrialResults(rawDatat,rawDataResults,msgLength,sSpeed)

        #infoPResults = addTrialResults(infoPt,infoPResults,idSpeed,sSpeed)
        #infoDelayResults = addTrialResults(infoDelayt,infoDelayResults,idSpeed,sSpeed)

        printDict(msgPt)
        #printDict(infoPt)

        (IDBeliefs, FirstBelief, beliefDelays, beliefSuccess, pAgents, delayAgents) = beliefStats(rcvdMsgs,sentMsgs,missionSpecs)
        firstBeliefResults = addTrialResults(FirstBelief,firstBeliefResults,idSpeed,sSpeed)
        beliefPResults = addTrialResults(pAgents,beliefPResults,idSpeed,sSpeed)
        beliefDelayResults = addTrialResults(delayAgents,beliefDelayResults,idSpeed,sSpeed)
    else:
        print currentMissionSpecs.sendRates
        ctSender = np.nonzero(currentMissionSpecs.sendRates)[0][0]
        msgSize = currentMissionSpecs.msgLength
        sSpeed = currentMissionSpecs.sendRates[ctSender]
        print "CT: SendSpeed,", sSpeed, "Sender", ctSender+1

        sentData = []
        rcvdData = []
        #GET THE RESULTS FILE FROM EACH AGENT
        for agentIDint in activeAgents:
            changeIDHz = currentMissionSpecs.changeIDRates[agentIDint-1]
            sendHz = currentMissionSpecs.sendRates[agentIDint-1] # 10hz
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            sentFileName = missionDirectory + "PreProcessedData/" + str(agentIDint) + 'sent' + '%02d' % currentMissionID + '.p'
            rcvdFileName = missionDirectory + "PreProcessedData/" + str(agentIDint) + 'rcvd' + '%02d' % currentMissionID + '.p'

            #sentFileName = missionDirectory + "PreProcessedData/" + 'sent' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'
            #rcvdFileName = missionDirectory + "PreProcessedData/" + 'rcvd' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'
            try:
                #print "CT"+sentFileName
                sentP = pickle.load(open(sentFileName,'rb'))
                #print "CT"+rcvdFileName
                rcvdP = pickle.load(open(rcvdFileName,'rb'))
                sentData += [sentP]
                rcvdData += [rcvdP]
            except IOError as e:
                print "Can't find", sentFileName
            except EOFError as ef:
                print "EOFERROR!", sentFileName
            except ValueError as v:
                print "There is an issue", sentFileName
        if len(sentData)!=len(activeAgents):
            print "Didn't get sent data from", list(set(activeAgents) - set(sentData))
            continue
        if len(rcvdData)!=len(activeAgents):
            print "Didn't get rcvd data from", list(set(activeAgents) - set(rcvdData))
            continue

        (rcvdMsgs, sentMsgs, missionSpecs) = cleanData2(rcvdData,sentData)
        numMsgsPerInfo = 0
        (meanMsgDelayt, stdMsgDelayt, msgPt,rawDatat) = msgStats(rcvdMsgs,sentMsgs,missionSpecs,numMsgsPerInfo)
        # print "CT"
        print msgPt
        #sender = msgPt.keys()[0] #0
        #receivers = msgPt[sender].keys()
        #OnlySenderResults
        #msgDelayt = meanMsgDelayt[ctSender+1]
        #msgPt = msgPt[ctSender+1]
        ctDelayResults = addTrialResults(meanMsgDelayt,ctDelayResults,msgSize,sSpeed)
        ctPResults = addTrialResults(msgPt,ctPResults,msgSize,sSpeed)

############################################ SAVE THE DATA
postProcessFileName = missionDirectory + "postProcessMsg" +  '.p'
pickle.dump([msgPResults,msgDelayResults], open(postProcessFileName, "wb" ) )

postProcessFileName = missionDirectory + "postProcessBelief" + '.p'
pickle.dump([beliefPResults,beliefDelayResults],open(postProcessFileName, "wb" ) )

#postProcessFileName = missionDirectory + "firstBeliefs"  + '.p'
#pickle.dump(firstBeliefResults,open(postProcessFileName, "wb" ) )

if len(ctPResults.keys())>0:
    postProcessCTFileName = missionDirectory + "postProcessCT" + '.p'
    pickle.dump([ctPResults,ctDelayResults], open(postProcessCTFileName, "wb" ) )


print "Missed Files, (agentID, missionID)"
print missedFiles
print missionDirectory
