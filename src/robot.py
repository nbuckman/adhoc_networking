# from wifiObjects import Wifi
import cbba, wifi  #These are custom modules by NoamBuckman
import udpMessage as udp
#from udpMessage import udpMessage, CBBA, TaskInfo, CBBA1
import multiprocessing
import numpy as np
import copy as copy
import rospy, time #These are all due to pi implementation
import kinematics as kn
import time
class Robot:
    def __init__(self,agentID,real=True):
        self.agentID = agentID
        self.real = real
        self.wifi = None
        self.planner = None
        self.sensor = None
        self.pose = kn.Pose(None,None)
        self.msgNum = 0
        self.simTime = 0
        #Info Passed from Sim
        self.speed = 1.0
        self.missionID = 9

    def move(self):
        raise NotImplementedError

    def plan(self):
        raise NotImplementedError

    def sense(self):
        raise NotImplementedError

    def broadcast(self,msg):
        raise NotImplementedError

    def listen(self,msg):
        raise NotImplementedError


class WifiRobot(Robot):
    def __init__(self,agentID,real=True):
        Robot.__init__(self,agentID,real)
        self.wifi = wifi.Wifi(real)  #NOT COMP WITH SIMULATOR

        #These are the actual messages
        self.rcvdMessages = []
        self.sentMessages = []

        #Used for plotting
        self.newHistorySentCodes = []
        self.newHistoryRcvCodes = []
        self.currentRcvCode = -9
        self.currentSendCode = -9
        self.phase1 = True
    def broadcast(self,msg):
        self.wifi.csma_send(msg)

class CbbaRobot(WifiRobot):
    def __init__(self,agentID,L_maxTasks=10,nTasks=30,nRobots=3,real=True):
        WifiRobot.__init__(self,agentID,real)

        self.nTasks = nTasks
        self.nRobots = nRobots
        self.L_maxTasks = L_maxTasks
        self.cbbaManager = cbba.CBBAManager(self.agentID,self.L_maxTasks,self.defaultPathValueF,self.nTasks,self.nRobots)

        self.addedReward=True

        self.nextBBTime = 0.1 + np.random.random()/100

        self.p_path_history = []

        self.r_id = -1
        self.t_D = -1

        self.t_star = None
        self.t_star_y = 0
        self.t_star_z = -1
        self.t_star_t = 0

        self.t_star_p = []

    def taskDiscovery(self,taskInfo):
        task = taskInfo.task
        distance = self.pose.dist(task.pose)
        tau_d = distance  #insert speed of robot

        t_discovery = self.time
        t_s = task.timeOut - tau_d  #This is the abs time that agent will execute
        tau_c = t_s - t_discovery

        if tau_c <=0:
            print "NO TIME TO CONSENSUS DISCOVERY AGENT WILL SERVICE!"

        tau_id = tau_c/3.0
        avgCommRound = .01 #Seconds
        r_id = int(np.floor(tau_id /avgCommRound))

        self.discoveryRobot = self.agentID
        print "Ag", self.discoveryRobot, "Discovered Task", task.id
        print "Dist:", distance
        print "T_Discovery", t_discovery
        print "T_s", t_s
        print "Deadline:", task.timeOut
        print "tau_c", tau_c

        #UPDATE SELF FOR NEW TASK
        self.L_maxTasks += 1
        self.cbbaManager.L_maxTasks = self.L_maxTasks
        self.cbbaManager.updateTaskLists(task)

        #BEST BID OF DISCOVERY TASK
        j=task.id
        [c_ij, task_n] = self.cbbaManager.max_marginal_value(j,self.cbbaManager.p_path,self.cbbaManager.tasksList,t_s) #7
        print "BID", c_ij

        task.r_id = tau_id #TODO: ADD TO TASK INFO MSG
        task.t_D = t_discovery
        self.t_star = task
        self.nextBBTime = self.time + 0.01 + np.random.random()/1000 #WHY?
        self.nextBBTime = round(self.nextBBTime,5)
        taskInfo.task = self.t_star
        taskInfo.sender = self.agentID
        udpMsg = taskInfo.toUDPMessage()
        for i in range(5):
            udpMsgLength = self.wifi.broadcastRobot(udpMsg)

        if self.cbbaManager.globalResetNum>-1:
            self.t_star=None
        print udpMsg.msg
        return udpMsg


    def loop(self):
        rcvdMessage = self.wifi.listenRobot()
        self.cbbaManager.cbbaLocalTime = self.time

        #MANAGE INCOMING MSGS
        if rcvdMessage != None:
            incomingMsg = udp.udpMessage()
            incomingCBBABid = incomingMsg.udpToInfoObject(rcvdMessage)
            if type(incomingCBBABid) == udp.TaskInfo:
                if self.t_star == None:
                    newTask = incomingCBBABid.task
                    if newTask.id not in set([t.id for t in self.cbbaManager.tasksList]):
                        print "AG %i, t: %0.4f:  New Time Sensitive Task" % (self.agentID, self.time)
                        #UPDATE SELF FOR NEW TASK
                        self.L_maxTasks += 1
                        self.cbbaManager.L_maxTasks = self.L_maxTasks
                        newTask = incomingCBBABid.task
                        self.cbbaManager.updateTaskLists(newTask)
                        self.t_star = newTask
                        self.nextBBTime = self.time + 0.01 + np.random.random()/1000
                        self.nextBBTime = round(self.nextBBTime,5)
                        if self.cbbaManager.globalResetNum > -1:
                            #CBBA RESET:  Don't go into TAA loop, rebroadcast taskinfo
                            self.t_star = None
                            print "Rebroadcasting TaskInfo"
                            incomingCBBABid.sender = self.agentID
                            udpMsg = incomingCBBABid.toUDPMessage()
                            for i in range(15):
                                udpMsgLength = self.wifi.broadcastRobot(udpMsg)
                        elif self.time > self.t_star.t_D + self.t_star.r_id:
                            print "AG %i, t: %0.4f:  I'm Not In Subteam / Beyond Radius" % (self.agentID, self.time)
                            self.t_star_y = -99
                else:
                    #Already saw the task
                    return
            else:
                if self.t_star != None:
                    if incomingCBBABid.taskID == self.t_star.id:
                        #print "AG %i, t: %0.4f:  rcvd bid from AG %i" % (self.agentID, self.time,incomingCBBABid.sender)
                        if self.t_star_y < incomingCBBABid.y_winbid:
                            self.t_star_y = incomingCBBABid.y_winbid
                            self.t_star_z = incomingCBBABid.z_winagent
                            self.t_star_t = incomingCBBABid.t_timestamp
                    else:
                        pass #Don't don anything with these bids
                else:
                    self.cbbaManager.listenerCBBA1([incomingCBBABid],10)

        if self.t_star != None: #T Task discovered and bidding
            if abs(self.time - self.nextBBTime)<0.0001:
                self.nextBBTime += 0.01 + np.random.random()/1000
                self.nextBBTime = round(self.nextBBTime,5)

                task = self.t_star
                j = task.id
                tau_id = task.r_id
                t_D = task.t_D
                t_s = t_D + 3*tau_id

                if self.time < task.t_D + tau_id: #Phase 1:  Make my own bid and broadcast task info
                    #print "AG %i, t: %0.4f:  Phase 1 Broadcast T Info" % (self.agentID, self.time)
                    [c_ij, task_n] = self.cbbaManager.max_marginal_value(j,self.cbbaManager.p_path,self.cbbaManager.tasksList,t_s) #7
                    self.t_star_p = copy.deepcopy(self.cbbaManager.p_path)
                    self.t_star_p.insert(task_n,task.id)
                    if c_ij > self.t_star_y:
                        self.t_star_y =  c_ij
                        self.t_star_z = self.agentID
                        self.t_star_t = self.time
                    #Broadcast Task Information  (TODO: THIS WILL NEED SOME EDITING)
                    tInfo = udp.TaskInfo(None,9,self.agentID,99,task)
                    tInfo.sender = self.agentID
                    tInfo.msgNum = 99
                    udpMsg = tInfo.toUDPMessage()
                    udpMsgLength = self.wifi.broadcastRobot(udpMsg)

                #Phase 2: FIRST CHECK FOR INCOMING BIDS ON T*
                if rcvdMessage != None:
                    incomingMsg = udp.udpMessage()
                    incomingCBBABid = incomingMsg.udpToInfoObject(rcvdMessage)
                    if type(incomingCBBABid) != udp.TaskInfo:
                        if incomingCBBABid.taskID == self.t_star.id:
                            #print "AG %i, t: %0.4f:  rcvd bid from AG %i" % (self.agentID, self.time,incomingCBBABid.sender)
                            if self.t_star_y < incomingCBBABid.y_winbid:
                                self.t_star_y = incomingCBBABid.y_winbid
                                self.t_star_z = incomingCBBABid.z_winagent
                                self.t_star_t = incomingCBBABid.t_timestamp

                #COMMUNICATE MY BID
                #print "AG %i, t: %0.4f:  Phase 1-2: Bidding Consensus" % (self.agentID, self.time)
                cbbaMsg = udp.CBBA()
                cbbaMsg.sender = self.agentID
                cbbaMsg.missionID = self.missionID
                cbbaMsg.msgNum = self.msgNum
                cbbaMsg.taskID = j
                cbbaMsg.z_winagent = self.t_star_z
                cbbaMsg.y_winbid = self.t_star_y
                cbbaMsg.t_timestamp = self.time
                cbbaUDPMsg = cbbaMsg.toUDPMessage()
                udpMsgLength = self.wifi.broadcastRobot(cbbaUDPMsg)

                if self.time > task.t_D + 3*tau_id: #END OF TASK BIDDING
                    if self.t_star_z == self.agentID:
                        print "AG %i, t: %0.4f:  Phase 3: Winning / Execution: I AM THE WINNER!" % (self.agentID, self.time)
                        self.cbbaManager.p_path = copy.deepcopy(self.t_star_p)
                        self.cbbaManager.b_bundle.append(task.id)
                        for j in self.cbbaManager.p_path:
                            self.cbbaManager.y_winbids[j] = 999999999
                            self.cbbaManager.z_winagents[j] = self.agentID
                        print "EXECUTING PATH",self.cbbaManager.p_path, "BUNDLE",self.cbbaManager.b_bundle
                        for tid in self.cbbaManager.p_path:
                            cbbaMsg = udp.CBBA()
                            cbbaMsg.sender = self.agentID
                            cbbaMsg.missionID = self.missionID #?
                            cbbaMsg.msgNum = self.msgNum
                            cbbaMsg.taskID = tid
                            cbbaMsg.z_winagent = self.agentID
                            cbbaMsg.y_winbid = 9999999
                            cbbaMsg.t_timestamp = self.time
                            cbbaUDPMsg = cbbaMsg.toUDPMessage()
                            udpMsgLength = self.wifi.broadcastRobot(cbbaUDPMsg)
                        self.nextBBTime = 0 #NO MORE BIDDING FOR WINNER
                    else:
                        print "AG %i, t: %0.4f:  Phase 3: Winning / Execution: I AM THE LOSER!" % (self.agentID, self.time)
                        self.nextBBTime = self.time + 0.1 + np.random.random()/100
                        self.nextBBTime = round(self.nextBBTime,5)
                    self.t_star = None
                    self.t_star_y= 0
                    self.t_star_z = -1
                    self.L_maxTasks-=1
                    self.cbbaManager.L_maxTasks = self.L_maxTasks

                return
        if abs(self.time - self.nextBBTime)<0.0001:
            self.nextBBTime += 0.1 + np.random.random()/100
            self.nextBBTime = round(self.nextBBTime,5)
            self.cbbaManager.bundleBuilder2(10)
            self.p_path_history.append((self.time,self.cbbaManager.p_path))
            if len(self.cbbaManager.toBroadcastBuffer)>0:
                tasksToSend = self.cbbaManager.toBroadcastBuffer.pop(0)
                for taskBundle in tasksToSend:
                    cbbaMsg = udp.CBBA()
                    cbbaMsg.sender = self.agentID
                    cbbaMsg.missionID = self.missionID #?
                    cbbaMsg.msgNum = self.msgNum
                    cbbaMsg.taskID = taskBundle.taskID
                    cbbaMsg.z_winagent = taskBundle.z_winagent
                    cbbaMsg.y_winbid = taskBundle.y_winbid
                    cbbaMsg.t_timestamp = taskBundle.t_timestamp
                    cbbaUDPMsg = cbbaMsg.toUDPMessage()
                    #print "RobLoop", self.agentID, "Sent Task", cbbaMsg.taskID, cbbaUDPMsg, cbbaMsg.t_timestamp

                    #cbbaUDPMsg.sendOnFakeUDP()

                    #IS IT BROADCAST ROBOT OR UDP BROADCAST?
                    udpMsgLength = self.wifi.broadcastRobot(cbbaUDPMsg)
                    if cbbaMsg.taskID>100:
                        print "TID HUGE:",cbbaMsg.taskID
                        print udpMsgLength
                    #self.wifi.outgoingMsgQueue.append(cbbaUDPMsg)

                    self.msgNum += 1

        #Output msgs to incomingbuffer 1 and incoming buffer 2


    def defaultPathValueF(self,path,tasksList=None,startTime=0.0):
            #Value function is based on distance, reward,ldiscount,ability
            if tasksList==None:
                tasksList = self.cbbaManager.tasksList
            # if self.originalPose == None:
            #     self.originalPose = copy.deepcopy(self.pose)
            pathValue = 0
            totalTime = startTime
            previous_pose = self.pose
            for j in path:
                task = tasksList[j]
                totalTime = totalTime + previous_pose.dist(task.pose)/self.speed
                tReward = task.reward * (task.ldiscount ** totalTime)                #print task.id, task.timeOut, time
                if task.timeOut != None:
                    if task.timeOut<totalTime:
                        tReward = 0
                    else:
                        if self.addedReward==True:
                            tReward += 1
                pathValue += tReward

                previous_pose = task.pose
                # totalTime += task.tau_service
            return pathValue

    def defaultPathValueFOrigin(self,path,tasksList=None,startTime=0.0):
            #Value function is based on distance, reward,ldiscount,ability
            if tasksList==None:
                tasksList = self.cbbaManager.tasksList
            pathValue = 0
            totalTime = startTime
            if self.originalPose == None:
                self.originalPose = copy.deepcopy(self.pose)
            previous_pose = self.originalPose
            for j in path:
                task = tasksList[j]
                totalTime = totalTime + previous_pose.dist(task.pose)/self.speed
                tReward = task.reward * (task.ldiscount ** totalTime)                #print task.id, task.timeOut, time
                if task.timeOut != None:
                    if task.timeOut<totalTime:
                        tReward = 0
                    else:
                        if self.addedReward==True:
                            tReward += 1
                pathValue += tReward

                # previous_pose = task.pose
                # totalTime += task.tau_service
            return pathValue


    def getArrivalTimes(self,path=None,tasksList=None,startTime=0.0):
        if tasksList==None:
            tasksList = self.cbbaManager.tasksList
        if path==None:
            path = self.cbbaManager.p_path
        pathValue = 0
        totalTime = startTime
        previous_pose = self.originalPose
        arrivalTimes = []
        for j in path:
            task = tasksList[j]
            totalTime = totalTime + previous_pose.dist(task.pose)/self.speed
            arrivalTimes += [totalTime]
            previous_pose = task.pose
            # totalTime += task.tau_service
        timeLeft = [t-self.currentTime() for t in arrivalTimes]
        return timeLeft

    def getReplanTimes(self,b_bundle=None,y_winbids=None):
        if b_bundle==None:
            b_bundle = self.cbbaManager.b_bundle
        if y_winbids==None:
            y_winbids = self.cbbaManager.y_winbids
        tau_bundleBuild = 0.07 #s from hardware
        sya = list(np.argsort(y_winbids))
        tau_rp = []
        for j in b_bundle:
            n = sya.index(j)
            tau_rp += [(n+1+2*self.D+1)*tau_bundleBuild]
        return tau_rp





    # def rij(self,task):
    #     return self.taskAbilityArray[task.id]*task.reward

    def broadcastBundle(self):
        if len(self.cbbaManager.listOfOutgoingBundles)>0:
            (z_winagents,y_winbids,t_timestamps) = self.cbbaManager.listOfOutgoingBundles.pop()
            cbbaMsg = udp.CBBA()
            cbbaMsg.sender = self.agentID
            cbbaMsg.missionID = self.missionID #?
            cbbaMsg.msgNum = self.msgNum
            cbbaMsg.numTasks = self.nTasks
            cbbaMsg.z_winagents = z_winagents
            cbbaMsg.y_winbids = y_winbids
            cbbaMsg.t_timestamps = t_timestamps
            cbbaUDPMsg = cbbaMsg.toUDPMessage()
            udpMsg = self.wifi.broadcastRobot(cbbaUDPMsg)
            self.msgNum += 1
            print udpMsg.msg
        else:
            print "No outgoing bundles"

############################################################################
