import numpy as np
import rospy,socket
import multiprocessing
import time
class Wifi():
    def __init__(self,realWifi=True):
        self.realWifi = realWifi

        #Public to Robot
        if self.realWifi:
            self.outgoingMsgQueue = multiprocessing.Queue()
            self.incomingMsgQueue = multiprocessing.Queue()
        else:
            self.incomingMsgQueue =  FakeQueue()
            self.outgoingMsgQueue = FakeQueue()
        #NOAM BIG change and will break sim
        if self.realWifi:
            self.wifiThreadFlag = multiprocessing.Event()

            #################################
            #Setting up the UDP Socket
            adhoc=True
            simNeighbors = False  #Set this true if you want to enforce neighbors
            if adhoc:
                UDP_IP = "10.10.3.255"
            else:
                UDP_IP = "128.31.39.255"
            UDP_PORT = 5005
            self.sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
            self.sock.bind((UDP_IP,UDP_PORT))
            self.sock.setblocking(0) #Don't block  <TODO:  Try BLOCKING!
            print "UDP target IP:", UDP_IP, "port:", UDP_PORT
            ####################################
            self.pR = multiprocessing.Process(target=self.Receiving,args=(self.wifiThreadFlag,))
            # t2.daemon = True
            self.pR.daemon = True
            self.pS =  multiprocessing.Process(target=self.Sending,args=(self.wifiThreadFlag,))
            self.pS.daemon = True
            self.pR.start()
            self.pS.start()
            print "RT", self.pR.pid
            print "ST", self.pS.pid


            #
            #
            # tR = threading.Thread(target=self.Receiving,args=(self.wifiThreadFlag,))
            # # t2.daemon = True
            # tR.daemon = True
            # tS = threading.Thread(target=self.Sending,args=(self.wifiThreadFlag,))
            # tS.daemon = True
            # tR.start()
            # tS.start()




        self.msgNumCounter = 0
        #Wifi State
        self.incomingTransmissionsBuffer = [] #Buffer that holds all incoming "bits"
        self.isBroadcasting = False
        self.broadcastingEndTime = 0
        self.successfulTransmissions = []
        self.droppedMsgs = 0

        #CSMA State:
        self.backoff = False
        self.mediumFree = True
        self.backOffCounter = 0
        self.backOffAmount = 0
        self.backoffQueue = []

        #Private
        #Characteristics of 802.11g
        self.commRate = 5*10**5 #bytes/seconds = 1mbps

        self.slotTime = 10e-6 #s => 9 us
        #self.sifs = .01 #mS
        self.sifs = 10e-6 #s  => 10 us
        self.difs = 2*self.slotTime + self.sifs #(28) uS

        self.difsN = int(self.difs/self.slotTime) #(This is hard coded but should change)
        self.cwMin = 15
        self.difsCounter = 0

        self.numAttemptedBroadcast = 0
        self.numReceivedBroadcast = 0

    #Public only to Robot
    def broadcastRobot(self,msg):
        self.msgNumCounter += 1
        self.outgoingMsgQueue.put(msg)
        self.numAttemptedBroadcast +=1
        #msg.sendOnFakeUDP(self)
        return msg

    def listenRobot(self):
        #Public to Robot#
        #Returns (timeRcvd,Message)
        if self.incomingMsgQueue.empty():
            return None
        else:
            data = self.incomingMsgQueue.get()
            # print "Incoming Queue Size:", self.incomingMsgQueue.qsize()
            self.numReceivedBroadcast += 1
            #print data
            return data


    def Sending(self,mission_stop):
        """
        This thread physically sends messages over serial
        """
        experimentRunning = True
        #1)  Unpack the mission specs  (make this explicit what is being sent over)
        sendHz = 200 # 10hz


        #2)  Set our send rate
        if sendHz>0.000001:
            sendRate = rospy.Rate(sendHz)
        spinRateS = rospy.Rate(100)
        sentMsgs = []
        sentBytes = []

        sendingMaxHistory = []
        msgNum = 0
        #Might need:  nonlocal outgoingBundlesQueue
        #MAIN SENDING SCRIPT RUN ON THE PI
        if sendHz<0.00001:
            sendRate = spinRateS
        UDP_IP="10.10.3.255"
        UDP_PORT = 5005
        while not mission_stop.is_set():
            #This could be done with try/catch on Queue
            if not self.outgoingMsgQueue.empty():
                cbbaOutgoingMsg = self.outgoingMsgQueue.get()
                try:
                    sent = cbbaOutgoingMsg.sendOnUDP(self.sock,UDP_IP,UDP_PORT)
                except socket.error as e:
                    print "Send: Msg Not Sent", e
                except socket.timeout:
                    print "Send Timeout"
                msgNum +=1
            sendRate.sleep()
        self.sock.close()
        del self.outgoingMsgQueue

    def Receiving(self,mission_stop):
        # neighbors = {
        #     1:{2,3},
        #     2:{1,3},
        #     3:{1,2,4,5},
        #     4:{1,2,3,5},
        #     5:{3,4,6},
        #     6:{4,5,6}
        # }
        # simNeighbors = False #HOW DO WE CHANGE THIS?  MISISON SPECS?
        # agentIDint = 3  #THESE ARENT BEING TAKEN FROM GLOBAL
        # if simNeighbors and not adhoc:
        #     myNeighbors = neighbors[agentIDint]
        # else:
        #     myNeighbors = {1,2,3,4,5,6} - {agentIDint}

        experimentRunning = True

        #1)  Unpack the mission specs
        rcvHz = 300

        #2)  Set variables
        receiveRate = rospy.Rate(rcvHz)
        spinRateR = rospy.Rate(300)
        rcvdMsgs = []
        rcvdBytes = []

        #3) Wait for start time  (add the randomizer here)
        lastMessage = 0
        lastSocketError = 0
        msgLength = 1024
        while not mission_stop.is_set():
            try:
                (data, addr) = self.sock.recvfrom(msgLength)  #TODO: Maybe this should just be blocked
                self.incomingMsgQueue.put(data)
            except socket.error as e:
                #print "RCV Error:", e
                pass
            except socket.timeout:
                print "RCV Timeout"
                pass
            receiveRate.sleep()
        leftOverMsgCounter = 0
        for i in range(20):
            try:
                self.sock.recvfrom(msgLength)
                leftOverMsgCounter+=1
            except socket.error as e:
                pass
        # print "Closing Socket.  Leftover Messages", leftOverMsgCounter
        # print "Leftover Queue Size", self.incomingMsgQueue.qsize()
        self.sock.close()
        del self.incomingMsgQueue


                #5)  Save results to some mission results

import collections
class FakeQueue:
    def __init__(self):
        self.q = collections.deque()
    def put(self,item):
        self.q.append(item)
    def get(self):
        return self.q.popleft()
    def empty(self):
        return len(self.q)==0
    def qsize(self):
        return len(self.q)





################################OLD Stuff#Public only to Simulator
# def csma_send(self,simulator,robot,printStatements=False):
#     #This needs to be run every time step
#     #Maintenance on broadcasting variable
#     if simulator.time >= self.broadcastingEndTime:
#         self.isBroadcasting= False
#     if self.isBroadcasting == True: #Already Broadcasting
#         return 9
#     if len(self.outgoingMsgQueue) == 0: #Nothing to Transmit
#         return 1
#     #1.  Carrier Sense
#     else:
#         self.mediumFree = self.carrierSense(simulator,robot) #Done using simulation
#         #2a.  Medium busy: Defer transmission
#         if not self.mediumFree: #Medium Busy
#             self.difsCounter = 0
#             if self.backOffCounter == 0:
#                 self.backOffCounter = np.random.randint(0,self.cwMin+1)*self.slotTime/simulator.deltaT #Set exponential backoff
#             return 2
#         #2b.  Medium is free, countdown DIFS and Backoff
#         else: #Entering DIFS
#             if self.difsCounter < self.difsN:
#                 self.difsCounter +=1
#                 return 3
#             else: #Entering Backoff
#                 if self.backOffCounter > 0:
#                     self.backOffCounter -=1
#                     return 4
#                 else: #Entering Transmission
#                     msg = self.outgoingMsgQueue.pop(0)
#                     msg.endTime = simulator.time + msg.getMsgLength()/self.commRate
#                     self.broadcastingEndTime = msg.endTime
#                     for otherRobot in simulator.commNeighbors[robot.agentID]:
#                         otherRobot.wifi.incomingTransmissionsBuffer.append(msg)
#                     self.isBroadcasting = True
#                     self.difsCounter = 0
#                     self.mediumFree = True
#                     self.successfulTransmissions.append(msg)
#                     return 9
#
# def carrierSense(self,simulator,robot): #Check neighbors for broadcasting
#     if simulator.commType == "Ideal":
#         return True
#     for otherRobot in simulator.commNeighbors[robot.agentID]:
#         if otherRobot.wifi.isBroadcasting:
#             return False
#     return True
#
# #Public only to simulator
# def csma_rcv(self,simulator,robot):
#     #Called by sim every time step to simulate the csma rcv
#     #Return: Status Code
#     if simulator.commType == "Ideal":
#         msg = None
#         if len(self.incomingTransmissionsBuffer)==0:
#             return 2
#         newIncomingTransmissionsBuffer = []
#         for i in range(len(self.incomingTransmissionsBuffer)):
#             msg = self.incomingTransmissionsBuffer[i]
#             if msg.endTime <= simulator.time: #Msg received
#                 self.newIncomingTransmissionsBuffer.append((simulator.time,msg))
#             else:
#                 newIncomingTransmissionsBuffer.append(msg)
#         self.incomingTransmissionsBuffer = newIncomingTransmissionsBuffer
#         if msg!=None:
#             return 9 #Successful transmission
#         else:
#             return 3 #Receiving Transmission
#
#     if self.isBroadcasting and simulator.commType != "Ideal": #Agent Broadcasting, so IncomingTrans dropped
#         self.droppedMsgs += len(self.incomingTransmissionsBuffer)
#         self.incomingTransmissionsBuffer = []
#         return 0
#     else:
#         for message in self.incomingTransmissionsBuffer: #Remove messages from robots out of range
#             if message.sender not in simulator.commNeighbors[robot.agentID]:
#                 self.incomingTransmissionsBuffer.remove(message)
#         numBroadcasting = 0  #Count Number of MSGS being received
#         for otherRobot in simulator.commNeighbors[robot.agentID]:
#             if otherRobot.wifi.isBroadcasting:
#                 numBroadcasting+=1
#         if numBroadcasting >= 2 and simulator.commType != "Ideal": #Message Collission on Receiving
#             self.droppedMsgs += len(self.incomingTransmissionsBuffer)
#             self.incomingTransmissionsBuffer = []
#             return 1
#         if len(self.incomingTransmissionsBuffer) == 0: #No incoming Msgs
#             return 2
#         msg = None
#         for i in range(len(self.incomingTransmissionsBuffer)):
#             if self.incomingTransmissionsBuffer[i].endTime <= simulator.time:
#                 msg = self.incomingTransmissionsBuffer.pop(i)
#                 self.incomingMsgQueue.append((simulator.time,msg))
#         if msg!=None:
#             return 9 #Successful transmission
#         else:
#             return 3  #Receiving Transmission
#
