#!/usr/bin/env python
import cPickle as pickle
import numpy as np
import matplotlib.pyplot as plt
import time
import argparse
import pysftp
import sys


def cleanData2(rcvdP,sentP):
    #Desc:  Imports pickled data and outputs cleaner msg data
    #Input(2):  Pickle File Names of rcvd and sent results
    #Ouput(3):  Dict of rcvdMsgs, Dict of sentMsgs, MissionSpecs
    # sentP = pickle.load(open(sentPFilename,'rb'))
    # rcvdP = pickle.load(open(rcvdPFilename,'rb'))
    missionSpecs = rcvdP[0].missionSpecs
    agentList = missionSpecs.activeAgents
    #agentList = [ord(i) for i in missionSpecs.activeAgents]  #NOTE!! For some reason this is returning x01
    #missionSpecs.activeAgents = agentList
    rcvdMsgs = {}
    sentMsgs = {}
    for result in rcvdP:
        rcvdMsgs[result.agentID] = result.rcvdMsgs
    for result in sentP:
        sentMsgs[result.agentID] = result.sentMsgs

    epochT = min([min([10**15]+[msg.time for msg in sentMsgs[j]]) for j in agentList]+[min([10**15]+[msg.time for msg in rcvdMsgs[j]]) for j in agentList])
    for agentID in agentList:
        for msg in sentMsgs[agentID]:
            msg.time -= epochT
        for msg in rcvdMsgs[agentID]:
            msg.time -= epochT
    return (rcvdMsgs, sentMsgs, missionSpecs)


    #STEP 2
def msgStats(rcvdMsgs,sentMsgs,missionSpecs):
    #Desc:  Compute MsgStas
    #Input(3):  Cleans msg data
    #Ouput(2):  msgDelay[i] = list of msgDelays for agent i, msgP[i][j]: prob msg goes from i->j
    msgDelay = {}
    msgDelayStats = {}
    msgP = {}
    pEdge = {}
    tEdge = {}
    agentList = missionSpecs.activeAgents
    msgRcvdByIFromJNum = {}
    for i in agentList:
        tempDict = {}
        for j in agentList:
            tempDict[j]={}
        for msg in rcvdMsgs[i]:
            tempDict[msg.sender][msg.msgNum] = msg
        msgRcvdByIFromJNum[i] = tempDict
    for i in agentList:
        if len(sentMsgs[i])==0:
            continue
        msgDelay[i] = dict()
        msgDelayStats[i] = dict()
        msgP[i] = dict()
        for j in agentList:
            numMsgsReceived = 0
            numMsgsSent = 0
            currentJDelay = []
            if i==j:
                msgP[i][j] = 0
                pass
            else:
                for msg_i in sentMsgs[i]:
                    numMsgsSent+=1
                    if msg_i.msgNum in msgRcvdByIFromJNum[j][i].keys():
                        msg_j=msgRcvdByIFromJNum[j][i][msg_i.msgNum]
                        currentJDelay += [msg_j.time - msg_i.time]
                        numMsgsReceived += 1
                msgDelay[i][j] = currentJDelay
                msgDelayStats[i][j] = (np.mean(currentJDelay),np.std(currentJDelay))
                msgP[i][j] = float(numMsgsReceived)/numMsgsSent
    return (msgDelay, msgDelayStats, msgP)


    #STEP 3
def beliefStats(rcvdMsgs,sentMsgs,missionSpecs):
    #Description #This Method Creates all Relevant Statics for a given expeirment
    ##Input:  rcvdMsgs,sentMsgs,missionSpecs
    ##Output: First belief, allbelief, delays, success in belief space # Look into asynchron
    #Mission Specs
    agentList = missionSpecs.activeAgents
    IDBeliefs = {}
    FirstBelief = {}
    for i in agentList:
        IDBeliefs[i] = sorted(rcvdMsgs[i]+sentMsgs[i],key=lambda x: x.time)
    for i in agentList:
        currentBelief = 0
        fBeliefs = []
        for belief in IDBeliefs[i]:
            if belief.runningMax>currentBelief:
                currentBelief = belief.runningMax
                fBeliefs.append(belief)
        FirstBelief[i] = fBeliefs

    maxID = FirstBelief[1][-1].runningMax
    beliefDelays = {2:[],3:[],4:[],5:[],6:[]}
    beliefSuccess = {2:0,3:0,4:0,5:0,6:0}
    for belief in FirstBelief[1]:
        time1 = belief.time
        maxi = belief.runningMax
        for j in agentList[1:]:
            try:
                timej = next((x.time for x in FirstBelief[j] if x.runningMax==maxi))
                beliefDelays[j] += [(maxi,timej-time1)]
                beliefSuccess[j] += 1
            except StopIteration:
                pass
    pAgents = {}
    delayAgents = {}
    for j in agentList[1:]:
        #print "# SUCCESS:",beliefSuccess[j]
        pAgents[j] = beliefSuccess[j]*1.0/maxID
        if len(beliefDelays[j])>0:
            delayAgents[j] = (np.mean(zip(*beliefDelays[j])[1]),np.std(zip(*beliefDelays[j])[1]))
        else:
            delayAgents[j] = None
        #print "Agent", j, "p", pAgents[j], "dt", delayAgents[j]
    #print FirstBelief
    return (IDBeliefs, FirstBelief, beliefDelays, beliefSuccess, pAgents, delayAgents)

download = False
if len(sys.argv)<2:
    print "Provide name of experiment directory to create"
elif len(sys.argv)<3:
    experimentName = sys.argv[1]
else:
    #Make Directory with Experiment Name
    experimentName = sys.argv[1]
    if sys.argv[2] == "False":
        download = False

# parser = argparse.ArgumentParser(description='Process data.')
# parser.add_argument('integer', metavar='N', type=int, nargs='?', help='an integer for the accumulator')
# args = parser.parse_args()
# msgLength = args.integer


IP_ADDRESSES = [
'128.31.38.137',
'128.31.36.238',
'128.31.34.234',
'128.31.35.224',
'128.31.35.47',
'128.31.37.77',
]
#'128.31.36.238',
noamResultsDirectory = '/home/nbuckman/DropboxMIT/ACL/catkin_ws/src/adhoc_networking/results/'
piResultsDirectory = '/home/pi/catkin_ws/src/adhoc_networking/results/'
if download:
    for IP in IP_ADDRESSES:
        print "Getting Results Data FROM:", IP
        with pysftp.Connection(IP, username='pi', password='uavswarm') as sftp:
            with sftp.cd(piResultsDirectory):             # temporarily chdir to public
                lDir = noamResultsDirectory + experimentName
                rDir = piResultsDirectory + experimentName
                print "DOWNLOADING"
                sftp.get_d(rDir,lDir)  # upload file to public/ on remote
                #sftp.get('remote_file')         # get a remote file


missionDirectory = noamResultsDirectory + experimentName
entireMissionSpecsFileName = missionDirectory + "experimentSpecs.p"
entireMissionSpecs = pickle.load(open(entireMissionSpecsFileName,'rb'))

msgPResults = {}     #Msg Size, Speed, Sender, Receiver,  [Reults_trial_i]
msgDelayResults={} #Msg Size, Speed, Sender, Receiver, [DelayResults]
beliefPResults={} #Msg Size, Speed, Receiver, P
beliefDelayResults={} #Msg Size, Speed, Receiver, P
missedFiles = []
ctPResults = {}
ctDelayResults = {}

for currentMissionSpecs in entireMissionSpecs:

    currentMissionID = currentMissionSpecs.missionID
    activeAgents = currentMissionSpecs.activeAgents
    sSpeed = currentMissionSpecs.sendRates[0]
    msgLength = currentMissionSpecs.msgLength
    # if msgLength!=1000:  ###THIS MUST BE CHANGED!!!
    #     continue
    connectionTest = False
    if len(set(currentMissionSpecs.sendRates)) > 1:
        connectionTest = True

    #initialize some entries:
    if not connectionTest:
        if msgLength not in msgPResults.keys():
            msgPResults[msgLength]={}     #Speed, Sender, Receiver,  [Reults_trial_i]
            msgDelayResults[msgLength]={} #Speed, Sender, Receiver, [DelayResults]
            beliefPResults[msgLength]={} #Speed, Receiver, P
            beliefDelayResults[msgLength]={} #Speed, Receiver, P

        if sSpeed not in msgPResults[msgLength].keys():
            msgPResults[msgLength][sSpeed]={}     #Speed, Sender, Receiver,  [Reults_trial_i]
            msgDelayResults[msgLength][sSpeed]={} #Speed, Sender, Receiver, [DelayResults]
            beliefPResults[msgLength][sSpeed]={}  #Speed, Receiver, P
            beliefDelayResults[msgLength][sSpeed]={}  #Speed, Receiver, P

        msgP=[]
        msgDelay= []
        beliefP= []
        beliefDelay= []

        sentData = []
        rcvdData = []
        #GET THE RESULTS FILE FROM EACH AGENT
        for agentIDint in activeAgents:
            changeIDHz = currentMissionSpecs.changeIDRates[agentIDint-1]
            sendHz = currentMissionSpecs.sendRates[agentIDint-1] # 10hz
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added
            endTime = startTime + duration

            sentFileName = missionDirectory + "PreProcessedData/" + 'sent' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'
            rcvdFileName = missionDirectory + "PreProcessedData/" + 'rcvd' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'
            try:
                print sentFileName
                sentP = pickle.load(open(sentFileName,'rb'))
                print rcvdFileName
                rcvdP = pickle.load(open(rcvdFileName,'rb'))
                sentData += [sentP]
                rcvdData += [rcvdP]
            except IOError as e:
                print "Can't find", sentFileName
                missedFiles += [(agentIDint,currentMissionID)]
            except ValueError as v:
                print "There is an issue", sentFileName
        if len(sentData)!=len(activeAgents):
            sAgents = set([s.agentID for s in sentData])
            print "Didn't get sent data from", list(set(activeAgents) - sAgents)
            continue
        if len(rcvdData)!=len(activeAgents):
            rAgents = set([r.agentID for r in rcvdData])
            print "Didn't get rcvd data from", list(set(activeAgents) - rAgents)
            continue

        (rcvdMsgs, sentMsgs, missionSpecs) = cleanData2(rcvdData,sentData)
        #print [msg.runningMax for msg in sentMsgs[1]]
        (msgDelayt, msgDelayStatst, msgPt) = msgStats(rcvdMsgs,sentMsgs,missionSpecs)
        (IDBeliefs, FirstBelief, beliefDelays, beliefSuccess, pAgents, delayAgents) = beliefStats(rcvdMsgs,sentMsgs,missionSpecs)

        for sender in msgPt.keys():
            if sender not in msgPResults[msgLength][sSpeed].keys():
                msgPResults[msgLength][sSpeed][sender] = {}
            for receiver in msgPt[sender].keys():
                if receiver not in msgPResults[msgLength][sSpeed][sender].keys():
                    msgPResults[msgLength][sSpeed][sender][receiver] = []
                msgPResults[msgLength][sSpeed][sender][receiver] += [msgPt[sender][receiver]]

        for sender in msgDelayt.keys():
            if sender not in msgDelayResults[msgLength][sSpeed].keys():
                msgDelayResults[msgLength][sSpeed][sender] = {}
            for receiver in msgDelayt[sender].keys():
                if receiver not in msgDelayResults[msgLength][sSpeed][sender].keys():
                    msgDelayResults[msgLength][sSpeed][sender][receiver] = []
                msgDelayResults[msgLength][sSpeed][sender][receiver] += [msgDelayt[sender][receiver]]

        for receiver in pAgents.keys():
            if receiver not in beliefPResults[msgLength][sSpeed].keys():
                beliefPResults[msgLength][sSpeed][receiver] = []
            beliefPResults[msgLength][sSpeed][receiver] += [pAgents[receiver]]

        for receiver in delayAgents.keys():
            if receiver not in beliefDelayResults[msgLength][sSpeed].keys():
                beliefDelayResults[msgLength][sSpeed][receiver] = []
            beliefDelayResults[msgLength][sSpeed][receiver] += [delayAgents[receiver]]
    else:
        ctSender = np.nonzero(currentMissionSpecs.sendRates)[0][0]
        sSpeed = currentMissionSpecs.sendRates[ctSender]
        print "CT: SendSpeed,", sSpeed, "Sender", ctSender

        if msgLength not in ctPResults.keys():
            ctPResults[msgLength]={}     #Speed, Sender, Receiver,  [Reults_trial_i]
            ctDelayResults[msgLength]={} #Speed, Sender, Receiver, [DelayResults]

        if sSpeed not in ctPResults[msgLength].keys():
            ctPResults[msgLength][sSpeed]={}     #Speed, Sender, Receiver,  [Reults_trial_i]
            ctDelayResults[msgLength][sSpeed]={} #Speed, Sender, Receiver, [DelayResults]

        msgP=[]
        msgDelay= []

        sentData = []
        rcvdData = []
        #GET THE RESULTS FILE FROM EACH AGENT
        for agentIDint in activeAgents:
            changeIDHz = currentMissionSpecs.changeIDRates[agentIDint-1]
            sendHz = currentMissionSpecs.sendRates[agentIDint-1] # 10hz
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added
            endTime = startTime + duration

            sentFileName = missionDirectory + "PreProcessedData/" + 'sent' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'
            rcvdFileName = missionDirectory + "PreProcessedData/" + 'rcvd' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'
            try:
                print "CT"+sentFileName
                sentP = pickle.load(open(sentFileName,'rb'))
                print "CT"+rcvdFileName
                rcvdP = pickle.load(open(rcvdFileName,'rb'))
                sentData += [sentP]
                rcvdData += [rcvdP]
            except IOError as e:
                print "Can't find", sentFileName
            except ValueError as v:
                print "There is an issue", sentFileName
        if len(sentData)!=len(activeAgents):
            print "Didn't get sent data from", list(set(activeAgents) - set(sentData))
            continue
        if len(rcvdData)!=len(activeAgents):
            print "Didn't get rcvd data from", list(set(activeAgents) - set(rcvdData))
            continue

        (rcvdMsgs, sentMsgs, missionSpecs) = cleanData2(rcvdData,sentData)
        #print [msg.runningMax for msg in sentMsgs[1]]
        (msgDelayt, msgDelayStatst, msgPt) = msgStats(rcvdMsgs,sentMsgs,missionSpecs)
        sender = msgPt.keys()[0] #0
        receivers = msgPt[sender].keys()
        if sender not in ctPResults[msgLength][sSpeed].keys():
            ctPResults[msgLength][sSpeed][sender] = {}
        for receiver in receivers:
            if receiver not in ctPResults[msgLength][sSpeed][sender].keys():
                ctPResults[msgLength][sSpeed][sender][receiver] = []
            ctPResults[msgLength][sSpeed][sender][receiver] += [msgPt[sender][receiver]]

        if sender not in ctDelayResults[msgLength][sSpeed].keys():
            ctDelayResults[msgLength][sSpeed][sender] = {}
        for receiver in receivers:
            if receiver == sender:
                continue
            if receiver not in ctDelayResults[msgLength][sSpeed][sender].keys():
                ctDelayResults[msgLength][sSpeed][sender][receiver] = []
            #print sender, receiver, sSpeed
            ctDelayResults[msgLength][sSpeed][sender][receiver] += [msgDelayStatst[sender][receiver]]
        #WORK NEEDS TO BE DONE ON THIS!!


        #ctPResults[sSpeed]+=[msgPt]
        #ctDelayResults[sSpeed] += [msgDelayStatst]

#ONE MORE POST PROCESS, REVERSE THE ORDERING OF THE TRIALS

def reverseMsgDictionaryKeys(originalDict):
    reverseDict = {}
    for speed in originalDict.keys():
        numTrials = len(originalDict[speed])
        reverseDict[speed] = {}
        if len(originalDict[speed])==0:
            print "No msgPResults for speed", speed
            continue
        for sender in originalDict[speed][0].keys():
            reverseDict[speed][sender] = {}
            for receiver in originalDict[speed][0][sender].keys():
                print speed, sender, receiver
                reverseDict[speed][sender][receiver] = [originalDict[speed][t][sender][receiver] for t in range(numTrials)]

    return reverseDict


def reverseBlfDictionaryKeys(originalDict):
    reverseDict = {}
    for speed in originalDict.keys():
        numTrials = len(originalDict[speed])
        reverseDict[speed] = {}
        if len(originalDict[speed])==0:
            print "No msgPResults for speed", speed
            continue
        for receiver in originalDict[speed][0].keys():
            reverseDict[speed][receiver] = [originalDict[speed][t][receiver] for t in range(numTrials)]
    return reverseDict



# reverseMsgPResults = reverseMsgDictionaryKeys(msgPResults)
# reverseMsgDelayResults = reverseMsgDictionaryKeys(msgDelayResults)
# reverseBeliefPResults = reverseBlfDictionaryKeys(beliefPResults)
# reverseBeliefDelayResults = reverseBlfDictionaryKeys(beliefDelayResults)




postProcessFileName = missionDirectory + "postProcess" +'Length'+ str(msgLength) + '.p'
pickle.dump([msgPResults,msgDelayResults,beliefPResults,beliefDelayResults], open(postProcessFileName, "wb" ) )
# pickle.dump([reverseMsgPResults,reverseMsgDelayResults,reverseBeliefPResults,reverseBeliefDelayResults], open(postProcessFileName, "wb" ) )

# ctPResults = reverseMsgDictionaryKeys(ctPResults)
# ctDelayResults = reverseMsgDictionaryKeys(ctDelayResults)
#
if len(ctPResults.keys())>0:
    postProcessCTFileName = missionDirectory + "postProcessCT" +'Length'+ str(msgLength) + '.p'
    # #pickle.dump([msgPResults,msgDelayResults,beliefPResults,beliefDelayResults], open(postProcessFileName, "wb" ) )
    pickle.dump([ctPResults,ctDelayResults], open(postProcessCTFileName, "wb" ) )


print "Missed Files, (agentID, missionID)"
print missedFiles
print missionDirectory

        # #Save Results to Dictionary
        # beliefResults[trialI] = (IDBeliefs, FirstBelief, beliefDelays, beliefSuccess, pAgents, delayAgents)
        # msgResults[trialI] = (msgDelay, msgP)



# #STEP 1
#
# for speed in msgPResults.keys():
#     numTrials = len(msgPResults[speed])
#     reverseMsgPResults[speed] = {}
#     if len(msgPResults[speed])==0:
#         print "No msgPResults for speed", speed
#         continue
#     for sender in msgPResults[speed][0].keys():
#         reverseMsgPResults[speed][sender] = {}
#         for receiver in msgPResults[speed][0][sender].keys():
#             reverseMsgPResults[speed][sender][receiver] = [msgPResults[speed][t][sender][receiver] for t in range(numTrials)]
#
# reverseMsgDelayResults = {}
# for speed in msgDelayResults.keys():
#     numTrials = len(msgDelayResults[speed])
#     reverseMsgDelayResults[speed] = {}
#     if len(msgPResults[speed])==0:
#         print "No msgPResults for speed", speed
#         continue
#     for sender in msgDelayResults[speed][0].keys():
#         reverseMsgDelayResults[speed][sender] = {}
#         for receiver in msgDelayResults[speed][0][sender].keys():
#             reverseMsgDelayResults[speed][sender][receiver] = [msgDelayResults[speed][t][sender][receiver] for t in range(numTrials)]
#
# reverseBeliefPResults = {}
# for speed in beliefPResults.keys():
#     numTrials = len(beliefPResults[speed])
#     reverseBeliefPResults[speed] = {}
#     if len(msgPResults[speed])==0:
#         print "No msgPResults for speed", speed
#         continue
#     for receiver in beliefPResults[speed][0].keys():
#         reverseBeliefPResults[speed][receiver] = [beliefPResults[speed][t][receiver] for t in range(numTrials)]
#
# reverseBeliefDelayResults = {}
# for speed in beliefDelayResults.keys():
#     numTrials = len(beliefDelayResults[speed])
#     reverseBeliefDelayResults[speed] = {}
#     if len(msgPResults[speed])==0:
#         print "No msgPResults for speed", speed
#         continue
#     for receiver in beliefDelayResults[speed][0].keys():
#         reverseBeliefDelayResults[speed][receiver] = [beliefDelayResults[speed][t][receiver] for t in range(numTrials)]
#
