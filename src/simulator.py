import numpy as np
import matplotlib.pyplot as plt
import sys, copy
import robot as rb
import cbba, ocbbaRobot, tasks
import udpMessage as udp
import kinematics as kn
import helperMethods as hm

class Simulator:
    def __init__(self):
        self.totalRobotArray = []
        self.missionID = 9
        self.nTotal = 6
        self.time = 0
        self.deltaT = 0.01
        self.finaLTime = 1000 #ms
        self.simulationOver = False
        self.commSim = None
        self.plotNum = 1
        plt.ion()
        self.previousTime = 0

    def createRobots(self,n):
        for i in range(n):
            self.totalRobotArray += [rb.Robot(i)]
            self.nTotal = n

    def runSimulation(self,totalTime):
        for i in range(totalTime):
            self.incrementTime()

    def incrementTime(self):
        self.time += self.deltaT
        for robot in self.totalRobotArray:
            robot.simTime = self.time
        np.random.shuffle(self.totalRobotArray)

    def updateRobotTime(self,robot):
        robot.simTime = self.time #Update robots internal clock

    def updateRobotPose(self,robot):
        #TODO
        return None

    def robotColor(self,robot):
        colors = ['r','g','brown','blue','m','y','pink','orange','grey']
        i = robot.agentID%len(colors)
        return colors[i]

    def plotRobots(self,fsize=(12,12)):
        fig = plt.figure(figsize=fsize)
        ax = fig.add_subplot(111,aspect='equal')
        ax.grid(False)

        minX = min([robot.pose.X for robot in self.totalRobotArray])
        maxX = max([robot.pose.X for robot in self.totalRobotArray])
        minY = min([robot.pose.Y for robot in self.totalRobotArray])
        maxY = max([robot.pose.Y for robot in self.totalRobotArray])
        minBoth = min(minX,minY)
        maxBoth = max(maxX,maxY)
        ax.set_ylim([minBoth-1,maxBoth+1])
        ax.set_xlim([minBoth-1,maxBoth+1])
        ax.xaxis.set_ticklabels([])
        ax.yaxis.set_ticklabels([])
        addToLegend = False #This is just for legend making
        for robot in self.totalRobotArray:
            directedTriangle = (3,0,30+robot.pose.Phi*180/np.pi)
            if addToLegend:
                robot_pnt = ax.plot(robot.pose.X,robot.pose.Y,marker=directedTriangle, c=self.robotColor(robot),markersize=16,label='Agents',linestyle="None")
            else:
                robot_pnt = ax.plot(robot.pose.X,robot.pose.Y,marker=directedTriangle, c=self.robotColor(robot),markersize=16,label='_nolegend_',linestyle="None")
            addToLegend=False
            printID=False
            if printID==True:
                ax.annotate(str(robot.agentID),(robot.pose.X,robot.pose.Y),xytext=(-3, -3),textcoords='offset points',size=12,color='black',fontweight='bold')
        firstRobotPlot=True
########################################################################################
class WifiSimulator(Simulator):
    def __init__(self,commType="Adhoc"):
        Simulator.__init__(self)
        self.commType = commType
        self.csma = True

        ##########  THIS CANNOT BE CHANGED!!!
        self.deltaT = 0.0001  #seconds
        #self.deltaT = min(robot.wifi.slotTime for robot in self.totalRobotArray)
        #This is likely 10e-6
        ##########
        self.contention = False

        #Comm stuff
        self.commRate = 1.0 #Mbps
        self.commRadius = 1.5 #M, disk model
        self.commNeighbors = dict() #I = senderID, J = Robot Object <-ugly
        self.p_comm = dict()
        self.csma_send_codes = {0:('Already Broadcasting','b'),
                                1:('Nothing to transmit','0.5'),
                                2:('Medium Busy','r'),3:('DIFS','#ffcc66'),
                                4:('BackOff','y'),
                                9:('Successful Transmission','g')}
        self.csma_rcv_codes = {0:('Already Broadcasting','m'),
                                1:('Collission','r'),
                                2:('No transmission ','0.5'),
                                3:('Receiving msg','y'),
                                9:('Successful Transmission','g')}


        #Tracking TransmissionReceptions
        self.attemptedBroadcast = dict()
        self.successfullyTransmitted = dict()
        self.successfullyReceived = dict()

    def initRobotPoseRing(self,robotArray):
        poses = kn.posesRing(len(robotArray),self.commRadius)
        for i in range(len(robotArray)):
            robotArray[i].pose = poses[i]
        self.updateCommNeighbors(robotArray)

    def initRobotPoseLine(self,robotArray):
        poses = kn.posesLine(len(robotArray),self.commRadius)
        for i in range(len(robotArray)):
            robotArray[i].pose = poses[i]
        self.updateCommNeighbors(robotArray)

    def initRobotPoseRingFC(self,robotArray):
        poses = kn.posesRingFC(len(robotArray),self.commRadius)
        for i in range(len(robotArray)):
            robotArray[i].pose = poses[i]
        self.updateCommNeighbors(robotArray)

    def initRobotPoseStar(self,robotArray):
        poses = kn.posesStar(len(robotArray),self.commRadius)
        for i in range(len(robotArray)):
            robotArray[i].pose = poses[i]
        self.updateCommNeighbors(robotArray)

    def incrementTime(self,printTime=False):
        #This will be the main function that is called by the simulator
        deltaT = self.deltaT #This is my arbitraty rate of agents
        robotI = [r.agentID for r in self.totalRobotArray]
        np.random.shuffle(robotI)

        self.time += deltaT
        if printTime:
            print "t=", str(self.time)

        for ri in robotI:
            robot=self.totalRobotArray[ri]
            self.updateRobotTime(robot)
            self.updateRobotPose(robot)

        self.updateCommNeighbors()
        for ri in robotI:
            robot=self.totalRobotArray[ri]
            self.updateRobotWifi(robot,True)

        for ri in robotI:
            robot=self.totalRobotArray[ri]
            robot.loop()

    def updateCommNeighbors(self,robotArray=None):
        if robotArray == None:
            robotArray = self.totalRobotArray
        for robot in robotArray:
            neighborList = []
            for otherRobot in robotArray:
                if otherRobot.agentID != robot.agentID and robot.pose.dist(otherRobot.pose)<=self.commRadius:
                    #neighborList+=[otherRobot]
                    self.p_comm[robot.agentID][otherRobot] = 1
                else:
                    self.p_comm[robot.agentID][otherRobot] = 0
            #self.commNeighbors[robot.agentID] = neighborList

    def initializeCommDictionaries(self):
        self.p_comm = dict.fromkeys([r.agentID for r in self.totalRobotArray])
        for k in self.p_comm.keys():
            self.p_comm[k] = dict.fromkeys([r for r in self.totalRobotArray],0)
        self.attemptedBroadcast = dict.fromkeys([r.agentID for r in self.totalRobotArray])
        for k in self.attemptedBroadcast.keys():
            self.attemptedBroadcast[k] =  dict.fromkeys([r.agentID for r in self.totalRobotArray],0)
        self.successfullyTransmitted = dict.fromkeys([r.agentID for r in self.totalRobotArray])
        for k in self.successfullyTransmitted.keys():
            self.successfullyTransmitted[k] =  dict.fromkeys([r.agentID for r in self.totalRobotArray],0)
        self.successfullyReceived = dict.fromkeys([r.agentID for r in self.totalRobotArray])
        for k in self.successfullyReceived.keys():
            self.successfullyReceived[k] =  dict.fromkeys([r.agentID for r in self.totalRobotArray],0)


    def updateRobotWifi(self,robot,logCSMACode=True, printCSMACode=False):
        send_code = self.csma_send(robot)   #TODO: Change title, maybe function not neccesary
        rcv_code = self.csma_rcv(robot)

        logRCV=True
        if logCSMACode:
            if send_code != robot.currentSendCode:
                robot.newHistorySentCodes.append((self.time,send_code))
                robot.currentSendCode = send_code
            if rcv_code != robot.currentRcvCode:
                if rcv_code == 9: #Get rid of "receiveing" status if successful transmission
                    t = robot.newHistoryRcvCodes.pop()[0]
                    robot.newHistoryRcvCodes.append((t,9))
                else:
                    robot.newHistoryRcvCodes.append((self.time,rcv_code))
                robot.currentRcvCode = rcv_code
        if printCSMACode:
            print robot.agentID,"s",self.csma_send_codes[send_code][0]
            print robot.agentID,"r",self.csma_rcv_codes[rcv_code][0]

    def csma_send(self,robot,printStatements=False):
        #OUTGOING MSG QUEUE (Sender) ->  INCOMING TRASMISSION BUFFER (Receiver)
        if self.time > robot.wifi.broadcastingEndTime:  #Maintenance on broadcasting variable
            robot.wifi.isBroadcasting = False
        if robot.wifi.isBroadcasting == True and self.commType != "Ideal": #Already Broadcasting
            return 9
        if robot.wifi.outgoingMsgQueue.empty(): #Nothing to Transmit
            return 1
        else:
            mediumFree = self.carrierSense(robot)         #1.  Carrier Sense
            if not mediumFree:                            #2a.  Medium busy: Defer transmission
                robot.wifi.difsCounter = 0
                if robot.wifi.backOffCounter == 0: #This needs to be fixed
                    robot.wifi.backOffCounter = np.random.randint(0,robot.wifi.cwMin+1)*robot.wifi.slotTime/self.deltaT #Set exponential backoff
                return 2
            else: #2b.  Medium is free, countdown DIFS and Backoff
                if robot.wifi.difsCounter < robot.wifi.difsN:
                    robot.wifi.difsCounter +=1
                    return 3
                else: #Entering Backoff
                    if robot.wifi.backOffCounter > 0:
                        robot.wifi.backOffCounter -=1
                        return 4
                    else: #Entering Transmission
                        msg = robot.wifi.outgoingMsgQueue.get()
                        dataString = msg.sendOnFakeUDP(self)
                        endTime = self.time + float(msg.getMsgLength())/robot.wifi.commRate
                        endTime = round(endTime,5)
                        #Add msg to every agent in neighborhood
                        for otherRobot in self.p_comm[robot.agentID].keys():
                            if self.p_comm[robot.agentID][otherRobot]>0.9:
                                self.successfullyTransmitted[robot.agentID][otherRobot.agentID] += 1
                                otherRobot.wifi.incomingTransmissionsBuffer.append((endTime,msg))

                        #for otherRobot in self.commNeighbors[robot.agentID]:
                            #self.successfullyTransmitted[robot.agentID][otherRobot.agentID] += 1
                            #otherRobot.wifi.incomingTransmissionsBuffer.append((endTime,msg))
                        #Update sender robot status
                        if endTime < robot.wifi.broadcastingEndTime:
                            #TODO: Why is this one called a lot?
                            #print "How is the end time before agents broadcastingEndTime?"
                            #print "t",self.time," dur", float(msg.getMsgLength())/robot.wifi.commRate
                            pass
                        robot.wifi.broadcastingEndTime = endTime
                        robot.wifi.isBroadcasting = True
                        robot.wifi.difsCounter = 0
                        return 9

    def carrierSense(self,robot): #Check neighbors for broadcasting
        if self.commType == "Ideal":
            return True
        for otherRobot in self.p_comm[robot.agentID].keys():
            if self.p_comm[robot.agentID][otherRobot]>.2 and otherRobot.wifi.isBroadcasting:
                return False
        return True
        # for otherRobot in self.commNeighbors[robot.agentID]:
        #     if otherRobot.wifi.isBroadcasting:
        #         return False
        # return True

    #Public only to simulator
    def csma_rcv(self,robot):
        #Called by sim every time step to simulate the csma rcv
        #Return: Status Code
        if robot.wifi.isBroadcasting and self.commType != "Ideal": #Agent Broadcasting, so IncomingTrans dropped
            robot.wifi.droppedMsgs += len(robot.wifi.incomingTransmissionsBuffer)
            robot.wifi.incomingTransmissionsBuffer = []
            return 0
        for transmit in robot.wifi.incomingTransmissionsBuffer: #Remove messages from robots out of range
            if self.p_comm[transmit[1].sender][robot]<0.05:
                robot.wifi.incomingTransmissionsBuffer.remove(transmit)

        numAgentsBroadcasting = 0  #Count Number of MSGS being received
        for otherRobot in self.p_comm[robot.agentID].keys():
            if (self.time - otherRobot.wifi.broadcastingEndTime<0.000001) and self.p_comm[robot.agentID][otherRobot]>0.4:
                numAgentsBroadcasting+=1
        if numAgentsBroadcasting >= 2 and self.commType != "Ideal": #Message Collission on Receiving
            robot.wifi.droppedMsgs += len(robot.wifi.incomingTransmissionsBuffer)
            robot.wifi.incomingTransmissionsBuffer = []
            return 1
        if len(robot.wifi.incomingTransmissionsBuffer) == 0: #No incoming Msgs
            return 2
        # if len(robot.wifi.incomingTransmissionsBuffer)>1:
        #     print "numAgentsBroadcasting", numAgentsBroadcasting
        #     print "t", '%0.10f' % self.time
        #     for (t,msg) in robot.wifi.incomingTransmissionsBuffer:
        #         print '%0.10f' % t, msg
        #         print "Msg Sender", msg.sender
        msgTransmitted = False
        for transmit in robot.wifi.incomingTransmissionsBuffer:
            if self.time > transmit[0]: #Transmission finished
                udpMsg = transmit[1]
                robot.wifi.incomingTransmissionsBuffer.remove(transmit)

                binaryErasureRand = np.random.random() #ADD BER
                if binaryErasureRand<self.p_comm[udpMsg.sender][robot]:
                    self.successfullyReceived[udpMsg.sender][robot.agentID] += 1
                    data = udpMsg.sendOnFakeUDP()
                    robot.wifi.incomingMsgQueue.put(data)
                    msgTransmitted = True
                    return 9 #Successful transmission
        if msgTransmitted:
            return 9
        else:
            return 3  #Still Transmitting

    def plotWifi(self):
        ax = plt.gca()
        self.updateCommNeighbors()
        for robot in self.totalRobotArray:
            for robotRcv in self.p_comm[robot.agentID].keys():
                if self.p_comm[robot.agentID][robotRcv]>0.9:
                    ax.plot([robot.pose.X, robotRcv.pose.X],[robot.pose.Y, robotRcv.pose.Y],'--',c='r',alpha=0.3)
            # for robotRcv in self.commNeighbors[robot.agentID]:
            #     ax.plot([robot.pose.X, robotRcv.pose.X],[robot.pose.Y, robotRcv.pose.Y],'--',c='r',alpha=0.3)

    def printCommNetwork(self):
        self.totalRobotArray.sort(key=lambda x:x.agentID)
        print "  |", [r.agentID for r in self.totalRobotArray]
        print "---", "---"*len(self.totalRobotArray)
        for robot in self.totalRobotArray:
            print robot.agentID, "|", [self.p_comm[robot.agentID][otherR] for otherR in self.totalRobotArray]


###############################################################################################################33
class TaskSimulator(WifiSimulator):
    def __init__(self,nRobots=0,nTasks=0):
        WifiSimulator.__init__(self)
        self.tasksArray = []
        self.nRobots = nRobots
        self.nTasks = nTasks
        self.L = int(nRobots/(nTasks+0.00000001))

        self.bundleResetNum = 0

    def incrementTime(self,printTime=False):
        #This will be the main function that is called by the simulator
        #1)Update Simulation Paramaters:
        robotI = [r.agentID for r in self.totalRobotArray]
        np.random.shuffle(robotI)
        wifiResolution = False  #This should be run by all wifi simulators
        for ri in robotI:
            robot=self.totalRobotArray[ri]

            if not robot.wifi.outgoingMsgQueue.empty() or len(robot.wifi.incomingTransmissionsBuffer)>0:
                wifiResolution = True
                break
        if wifiResolution:
            self.deltaT = 1e-5
            self.time += self.deltaT
        else:
            # self.deltaT = 0.0001
            self.deltaT = 0.001
            self.time = self.time + self.deltaT
            # self.time = np.round(self.time / self.deltaT) *self.deltaT
        #1)Update Simulation Paramaters:

        if printTime and self.time-self.previousTime>0.01:
            self.previousTime = self.time
            sys.stdout.flush()
            print '%0.10f' % self.time + "\r",

        #2)Update Reality on Each Robot
        for ri in robotI:
            robot=self.totalRobotArray[ri]
            self.updateRobotTime(robot)
            robot.updateRobotPose(self.deltaT)
            # self.updateRobotPose(robot)

        #3)Keep track of the new world
        self.updateCommNeighbors()
        for ri in robotI:
            robot=self.totalRobotArray[ri]
            self.updateRobotWifi(robot,True,False)

        #4)Every robot should make decisions and take actions
        for ri in robotI:
            robot=self.totalRobotArray[ri]
            robot.loop()

        tasksServiced = set()
        for ri in robotI:
            robot=self.totalRobotArray[ri]
            tasksServiced.update(set(robot.cbbaManager.p_path))
        #5)Shuffle the order of agents for iterating:
        return tasksServiced


    def createRobots(self,L):
        for i in range(self.nRobots):
            self.totalRobotArray += [rb.CbbaRobot(i,L,copy.deepcopy(self.nTasks),self.nRobots,False)]
            self.nTotal = len(self.totalRobotArray)
        self.initializeCommDictionaries()

    def initializeAllRobotLists(self):
        for robot in self.totalRobotArray:
            robot.cbbaManager.initializeLists(copy.deepcopy(self.tasksArray),len(self.totalRobotArray))

    def loadTasks(self,tasksArray):
        self.tasksArray = copy.deepcopy(tasksArray)
        self.nTasks = len(self.tasksArray)
        nRobots = len(self.totalRobotArray)
        self.initializeAllRobotLists()

    def createAbilityArrays(self,nr,nt,pAble,correl=1):
        Lability = float(nt)/nr
        randomTaskArray = np.arange(nt)
        np.random.shuffle(randomTaskArray)
        abilityMatrix = {}
        #this is the initial allocation
        start=0
        end = Lability
        for i in range(nr):
            abilityArray = [0]*nt
            for t in range(int(start),int(end)):
                abilityArray[t]=1
            nAdditionalToAdd = int(round(pAble*nt)) - (int(end)-int(start))
            remainingTasks = range(0,int(start)) + range(int(end),nt)
            newlyAssignedTasks = np.random.choice(remainingTasks,nAdditionalToAdd,replace=False)
            for t in newlyAssignedTasks:
                task_i = randomTaskArray[t]
                abilityArray[task_i]=1
            start+=Lability
            end+=Lability
            abilityMatrix[i]=abilityArray
            #r.taskAbilityArray = abilityArray
        return abilityMatrix

    def loadTaskAbilityMatrix(self,abilityMatrix):
        for r in self.totalRobotArray:
            r.taskAbilityArray = abilityMatrix[r.agentID]

    # Different Experiement/Missions To Run
    def incrementConvergence(self,endTime,printTime=True,plot=True):
        converge=False
        lastPlot = 0.1
        while self.time<endTime and not converge:
            tasksServiced = self.incrementTime(printTime)
            if len(tasksServiced) == len(self.tasksArray):
                print "CONVERGED"
                self.plotAll()
                return self.time
            if (lastPlot - self.time) < 0.000001 and plot==True:
                taskIDs = set([t.id for t in self.tasksArray])
                lastPlot+=0.1
                self.plotAll()
                plt.show()
                #print "Unserviced:", taskIDs.difference(tasksServiced)
        print "TimeOut"
        return None


    def incrementConvergState(self,endTimeBB,initial=False,printTime=False,plot=False):
        allConverge = False
        lastPlot=0.1
        timeOut = False
        prevCBBARound = min([r.cbbaRound for r in self.totalRobotArray])
        # sga = self.centralSGA()
        while not timeOut and not allConverge:
            tasksServiced = self.incrementTime(printTime)
            currentCBBARound = min([r.cbbaRound for r in self.totalRobotArray])
            if initial==True:
                allConverge = all([r.initialResults!=None for r in self.totalRobotArray])
            else:
                allConverge = all([r.finalResults!=None for r in self.totalRobotArray])
            if currentCBBARound != prevCBBARound:
                if plot:
                    print currentCBBARound
                    for r in self.totalRobotArray:
                        print r.cbbaManager.z_winagents
                prevCBBARound = currentCBBARound
                # consArray = copy.deepcopy(sga.z_winagents)
                # for r in self.totalRobotArray:
                #     # print r.agentID, list(reversed([r.cbbaManager.z_winagents[s] for s in ys]))
                #     consArray = [r.cbbaManager.z_winagents[i] if consArray[i]==r.cbbaManager.z_winagents[i] else -1 for i in range(len(consArray))]
                # print currentCBBARound
                # ys=  list(reversed(np.argsort(sga.y_winbids)))
                # for r in self.totalRobotArray:
                #     print r.agentID, [r.cbbaManager.z_winagents[j] for j in ys]
                # print "j",[ys[j] if j!=currentCBBARound else str(ys[j]) for j in range(len(ys))]
                # print "y",['%0.02f'%self.totalRobotArray[0].cbbaManager.y_winbids[j] if self.totalRobotArray[0].cbbaManager.y_winbids[j]>0 else '-' for j in list(reversed(np.argsort(self.totalRobotArray[0].cbbaManager.y_winbids)))]
                # print "z",[consArray[ys[j]] if j!=(currentCBBARound-2) else str(consArray[ys[j]]) for j in range(len(ys))]
            if type(endTimeBB) == float:
                timeOut = (self.time>=endTimeBB)
            elif type(endTimeBB) == int:
                timeOut = (currentCBBARound >= endTimeBB)
            else:
                timeOut = False
        p_central = [r.cbbaManager.p_path for r in self.totalRobotArray]
        b_central = [r.cbbaManager.b_bundle for r in self.totalRobotArray]
        y_central = self.totalRobotArray[0].cbbaManager.y_winbids
        z_central = self.totalRobotArray[0].cbbaManager.z_winagents
        teamValueI = [r.cbbaManager.pathValueF(r.cbbaManager.p_path,r.cbbaManager.tasksList) for r in self.totalRobotArray]
        teamValue = sum(teamValueI)

        res = hm.CBBAResults()
        res.teamValue = teamValue
        res.p_paths = p_central
        res.b_bundles = b_central
        res.y_winbids = y_central
        res.z_winagents = z_central
        res.cbbaRounds = currentCBBARound
        maxYInsertion = max([-1]+[r.insertionHeuristic[0] for r in self.totalRobotArray if r.insertionHeuristic!=None])
        maxT = max([-1]+[r.insertionHeuristic[1] for r in self.totalRobotArray if r.insertionHeuristic!=None])
        if maxT>0:
            res.insertionHeuristic = maxYInsertion/maxT
        else:
            res.insertionHeuristic = None
        if allConverge == False:
            res.convergenceTime = -1
        else:
            res.convergenceTime = self.time
        return res


    def incrementConvergeMultiple(self,endTimeBB,initial=False,printTime=False,plot=False):
        allConverge = False
        lastPlot=0.1
        timeOut = False
        prevCBBARound = min([r.cbbaRound for r in self.totalRobotArray])
        # sga = self.centralSGA()
        while not timeOut and not allConverge:
            tasksServiced = self.incrementTime(printTime)
            currentCBBARound = min([r.cbbaRound for r in self.totalRobotArray])
            allConverge = all([r.convergeCounter>=2 for r in self.totalRobotArray])
            if currentCBBARound != prevCBBARound:
                if plot:
                    print currentCBBARound
                    for r in self.totalRobotArray:
                        print r.cbbaManager.z_winagents
                prevCBBARound = currentCBBARound
                # consArray = copy.deepcopy(sga.z_winagents)
                # for r in self.totalRobotArray:
                #     # print r.agentID, list(reversed([r.cbbaManager.z_winagents[s] for s in ys]))
                #     consArray = [r.cbbaManager.z_winagents[i] if consArray[i]==r.cbbaManager.z_winagents[i] else -1 for i in range(len(consArray))]
                # print currentCBBARound
                # ys=  list(reversed(np.argsort(sga.y_winbids)))
                # for r in self.totalRobotArray:
                #     print r.agentID, [r.cbbaManager.z_winagents[j] for j in ys]
                # print "j",[ys[j] if j!=currentCBBARound else str(ys[j]) for j in range(len(ys))]
                # print "y",['%0.02f'%self.totalRobotArray[0].cbbaManager.y_winbids[j] if self.totalRobotArray[0].cbbaManager.y_winbids[j]>0 else '-' for j in list(reversed(np.argsort(self.totalRobotArray[0].cbbaManager.y_winbids)))]
                # print "z",[consArray[ys[j]] if j!=(currentCBBARound-2) else str(consArray[ys[j]]) for j in range(len(ys))]
            if type(endTimeBB) == float:
                timeOut = (self.time>=endTimeBB)
            elif type(endTimeBB) == int:
                timeOut = (currentCBBARound >= endTimeBB)
            else:
                timeOut = False
        p_central = [r.cbbaManager.p_path for r in self.totalRobotArray]
        b_central = [r.cbbaManager.b_bundle for r in self.totalRobotArray]
        y_central = self.totalRobotArray[0].cbbaManager.y_winbids
        z_central = self.totalRobotArray[0].cbbaManager.z_winagents
        teamValueI = [r.cbbaManager.pathValueF(r.cbbaManager.p_path,r.cbbaManager.tasksList) for r in self.totalRobotArray]
        teamValue = sum(teamValueI)

        res = hm.CBBAResults()
        res.teamValue = teamValue
        res.p_paths = p_central
        res.b_bundles = b_central
        res.y_winbids = y_central
        res.z_winagents = z_central
        res.cbbaRounds = currentCBBARound
        maxYInsertion = max([-1]+[r.insertionHeuristic[0] for r in self.totalRobotArray if r.insertionHeuristic!=None])
        maxT = max([-1]+[r.insertionHeuristic[1] for r in self.totalRobotArray if r.insertionHeuristic!=None])
        if maxT>0:
            res.insertionHeuristic = maxYInsertion/maxT
        else:
            res.insertionHeuristic = None
        if allConverge == False:
            res.convergenceTime = -1
        else:
            res.convergenceTime = self.time
        return res

    def incrementCbbaRounds(self,nrounds,printCbbaRounds=False,printTime=False,plotdt=0):
        currentCBBARound = min([r.cbbaManager.bbRound for r in self.totalRobotArray])
        (team_total_value, b_central, y_central, z_central, p_team, b_team) = self.centralSGANewTask(None,-1)
        prevRound = 0
        while currentCBBARound < nrounds:
            currentCBBARound = min([r.cbbaManager.bbRound for r in self.totalRobotArray])
            if currentCBBARound!=prevRound:
                if sum([r.cbbaManager.bbRound for r in self.totalRobotArray])/float(len(self.totalRobotArray)) != currentCBBARound:
                    raise Exception('Not all robots on same bbRound')
                prevRound = currentCBBARound
                if printCbbaRounds:
                    p_central = [r.cbbaManager.p_path for r in self.totalRobotArray]
                    teamValueI = [r.cbbaManager.pathValueF(r.cbbaManager.p_path,r.cbbaManager.tasksList) for r in self.totalRobotArray]
                    teamValue = sum(teamValueI)
                    print "RD",currentCBBARound, teamValue, p_central
                    print " "
                    for r in self.totalRobotArray:
                        print r.cbbaManager.z_winagents
                # print "jit", ["ag"+str(r.agentID) for r in sorted(self.totalRobotArray,key=lambda x:x.agentID)]
                # for jit in range(1,currentCBBARound):
                #     print b_central[jit-1], [(j,robot.cbbaManager.z_winagents[j],'%0.03f'%robot.cbbaManager.y_winbids[j]) for robot in sorted(self.totalRobotArray,key=lambda x:x.agentID) for j in [np.argsort(robot.cbbaManager.y_winbids)[-jit]]]
            tasksServiced = self.incrementTime(printTime)
        p_central = [r.cbbaManager.p_path for r in self.totalRobotArray]
        b_central = [r.cbbaManager.b_bundle for r in self.totalRobotArray]
        y_central = self.totalRobotArray[0].cbbaManager.y_winbids
        z_central = self.totalRobotArray[0].cbbaManager.z_winagents
        teamValueI = [r.cbbaManager.pathValueF(r.cbbaManager.p_path,r.cbbaManager.tasksList) for r in self.totalRobotArray]
        teamValue = sum(teamValueI)

        res = hm.CBBAResults()
        res.teamValue = teamValue
        res.p_paths = p_central
        res.b_bundles = b_central
        res.y_winbids = y_central
        res.z_winagents = z_central
        res.convergenceTime = self.time
        res.cbbaRounds = currentCBBARound
        return res

    def incrementConsensus(self,endTime,printTime=True,plotdt=0):
        consensus=False
        lastPlot = 0.1
        consensusCounter = 0
        while self.time<endTime and not consensus:
            tasksServiced = self.incrementTime(printTime)
            if len(tasksServiced) == len(self.tasksArray):
                if all(self.totalRobotArray[i].cbbaManager.z_winagents == self.totalRobotArray[i+1].cbbaManager.z_winagents for i in range(len(self.totalRobotArray)-1)):
                    consensusCounter+=1
                    print "Consensus!"
                    consensus=True
                    if plotdt != 0:
                        self.plotRobots()
                        self.plotTasks()
                        self.plotTaskPath(None,True)
                        self.plotWifi()
                        plt.show()
                    break
            if (lastPlot - self.time) < 0.000001 and plotdt>0:
                taskIDs = set([t.id for t in self.tasksArray])
                lastPlot+=plotdt
                self.plotRobots()
                self.plotTasks()
                self.plotTaskPath(None,True)
                self.plotWifi()
                plt.show()
                #print "Unserviced:", taskIDs.difference(tasksServiced)
        if consensus!=True:
            print "TimeOut"
        p_central = [r.cbbaManager.p_path for r in self.totalRobotArray]
        b_central = [r.cbbaManager.b_bundle for r in self.totalRobotArray]
        y_central = self.totalRobotArray[0].cbbaManager.y_winbids
        z_central = self.totalRobotArray[0].cbbaManager.z_winagents
        teamValueI = [r.cbbaManager.pathValueF(r.cbbaManager.p_path,r.cbbaManager.tasksList) for r in self.totalRobotArray]
        teamValue = sum(teamValueI)

        res = hm.CBBAResults()
        res.teamValue = teamValue
        res.p_paths = p_central
        res.b_bundles = b_central
        res.y_winbids = y_central
        res.z_winagents = z_central
        res.convergenceTime = self.time
        return res

    def incrementEndtime(self,endTime,plot=True):
        converge=False
        lastPlot = 0.1 #This is how frequent we plot in (s)
        while self.time<endTime:
            converge = self.incrementTime(True)
            if (lastPlot - self.time) < 0.000001 and plot==True:
                lastPlot+=0.1
                self.plotRobots()
                self.plotTasks()
                self.plotTaskPath()
                self.plotWifi()
                plt.show()

    def computeTeamValue(self):
        teamValue = 0
        for robot in self.totalRobotArray:
            teamValue += robot.cbbaManager.pathValueF(robot.cbbaManager.p_path,robot.cbbaManager.tasksList,robot.cbbaManager.cbbaLocalTime)
        return teamValue

    def addTasks(self,newTaskObjectList,discoveryRobots=None):
        if discoveryRobots == None:
            discoveryRobots = self.totalRobotArray
        for task in newTaskObjectList:
            self.nTasks += 1
            # task.id = self.nTasks-1
            self.tasksArray += [task]
            for dRobot in discoveryRobots:
                #print "Agent:", dRobot.agentID, " Broadcasting Task:", task.id
                taskInfo = udp.TaskInfo()
                taskInfo.missionID = 99
                taskInfo.msgNum = 9
                taskInfo.task = task
                taskInfo.sender = dRobot.agentID
                dRobot.taskDiscovery(taskInfo)

    def centralSGA(self,plot=True):
        tasksSet = set(self.tasksArray)
        p_team = dict.fromkeys(self.totalRobotArray)
        b_team = dict.fromkeys(self.totalRobotArray)
        b_central = []
        z_central = [-1]*len(tasksSet)
        y_central = [-9999999]*len(tasksSet)
        central_value = 0
        for robot in p_team.keys():
            p_team[robot] = []
            b_team[robot] = []
            inf = robot.cbbaManager.inf
            precision = robot.cbbaManager.precision
        while len(tasksSet)!=0:
            maxcij = -inf
            maxi = None
            maxj = None
            max_task_n = None
            for robot in self.totalRobotArray:
                i = robot.agentID
                if len(p_team[robot])==robot.L_maxTasks:
                    continue
                v0 = robot.cbbaManager.pathValueF(p_team[robot],self.tasksArray)
                for task in tasksSet:
                    j = task.id

                    (pnew, Snew) = robot.cbbaManager.max_marginal_value(j,tuple(p_team[robot]),self.tasksArray) #7
                    c_ij = Snew - v0
                    if (c_ij-maxcij)>precision or (abs(c_ij-maxcij)<=precision and i>maxi.agentID):
                        maxcij = c_ij
                        maxi = robot
                        maxj = task
                        maxp = pnew
            #print maxj.id, maxi.agentID, maxcij

            #print maxi.agentID, maxj.id, maxcij
            if maxi==None:
                print tasksSet, [r.cbbaManager.z_winagents for r in self.totalRobotArray]
            p_team[maxi] = maxp
            b_team[maxi].append(maxj.id)
            b_central.append((maxj.id,maxi.agentID,maxcij))
            y_central[maxj.id] = maxcij
            z_central[maxj.id] = maxi.agentID
            central_value += maxcij
            tasksSet.remove(maxj)
        #print "b", self.b_central
        #print "y", ['%0.04f' % y for y in self.y_central]
        #print "z", [str(z).rjust(2) for z in self.z_central]
        if plot==True:
            self.plotRobots()
            self.plotTasks()

            teamValue = 0
            tasksServiced = set([])
            for robot in self.totalRobotArray:
                task_x = [robot.cbbaManager.tasksList[i].pose.X for i in p_team[robot]]
                task_y = [robot.cbbaManager.tasksList[i].pose.Y for i in p_team[robot]]
                ax = plt.gca()
                tk =ax.plot(task_x,task_y,'-*',c=self.robotColor(robot),markersize=5,label='_nolegend_')
                if len(task_x)>0:
                    tk1 =ax.plot([robot.pose.X, task_x[0]],[robot.pose.Y, task_y[0]],'-',c=self.robotColor(robot),markersize=5,label='_nolegend_')
                else:
                    print "ROBOT", robot.agentID, "has no tasks"
                teamValue += robot.cbbaManager.pathValueF(p_team[robot],robot.cbbaManager.tasksList,robot.cbbaManager.cbbaLocalTime)
                tasksServiced.update(set(p_team[robot]))
            ax.text(-1,-1,'%.2f'%teamValue,fontsize=12)
            ax.text(-1,-1.25,str(len(tasksServiced))+'/' + str(len(self.tasksArray)),fontsize=12)

        central_value = sum([r.cbbaManager.pathValueF(p_team[r],self.tasksArray) for r in self.totalRobotArray])
        res = hm.CBBAResults()
        res.teamValue = central_value
        res.p_paths = [p_team[i] for i in sorted(p_team.keys())]
        res.b_bundles = [b_team[i] for i in sorted(p_team.keys())]
        res.y_winbids = y_central
        res.z_winagents = z_central
        return res

    def centralSGANewTask(self,newTask,nTotalReset=0):
        tasksList = copy.deepcopy(self.tasksArray)
        p_team = dict.fromkeys(self.totalRobotArray)
        b_team = dict.fromkeys(self.totalRobotArray)
        for robot in p_team.keys():
            p_team[robot] = []
            b_team[robot] = []
            inf = robot.cbbaManager.inf
            precision = robot.cbbaManager.precision
        b_central = []
        z_central = [-1]*len(tasksList)
        y_central = [-inf]*len(tasksList)
        team_total_value = 0
        unassignedTasksSet = set(tasksList)
        insertedNewT = False
        while len(unassignedTasksSet)!=0:
            if len(unassignedTasksSet)<=nTotalReset and insertedNewT==False:
                #Add the new task at this point
                tasksList.append(newTask)
                unassignedTasksSet.add(newTask)
                z_central.extend([-1])
                y_central.extend([-inf])
                insertedNewT=True
            maxcij = -inf
            maxi = None
            maxj = None
            maxp = None
            for task in unassignedTasksSet:
                j = task.id
                for robot in self.totalRobotArray:
                    i = robot.agentID
                    if len(p_team[robot])==robot.L_maxTasks:
                        continue
                    else:
                        p, v = robot.cbbaManager.max_marginal_value(j,tuple(p_team[robot]),tasksList) #7
                        c_ij = v - robot.defaultPathValueF(tuple(p_team[robot]),tasksList)
                        if (c_ij-maxcij)>precision or (abs(c_ij-maxcij)<=precision and i>maxi.agentID):
                            maxcij = c_ij
                            maxi = robot
                            maxj = task
                            maxp = p
                            # max_task_n = task_n
            if maxi==None:
                print unassignedTasksSet, [r.cbbaManager.z_winagents for r in self.totalRobotArray]
                # raise NameError('L*R Not Large Enough')
            p_team[maxi] = maxp
            b_team[maxi].append(maxj.id)
            b_central.append((maxj.id,maxi.agentID,maxcij))
            y_central[maxj.id] = maxcij
            z_central[maxj.id] = maxi.agentID
            team_total_value += maxcij
            unassignedTasksSet.remove(maxj)
            if nTotalReset==0 and len(unassignedTasksSet)==0 and insertedNewT==False:
                #Add the new task at this point
                tasksList.append(newTask)
                unassignedTasksSet.add(newTask)
                z_central.extend([-1])
                y_central.extend([-inf])
                insertedNewT=True
        # return team_total_value
        return team_total_value, b_central, y_central, z_central, p_team, b_team

    #Some extra plotting tools

    def plotAll(self,saveName=False,ntNewArray=[],resetTask=False,axisbox=[],wifi=False,fsize=(8,8)):
        self.plotRobots(fsize)
        self.plotTasks()
        self.plotTaskPath()
        if resetTask==True:
            self.plotResetTasks()
        self.plotNewTask(ntNewArray)
        if wifi==True:
            self.plotWifi()
        if len(axisbox)>0:
            ax = plt.gca()
            ax.set_xlim([axisbox[0],axisbox[1]])
            ax.set_ylim([axisbox[2],axisbox[3]])
        if saveName!=False:
            plt.savefig(saveName+'.png',dpi=100,bbox_inches='tight')
        plt.show()

    def plotNewTask(self,ntNewArray):
        ax = plt.gca()
        tcolor='cyan'
        tedge='black'
        tedgesize= 3
        tsize = 40
        tmarker='*'
        for tid in ntNewArray:
            task = self.tasksArray[tid]
            tk =ax.plot(task.pose.X,task.pose.Y,marker=tmarker,markerfacecolor=tcolor,markersize=tsize,markeredgecolor=tedge,markeredgewidth=tedgesize,label="Tasks",linestyle="None")
            ax.annotate('T*',(task.pose.X,task.pose.Y),xytext=(-5, -5),textcoords='offset points')
        minTasksX = min([task.pose.X for task in self.tasksArray])-1
        maxTasksX = max([task.pose.X for task in self.tasksArray])+1
        minTasksY = min([task.pose.Y for task in self.tasksArray])-1
        maxTasksY = max([task.pose.Y for task in self.tasksArray])+1
        ax.set_xlim([min(minTasksX,ax.get_xlim()[0]),max(maxTasksX,ax.get_xlim()[1])])
        ax.set_ylim([min(minTasksY,ax.get_ylim()[0]),max(maxTasksY,ax.get_ylim()[1])])

    def plotResetTasks(self):
        ax = plt.gca()
        tcolor='black'
        tsize=12
        tmarker='o'
        tedgesize=0
        tedge='black'
        for r in self.totalRobotArray:
            if len(r.cbbaManager.tasksToReset)>0:
                for j in r.cbbaManager.tasksToReset:
                    task = self.tasksArray[j]
                    tk =ax.plot(task.pose.X,task.pose.Y,marker=tmarker,markerfacecolor=tcolor,markersize=tsize,markeredgecolor=tedge,markeredgewidth=tedgesize,label="Tasks",linestyle="None")
        minTasksX = min([task.pose.X for task in self.tasksArray])-1
        maxTasksX = max([task.pose.X for task in self.tasksArray])+1
        minTasksY = min([task.pose.Y for task in self.tasksArray])-1
        maxTasksY = max([task.pose.Y for task in self.tasksArray])+1
        ax.set_xlim([min(minTasksX,ax.get_xlim()[0]),max(maxTasksX,ax.get_xlim()[1])])
        ax.set_ylim([min(minTasksY,ax.get_ylim()[0]),max(maxTasksY,ax.get_ylim()[1])])


    def plotTasks(self,ntNewArray=[-1]):
        ax = plt.gca()
        #ax.text(-1,1,'%.2f'%self.time,fontsize=12)
        addToLegend = True
        for task in self.tasksArray:
            if task.id in ntNewArray:
                tcolor='cyan'
                tedge='black'
                tedgesize= 3
                tsize = 40
                tmarker='*'
            else:
                tcolor='black'
                tsize=10
                tmarker='.'
                tedgesize=0
                tedge='black'
            if task.timeOut>0:
                tk =ax.plot(task.pose.X,task.pose.Y,marker='X',c='r',markersize=15,label='_nolegend_',linestyle="None")
            if addToLegend:
                tk =ax.plot(task.pose.X,task.pose.Y,marker=tmarker,markerfacecolor=tcolor,markersize=tsize,markeredgecolor=tedge,markeredgewidth=tedgesize,label="Tasks",linestyle="None")
            else:
                tk =ax.plot(task.pose.X,task.pose.Y,marker=tmarker,markerfacecolor=tcolor,markersize=tsize,markeredgecolor=tedge,markeredgewidth=tedgesize,label='_nolegend_',linestyle="None")
            if task.id in ntNewArray:
                ax.annotate('T*',(task.pose.X,task.pose.Y),xytext=(-5, -5),textcoords='offset points')

            addToLegend=False
        minTasksX = min([task.pose.X for task in self.tasksArray])-1
        maxTasksX = max([task.pose.X for task in self.tasksArray])+1
        minTasksY = min([task.pose.Y for task in self.tasksArray])-1
        maxTasksY = max([task.pose.Y for task in self.tasksArray])+1
        ax.set_xlim([min(minTasksX,ax.get_xlim()[0]),max(maxTasksX,ax.get_xlim()[1])])
        ax.set_ylim([min(minTasksY,ax.get_ylim()[0]),max(maxTasksY,ax.get_ylim()[1])])

    def plotTaskPath(self, robots = None,annotateScore=False,ntNewArray=[-1]):
        if robots == None:
            robotList = self.totalRobotArray
        else:
            robotList = robots
        teamValue = 0
        tasksServiced = set([])
        #self.plotRobots()
        for robot in robotList:
            task_x = [robot.cbbaManager.tasksList[i].pose.X for i in robot.cbbaManager.p_path]
            task_y = [robot.cbbaManager.tasksList[i].pose.Y for i in robot.cbbaManager.p_path]
            ax = plt.gca()
            tk =ax.plot(task_x,task_y,'.-',c=self.robotColor(robot),markersize=10,label='_nolegend_')
            if len(task_x)>0:
                tk1 =ax.plot([robot.pose.X, task_x[0]],[robot.pose.Y, task_y[0]],'-',c=self.robotColor(robot),markersize=5,label='_nolegend_')
            else:
                print "ROBOT", robot.agentID, "has no tasks"
            teamValue += robot.cbbaManager.pathValueF(robot.cbbaManager.p_path,robot.cbbaManager.tasksList,robot.cbbaManager.cbbaLocalTime)
            tasksServiced.update(set(robot.cbbaManager.p_path))
        if annotateScore:
            ax.text(-1,-1,'%.2f'%teamValue,fontsize=12)
            ax.text(-1,-1.25,str(len(tasksServiced))+'/' + str(len(self.tasksArray)),fontsize=12)



class OldCBBASimulator(TaskSimulator):
    def __init__(self,nRobots,nTasks):
        TaskSimulator.__init__(self,nRobots,nTasks)

    def loadRobots(self,robotArray):
        self.totalRobotArray = robotArray
        self.nTotal = len(self.totalRobotArray)
        self.initializeCommDictionaries()

    def createRobots(self,robotPoses=None,L=0,bundleResetNum=-1):
        for i in range(self.nRobots):
            robot = ocbbaRobot.OCbbaRobot(i,L,copy.deepcopy(self.nTasks),self.nRobots,False)
            if robotPoses!=None:
                robot.pose = robotPoses[i]
            robot.cbbaManager.bundleResetNum = bundleResetNum
            self.totalRobotArray.append(robot)
            self.nTotal = len(self.totalRobotArray)
        self.initializeCommDictionaries()


    def createFCIdeal(self):
        self.commType = "Ideal"
        minR = max([robot.pose.X for robot in self.totalRobotArray]) - min([robot.pose.X for robot in self.totalRobotArray]) + max([robot.pose.Y for robot in self.totalRobotArray]) - min([robot.pose.Y for robot in self.totalRobotArray])
        self.commRadius = minR
        self.updateCommNeighbors()

    def createMHIdeal(self):
        self.commType = "Ideal"
        maxR = 0
        for r1 in self.totalRobotArray:
            minDist = min([r1.pose.dist(r2.pose) for r2 in self.totalRobotArray if r1!=r2])
            maxR = max(maxR,minDist)
        # minR = min([r1.pose.dist(r2.pose) for r1 in self.totalRobotArray for r2 in self.totalRobotArray if r1!=r2])
        self.commRadius = 1.1*maxR
        self.updateCommNeighbors()


    def addTasks(self,newTaskObjectList,resetStrategy,globalResetValue,discoveryRobots=None,pAbility=1.0):
        if discoveryRobots == None:
            discoveryRobots = self.totalRobotArray
        for task in newTaskObjectList:
            #Update Sim Global Vars
            self.tasksArray += [task]
            self.nTasks = len(self.tasksArray)

            # nRobotsCanService = int(np.rint(pAbility*len(self.totalRobotArray)))
            # robotsThatCanService = np.random.choice(self.totalRobotArray,nRobotsCanService,False)
            # for r in self.totalRobotArray:
                # r.taskAbilityArray.append(0)
                # if r in robotsThatCanService:
                    # r.taskAbilityArray[task.id] = 1
            for dRobot in discoveryRobots:
                #print "Agent:", dRobot.agentID, " Broadcasting Task:", task.id
                taskInfo = udp.TaskInfo()
                taskInfo.missionID = 99
                taskInfo.msgNum = 9
                taskInfo.task = task
                taskInfo.sender = dRobot.agentID
                dRobot.taskDiscovery(taskInfo,resetStrategy,globalResetValue)

#########################
saved50Tasks = [tasks.Task(1,kn.Pose(-1.36628,-1.52335),0),tasks.Task(1,kn.Pose(-1.13785,-1.79054),1),tasks.Task(1,kn.Pose(-0.94388,-1.34090),2),tasks.Task(1,kn.Pose(-0.51308,-1.19725),3),tasks.Task(1,kn.Pose(0.44558,-1.16312),4),tasks.Task(1,kn.Pose(0.49316,-1.42722),5),tasks.Task(1,kn.Pose(0.78951,-1.52314),6),tasks.Task(1,kn.Pose(1.52320,-1.55117),7),tasks.Task(1,kn.Pose(-1.31296,-0.63352),8),tasks.Task(1,kn.Pose(-0.92668,-1.16067),9),tasks.Task(1,kn.Pose(-0.35987,-1.46344),10),tasks.Task(1,kn.Pose(0.21987,-1.08410),11),tasks.Task(1,kn.Pose(0.31461,-1.22243),12),tasks.Task(1,kn.Pose(0.28025,-1.08130),13),tasks.Task(1,kn.Pose(0.95337,-0.88604),14),tasks.Task(1,kn.Pose(1.98521,-1.17911),15),tasks.Task(1,kn.Pose(-1.05635,-0.35921),16),tasks.Task(1,kn.Pose(-1.18630,-0.30999),17),tasks.Task(1,kn.Pose(-0.15799,-0.53736),18),tasks.Task(1,kn.Pose(-0.40479,-0.81039),19),tasks.Task(1,kn.Pose(-0.17936,-0.37182),20),tasks.Task(1,kn.Pose(0.43803,-0.21128),21),tasks.Task(1,kn.Pose(1.56506,-0.36792),22),tasks.Task(1,kn.Pose(1.37400,-0.64057),23),tasks.Task(1,kn.Pose(-1.78430,0.14345),24),tasks.Task(1,kn.Pose(-1.54948,-0.22422),25),tasks.Task(1,kn.Pose(-0.91562,-0.06012),26),tasks.Task(1,kn.Pose(0.07492,0.14773),27),tasks.Task(1,kn.Pose(0.51823,-0.15021),28),tasks.Task(1,kn.Pose(0.48234,0.36310),29),tasks.Task(1,kn.Pose(1.10814,-0.21987),30),tasks.Task(1,kn.Pose(1.27223,0.19972),31),tasks.Task(1,kn.Pose(-1.65079,0.85482),32),tasks.Task(1,kn.Pose(-1.31298,0.78988),33),tasks.Task(1,kn.Pose(-0.83793,0.27633),34),tasks.Task(1,kn.Pose(0.04574,0.29774),35),tasks.Task(1,kn.Pose(0.05968,0.82231),36),tasks.Task(1,kn.Pose(0.15307,0.00223),37),tasks.Task(1,kn.Pose(0.92080,0.80053),38),tasks.Task(1,kn.Pose(1.47602,0.50945),39),tasks.Task(1,kn.Pose(-1.85630,0.64117),40),tasks.Task(1,kn.Pose(-0.90275,1.26141),41),tasks.Task(1,kn.Pose(-0.40685,1.17424),42),tasks.Task(1,kn.Pose(0.26203,0.93197),43),tasks.Task(1,kn.Pose(-0.20100,1.16251),44),tasks.Task(1,kn.Pose(1.09394,0.94368),45),tasks.Task(1,kn.Pose(1.37980,0.60771),46),tasks.Task(1,kn.Pose(1.50358,1.39293),47),tasks.Task(1,kn.Pose(-1.18517,1.28078),48),tasks.Task(1,kn.Pose(-1.30595,1.88238),49),]
