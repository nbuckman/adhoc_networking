#!/usr/bin/env python
import copy
import numpy as np
import cbba,robot, ocbbaRobot
import kinematics as kn
import cPickle as pickle
import Queue

class CBBAResults:
    def __init__(self,teamValue=None,p_paths=[],b_bundles=[],y_winbids=[],z_winagents=[],convergenceTime=None,cbbaRounds=None):
        self.teamValue = teamValue
        self.p_paths = p_paths
        self.b_bundles = b_bundles
        self.y_winbids = y_winbids
        self.z_winagents = z_winagents
        self.convergenceTime = convergenceTime
        self.cbbaRounds = cbbaRounds

    def printCentralBids(self):
        for t in np.argsort(self.y_winbids):
            print t, '%0.03f'%self.y_winbids[t], self.z_winagents[t]


class ExperimentResults():
    def __init__(self,initialResults=None, fullReplan=None,insertion=None,insertionSingleReset=None,rebuild=None,rebuildSingleReset=None):
        self.initialResults = initialResults
        self.fullReplan = fullReplan
        self.insertion = insertion
        self.insertionSingleReset = insertionSingleReset
        self.rebuild = rebuild
        self.rebuildSingleReset = rebuildSingleReset

class ExperimentSettings():
    def __init__(self,L=None,nt=None,nr=None,nreset=None,ntNew=None):
        self.L = L
        self.nt = nt
        self.nr = nr
        self.nreset = nreset
        self.ntNew = ntNew

class Heuristics():
    def __init__(self,maxTStar=None,maxInsertion=None,n=None,rbEntropy=None):
        self.maxTStar = maxTStar
        self.maxInsertion = maxInsertion
        self.n = n
        self.rbEntropy = rbEntropy

class Experiment():
    def __init__(self,settings=None,results=None):
        self.s = settings
        self.r = results
class HeuristicExperiment():
    def __init__(self,settings=None,results=None,heuristics=None):
        self.s = settings
        self.r = results
        self.h = heuristics



print "Loaded Helper"
def createAbilitySets(nr,nt,pAble,seed=None):
    taskSet = frozenset(range(nt))
    array = np.arange(nt)
    np.random.shuffle(array)
    abilityArrays = np.array_split(array,nr)
    abilitySets = [frozenset([j for j in abilityArrays[i]]) for i in range(len(abilityArrays))]

    for i in range(len(abilitySets)):
        abilitySet = abilitySets[i]
        nExtra = int((nt-len(abilitySet))*pAble)
        remainingTasks = taskSet - abilitySet
        extraTasks = np.random.choice(list(remainingTasks),nExtra,replace=False)
        abilitySet = abilitySet | frozenset(extraTasks)
        abilitySets[i] = abilitySet
    np.random.shuffle(abilitySets)
    return abilitySets

def createOptimalPathMatrix(L,robotPose,taskList):
    taskSet = frozenset(range(len(taskList)))
    queue_of_paths = Queue.LifoQueue()
    optimalPath = dict()
    nullSet = frozenset([])
    queue_of_paths.put((nullSet,taskSet))
    optimalPath[nullSet] = ((),0)
    toc = 0
    while not queue_of_paths.empty():
        (pathSet,remainingTaskSet) = queue_of_paths.get()
        if len(pathSet)>L or len(remainingTaskSet) == 0:
            continue
        #     continue #No need to do anything
        opPath,opValue = optimalPath[pathSet]
        for task in remainingTaskSet:
            newOpPathSet = pathSet | frozenset([task])
            newRemainingTasks = remainingTaskSet - frozenset([task])
            if newOpPathSet in optimalPath:
                newOpPath,newOpValue = optimalPath[newOpPathSet]
            else:
                newOpPath,newOpValue = find_optimal_location(opPath,task,robotPose,taskList)
                optimalPath[newOpPathSet] = (newOpPath,newOpValue)
                queue_of_paths.put((newOpPathSet,newRemainingTasks))
    return robotPose,taskList[:-1],taskList[-1],optimalPath

#     print "Time to Calc",toc-tic
#     print "Size", sys.getsizeof(finalData)/1000000.0,"MB"

def find_optimal_location(opPath,task,robotPose,taskArray):
    #Call pathValue |opPath|+1 times
    maxN = -1
    maxValue = -999999
    for n in range(len(opPath)+1):
        newTestPath = opPath[:n] + (task,) + opPath[n:]
        v = defaultPathValueF(newTestPath,taskArray,robotPose)
        if v>maxValue:
            maxN = n
            maxValue = v
    maxPath  = opPath[:maxN] + (task,) + opPath[maxN:]
    return (maxPath,maxValue)

def SGA_vf_robots(ynreset,robotObjectList,tasksList,newTasksList=[]):
    nt = len(tasksList)
    ntNew = len(newTasksList)
    teamValue = 0
    b_central = [[] for i in robotObjectList]
    p_central = [[] for i in robotObjectList]
    y_central = [0 for i in range(nt)]
    z_central = [-1 for i in range(nt)]
    sga_order = []
    remainingTasks = frozenset(range(nt))
    while len(remainingTasks)>0 and not all([len(b_central[i])==robotObjectList[i].cbbaManager.L_maxTasks for i in range(len(robotObjectList))]):
        maxT = None
        maxCij = -999999
        for i in range(len(robotObjectList)):
            r = robotObjectList[i]
            p_0 = tuple(p_central[i])
            v_0 = r.cbbaManager.pathValueF(p_0,tasksList)
            if len(p_0) == r.cbbaManager.L_maxTasks:
                # print i, "Full"
                continue
            for t in remainingTasks:
                # print p_0, t
                ptemp,vtemp = r.cbbaManager.max_marginal_value(t,p_0,tasksList)
                c_ij = vtemp-v_0
                if abs(c_ij-maxCij)<0.00001:
                    print "Getting close values"
                if c_ij>maxCij:
                    maxCij = c_ij
                    maxP = ptemp
                    maxT = t
                    maxI = i
        if maxT==None:
            print remainingTasks
            print p_central

        p_central[maxI] = maxP
        b_central[maxI] = b_central[maxI] + [maxT]
        y_central[maxT] = maxCij
        z_central[maxT] = maxI
        sga_order.append(maxT)
        remainingTasks = remainingTasks - frozenset([maxT])
    teamValue = sum(y_central)
    initialResults = CBBAResults()
    initialResults.teamValue = teamValue
    initialResults.p_paths = p_central
    initialResults.b_bundles = b_central
    initialResults.y_winbids = y_central
    initialResults.z_winagents = z_central
    initialResults.sga_order = sga_order
    return initialResults


def sequentialResults(allowedTauReplan,robotObjectList,tasksList,initialResult,newTasksList=[]):
    nt = len(tasksList)
    ntNew = len(newTasksList)
    teamValue = 0
    b_central = copy.deepcopy(initialResult.b_bundles)
    p_central = copy.deepcopy(initialResult.p_paths)
    y_central = copy.deepcopy(initialResult.y_winbids)
    z_central = copy.deepcopy(initialResult.z_winagents)
    sga_order = copy.deepcopy(initialResult.sga_order)

    allTasksList = tasksList + newTasksList
    nTasksReset = []
    for newTask in newTasksList:
        remainingTasks = frozenset([newTask.id])
        y_central.append(-99999)
        z_central.append(-1)
        if allowedTauReplan!=None:
            for i in range(len(robotObjectList)):
                r = robotObjectList[i]
                tau_rp = r.getGlobalReplanTimes(y_central)
                            # print ['%.02f'%t for t in tau_rp]
                taskb_allowedReset = [x[0] for x in enumerate(tau_rp) if x[1]<allowedTauReplan]
                maxAllowedYns = max([y_central[j] for j in range(len(y_central)) if (tau_rp[j]<allowedTauReplan and y_central[j]<=r.cbbaManager.inf/2)])
                m = float(maxAllowedYns)
                tasksToReset = set([t for t in b_central[i] if y_central[t]<=m and y_central[t]<r.cbbaManager.inf/2.0])
                    # print self.agentID, tasksToReset
                b_central[i] = [t for t in b_central[i] if t not in tasksToReset]
                p_central[i] = [t for t in p_central[i] if t not in tasksToReset]
            allTasksToReset =set([t for t in range(len(y_central)) if y_central[t]<=m and y_central[t]<r.cbbaManager.inf/2.0])
            # print "RESET # TASKS", len(allTasksToReset)
            for t in allTasksToReset:
                z_central[t]=-1
                y_central[t]=-r.cbbaManager.inf
            remainingTasks = remainingTasks | allTasksToReset
            nTasksReset += [len(allTasksToReset)]
        while len(remainingTasks)>0 and not all([len(b_central[i])==robotObjectList[i].cbbaManager.L_maxTasks for i in range(len(robotObjectList))]):
            maxT = None
            maxCij = -999999
            for i in range(len(robotObjectList)):
                r = robotObjectList[i]
                p_0 = tuple(p_central[i])
                v_0 = r.cbbaManager.pathValueF(p_0,allTasksList)
                if len(p_0) == r.cbbaManager.L_maxTasks:
                    continue
                for t in remainingTasks:
                    ptemp,vtemp = r.cbbaManager.max_marginal_value(t,p_0,allTasksList)
                    c_ij = vtemp-v_0
                    if abs(c_ij-maxCij)<0.0001:
                        print "Getting close values"
                    if c_ij>maxCij:
                        maxCij = c_ij
                        maxP = ptemp
                        maxT = t
                        maxI = i
            p_central[maxI] = maxP
            b_central[maxI] = b_central[maxI] + [maxT]
            y_central[maxT] = maxCij
            z_central[maxT] = maxI
            sga_order.append(maxT)
            remainingTasks = remainingTasks - frozenset([maxT])
    teamValue = sum(y_central)
    insertionResult = CBBAResults()
    insertionResult.teamValue = teamValue
    insertionResult.p_paths = p_central
    insertionResult.b_bundles = b_central
    insertionResult.y_winbids = y_central
    insertionResult.z_winagents = z_central
    insertionResult.sga_order = sga_order
    insertionResult.nResetList = nTasksReset


    # fullReplanResult = SGA_vf_robots(0,robotObjectList,tasksList+newTasksList)

    return insertionResult



#     if ynreset>-1:
#         if len(remainingTasks)<=ynreset:
#             remainingTasks = remainingTasks | frozenset([newTask])
#             z_central.append(-1)
#             y_central.append(-999999)
#             ynreset=-1
#     for i in range(len(robotPoses)):
#         if len(b_central[i])==L_max:
#             continue
#         rPose = robotPoses[i]
#         path = p_central[i]
#         prevValue = defaultPathValueF(path,taskList,rPose,0)
#         for j in remainingTasks:
#             if j in taskAbilitySets[i]:
#                 for n in range(len(path)+1):
#                     newPath = path[:n] + [j] + path[n:]
#                     newValue = defaultPathValueF(newPath,taskList,rPose)
#                     c_new = newValue-prevValue
#                     if (c_new-maxC)>0.001 or (abs(c_new-maxC)<0.001 and i>maxI):
#                         maxJ = j
#                         maxI = i
#                         maxB = b_central[i] + [j]
#                         maxC = c_new
#                         maxP = newPath
#     if maxI == None:
#         break
#     y_central[maxJ] = maxC
#     z_central[maxJ] = maxI + 3
#     b_central[maxI] = maxB
#     p_central[maxI] = maxP
#     sga_order.append(maxJ)
#     remainingTasks = remainingTasks - frozenset([maxJ])
#     if len(remainingTasks)==0 and ynreset==0:
#         remainingTasks = remainingTasks | frozenset([newTask])
#         y_central.append(-99999)
#         z_central.append(-1)
#         ynreset=-1
# # p_central = [c_ijList[i][frozenset(b_central[i])][0] for i in range(len(c_ijList))]
# # teamValue = [c_ijList[i][frozenset(b_central[i])][1] for i in range(len(c_ijList))]
# teamValue = sum(y_central)
# res = CBBAResults()
# res.teamValue = teamValue
# res.p_paths = p_central
# res.b_bundles = b_central
# res.y_winbids = y_central
# res.z_winagents = z_central
# return res

def SGA_vf(L_max,ynreset,robotPoses,taskList,taskAbilitySets=None):
    nt = len(taskList)
    taskSet = frozenset(range(nt-1))
    if taskAbilitySets==None:
        taskAbilitySets = [taskSet for r in robotPoses]
    newTask = nt-1
    teamValue = 0
    b_central = [[] for i in robotPoses]
    p_central = [[] for i in robotPoses]
    y_central = [0 for i in range(nt-1)]
    z_central = [-1 for i in range(nt-1)]
    sga_order = []
    remainingTasks = taskSet
    while len(remainingTasks)>0 and not all([len(b)==L_max for b in b_central]):
        maxJ = None
        maxI = None
        maxC = -9999999
        maxB = None
        maxP = []
        if ynreset>-1:
            if len(remainingTasks)<=ynreset:
                remainingTasks = remainingTasks | frozenset([newTask])
                z_central.append(-1)
                y_central.append(-999999)
                ynreset=-1
        for i in range(len(robotPoses)):
            if len(b_central[i])==L_max:
                continue
            rPose = robotPoses[i]
            path = p_central[i]
            prevValue = defaultPathValueF(path,taskList,rPose,0)
            for j in remainingTasks:
                if j in taskAbilitySets[i]:
                    for n in range(len(path)+1):
                        newPath = path[:n] + [j] + path[n:]
                        newValue = defaultPathValueF(newPath,taskList,rPose)
                        c_new = newValue-prevValue
                        if (c_new-maxC)>0.001 or (abs(c_new-maxC)<0.001 and i>maxI):
                            maxJ = j
                            maxI = i
                            maxB = b_central[i] + [j]
                            maxC = c_new
                            maxP = newPath
        if maxI == None:
            break
        y_central[maxJ] = maxC
        z_central[maxJ] = maxI + 3
        b_central[maxI] = maxB
        p_central[maxI] = maxP
        sga_order.append(maxJ)
        remainingTasks = remainingTasks - frozenset([maxJ])
        if len(remainingTasks)==0 and ynreset==0:
            remainingTasks = remainingTasks | frozenset([newTask])
            y_central.append(-99999)
            z_central.append(-1)
            ynreset=-1
    # p_central = [c_ijList[i][frozenset(b_central[i])][0] for i in range(len(c_ijList))]
    # teamValue = [c_ijList[i][frozenset(b_central[i])][1] for i in range(len(c_ijList))]
    teamValue = sum(y_central)
    res = CBBAResults()
    res.teamValue = teamValue
    res.p_paths = p_central
    res.b_bundles = b_central
    res.y_winbids = y_central
    res.z_winagents = z_central
    return res


def SGA(L_max,ynreset,nt,c_ijList,taskAbilityList):
    taskSet = frozenset(range(nt-1))
    newTask = nt-1
    teamValue = 0
    b_central = [[] for i in c_ijList]
    y_central = [0 for i in range(nt-1)]
    z_central = [-1 for i in range(nt-1)]
    sga_order = []
    remainingTasks = taskSet
    while len(remainingTasks)>0 and not all([len(b)==L_max for b in b_central]):
        maxJ = None
        maxI = None
        maxC = -9999999
        maxB = None
        if ynreset>-1:
            if len(remainingTasks)<=ynreset:
                remainingTasks = remainingTasks | frozenset([newTask])
                z_central.append(-1)
                y_central.append(-999999)
                ynreset=-1
        for i in range(len(c_ijList)):
            if len(b_central[i])==L_max:
                continue
            prevValue = c_ijList[i][frozenset(b_central[i])][1]
            for j in remainingTasks:
                b_new = frozenset(b_central[i]) | frozenset([j])
                if j in taskAbilityList[i]:
                    (p_new,v_new) = c_ijList[i][b_new]
                    c_new = v_new - prevValue
                    if c_new>maxC:
                        maxJ = j
                        maxI = i
                        maxB = b_central[i] + [j]
                        maxC = c_new
        if maxI == None:
            break
        y_central[maxJ] = c_ijList[maxI][frozenset(maxB)][1]-c_ijList[maxI][frozenset(b_central[maxI])][1]
        z_central[maxJ] = maxI +3
        b_central[maxI] = maxB
        sga_order.append(maxJ)
        remainingTasks = remainingTasks - frozenset([maxJ])
        if len(remainingTasks)==0 and ynreset==0:
            remainingTasks = remainingTasks | frozenset([newTask])
            y_central.append(-99999)
            z_central.append(-1)
            ynreset=-1
    p_central = [c_ijList[i][frozenset(b_central[i])][0] for i in range(len(c_ijList))]
    teamValue = [c_ijList[i][frozenset(b_central[i])][1] for i in range(len(c_ijList))]
    res = CBBAResults()
    res.teamValue = sum(teamValue)
    res.p_paths = p_central
    res.b_bundles = b_central
    res.y_winbids = y_central
    res.z_winagents = z_central
    return res



def getAllCij(envI,agentIDs):
    cijDir = '/home/nbuckman/rMatrix'
    c_ijList  = []
    robotPoses = []
    for i in range(len(agentIDs)):
        agI = agentIDs[i]
        [rP,tL,tT,c_ij] = pickle.load(open(cijDir+str(agI)+'/env'+str(envI)+'.p','rb'))
        c_ijList+=[c_ij]
        robotPoses+=[rP]
    return tL,tT,robotPoses,c_ijList

def defaultPathValueF(path,tasksList,rPose,startTime=0.0):
    #Value function is based on distance, reward,ldiscount,ability
    pathValue = 0
    totalTime = startTime
    speed = 1.0
    previous_pose = rPose
    for j in path:
        task = tasksList[j]
        totalTime = totalTime + previous_pose.dist(task.pose)/speed
        heteroReward = task.reward
        tReward = heteroReward * (task.ldiscount ** totalTime)                #print task.id, task.timeOut, time
        pathValue += tReward
        previous_pose = task.pose
        # totalTime += task.tau_service
    return pathValue

def getTeamValue(finalPaths,startTimes,robotPoses,taskArray):
    livePaths = [copy.deepcopy(p) for p in finalPaths]
    servicedTasks = set([])
    teamValue = 0

    currentPoses = [rP for rP in robotPoses]
    currentTime = [copy.copy(sT) for sT in startTimes]
    speed = 1.0
    while not all([len(p)==0 for p in livePaths]):
        firstArrival = 999999999
        firstArrivalI = None
        firstArrivalJ = None
        firstArrivalTask = None
        for i in range(len(robotPoses)):
            if len(livePaths[i])==0:
                continue
            nextTaskID = livePaths[i][0]
            newTask = taskArray[nextTaskID]
            nextTaskArrival = currentTime[i] + currentPoses[i].dist(newTask.pose)/speed

            if nextTaskArrival<=firstArrival:
                firstArrival = min(firstArrival,nextTaskArrival)
                firstArrivalI = i
                firstArrivalJ = nextTaskID
                firstArrivalTask = newTask
        if firstArrivalTask==None:
            print livePaths
            print "NO MORE TASKS!"
            return teamValue
        #Visit the task and update time/pose/path
        servicingTask = livePaths[firstArrivalI].pop(0) #Remove the task
        if servicingTask!=firstArrivalJ:
            print "THATS WEIRD"
        currentPoses[firstArrivalI] = firstArrivalTask.pose
        currentTime[firstArrivalI] = firstArrival
#         print "Ag", firstArrivalI, "Visiting", firstArrivalJ, firstArrival
        #Check  that servicingTask = firstArrival J
        if servicingTask not in servicedTasks:
            servicedTasks |= set([firstArrivalJ])
            teamValue += firstArrivalTask.reward * (firstArrivalTask.ldiscount ** currentTime[firstArrivalI])
            currentTime[firstArrivalI] += firstArrivalTask.tau_service
    return teamValue
