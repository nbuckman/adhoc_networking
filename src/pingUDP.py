#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage #Should this be from udpMessage import maxIDMessage?
import threading
import cPickle as pickle
from adhoc_networking.msg import maxIDMissionSpecs, MissionSentResults, MissionRcvdResults, UdpROSMsg #Should this be CAP?
import time


agentIDstr=os.environ['AGENT_NAME'][-1]
agentIDint = int(agentIDstr)
adhoc=True
simNeighbors = True  #Set this true if you want to enforce neighbors
UDP_IP = "10.10.3.255"
UDP_PORT = 5005
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
sock.bind((UDP_IP,UDP_PORT))
#sock.setblocking(0) #Don't block
print "UDP target IP:", UDP_IP, "port:", UDP_PORT

delay = 10
duration = 30
allSpeeds = [10,25,50,75,100,150,250,500,750,1000]
numTrials = 4

startTime = time.time()+delay
endTime = startTime + duration
sentMsgs = []

allRawResults = {}
missionNum=0
for j in range(numTrials):
    for speed in allSpeeds:
        nSent = 0
        while time.time()<startTime:
            pass
        #Starting Mission
        missionNum+=1
        print "Speed", speed
        timeDelay = 1.0/speed
        while time.time()<endTime:
            sent = sock.sendto(agentIDstr+str(missionNum).zfill(3),(UDP_IP,UDP_PORT))
            #print sent
            sentMsgs += [sent]
            nSent += 1
            time.sleep(timeDelay)
        print nSent
        startTime += duration + delay
        endTime = startTime + duration
        if speed not in allRawResults.keys():
            allRawResults[speed] = []
        allRawResults[speed] += [nSent]
print allRawResults
filename = '/home/pi/results/'+str(agentIDint)+'V4FC_PudpPingResults.p'
pickle.dump(allRawResults,open(filename,'wb'))
print filename
