#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage
from adhoc_networking.msg import networkTestSpecs, networkTestResults

#Title: broadcastTestNode_pi.py
#Author: Noam Buckman, 1/2017
#Descr:  This node will broadcast UDP connection test objects

def sender():
    rospy.init_node('sender', anonymous=True)
    rate = rospy.Rate(10000000) # 10hz
    agentIDstr=os.environ['AGENT_NAME'][-1]
    agentIDint = int(agentIDstr)
    random.seed(agentIDint)

    missionSpecsSub = rospy.Subscriber('missionNetworkSpecs',networkTestSpecs, missionSpecsCallback)

    #########################################################################################3
    #Setting up the UDP Socket
    UDP_IP = "10.10.3.255"
    UDP_PORT = 5005
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
    print "UDP target IP:", UDP_IP, "port:", UDP_PORT
    #########################################################################################3




    #Have the agents randomly start at different times (asynchrony)
    rospy.sleep(1)
    rospy.sleep(random.random())
    startTime = rospy.get_time()


    #ctUDPMsg = connectionTest.toUDPMessage()  #Converts it to a UDP Msg

    msgCounter = 0
    amountSent = 0
    missionDone = True
    while not rospy.is_shutdown():
        #1) Standby:  Wait to receive specs or stay out of experiments
        while ((missionDone) or (agentIDint not in activeAgents)) and not rospy.is_shutdown():
            rospy.spin()

        #2) Create Initial Test Message (from specs received)
        connectionTest = udpMessage.ConnectionTest()
        connectionTest.sender = agentIDint
        connectionTest.status = 0
        connectionTest.missionID = currentMissionID
        connectionTest.msgNum = 0
        connectionTest.totalNums = numOfMsgs
        connectionTest.sleepTime = sleepDelay
        connectionTest.msgLength = msgSize
        msgCounter=0 #RED?

        #3) Mission Started: Send Connection Test Msgs
        while not missionDone and not rospy.is_shutdown():
            connectionTest.msgNum=msgCounter
            ctUDPMsg = connectionTest.toUDPMessage()
            sizeSent = ctUDPMsg.sendOnUDP(sock,UDP_IP,UDP_PORT)
            amountSent+=sizeSent
            msgCounter+=1
            print "Sent", sizeSent, "Bytes"
            #4) Mission Done / Send Termination MSG
            if msgCounter == numOfMsgs:
                connectionTest.status = 9
                ctUDPMsg = connectionTest.toUDPMessage()
                finalTime = rospy.get_time()
                print "Packet Size", sizeSent, "Bytes"
                print "Total Time", finalTime-startTime, "s"
                print "Total Data", amountSent/1000000.0, "MB"
                print "Data Rate", amountSent*8.0/1000000/(finalTime-startTime), "mbps"
            elif msgCounter >= (numOfMsgs + numTermMsgs): #5? Done Transmitting Termination Msgs
                missionDone=True

            #rate.sleep()
            rospy.sleep(sleepDelay/1000000.0) #TODO:  ADD OPTION FOR RANDOM SLEEP

def missionSpecsCallback(data):
    #Update Mission Specifications and Start a New Mission
    print "RECEIVED A MISSION!"
    currentMissionID = data.missionID
    activeAgents = data.activeAgents
    sleepDelay = data.sleep_us
    msgSize = data.message_size
    numOfMsgs = data.numMsgs

    missionDone = False
    msgCounter = 0
    amountSent = 0

if __name__ == '__main__':
    try:
        #Global Variables
        currentMissionID = 1
        activeAgents = [1]
        sleepDelay = 100
        msgSize = 800
        numOfMsgs = 500
        missionDone = True
        msgCounter = 0
        amountSent = 0

        sender()
    except rospy.ROSInterruptException:
        pass

######################################################
#OLD
######################################################
    #Mission Setup
    # msgSize = (800-22)
    # numOfMsgs = 500
    # numOfAgents = 9

    #Load mission parameters
    # msgSize = rospy.get_param("/msgSize",800)
    # numOfMsgs = rospy.get_param("/numMsgs",500)
    # p = rospy.get_param("pSend",1.0)
    # activeAgents = rospy.get_param("/activeSenders",[])
    # numOfAgents = rospy.get_param("/numAgents",9)
    # numTermMsgs = rospy.get_param("/numTermMsgs",1000)
