#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage #Should this be from udpMessage import maxIDMessage?
from adhoc_networking.msg import leaderElectionMissionSpecs #Should this be CAP?

#Title: broadcastTestNode_pi.py
#Author: Noam Buckman, 3/2017
#Descr:  This node will broadcast and update my running nax

def sender():
    rospy.init_node('sender', anonymous=True)
    rate = rospy.Rate(10000000) # 10hz
    agentIDstr=os.environ['AGENT_NAME'][-1]
    agentIDint = int(agentIDstr)
    currentRunningMax = agentIDint #Initial running average

    random.seed(agentIDint)

    #This subscriber will rcv runningMax from runningMaxPublisher
    runningMaxSubscriber = rospy.Subscriber('runningMaxReceived', Int16, runningMaxUpdate)
    missionSpecsSubscriber = rospy.Subscriber('missionSpecs', leaderElectionMissionSpecs, missionSpecsCallback)
    #########################################################################################3
    #Setting up the UDP Socket
    UDP_IP = "10.10.3.255"
    UDP_PORT = 5005
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
    print "UDP target IP:", UDP_IP, "port:", UDP_PORT
    #########################################################################################3
    #Experiment = Set of trials/missions
    #Missions = trial
    #Have the agents randomly start at different times (asynchrony)
    startTime = rospy.get_time()

    msgCounter = 0
    amountSent = 0
    missionDone = True
    experimentRunning = True
    while not rospy.is_shutdown() and experimentRunning:
        #1) Standby:  Wait to receive specs or stay out of experiments
        while ((missionDone) or (agentIDint not in activeAgents)) and not rospy.is_shutdown():
            rospy.spin()

        #2) Create maxID Message (from specs received)
        maxIDMessage = udpMessage.MaxID()
        maxIDMessage.missionID = currentMissionID
        maxIDMessage.sender = agentIDint
        maxIDMessage.time = rospy.get_time()
        maxIDMessage.runningMax = currentRunningMax
        maxIdMessage.msgLength = msgSize

        cMaxAvgUDP = maxIDMessage.toUDP()
        cMaxAvgUDP.send()
        #3) Mission Started: Send Connection Test Msgs
        rospy.sleep(sleepDelay/1000000.0) #TODO:  ADD OPTION FOR RANDOM SLEEP

def missionSpecsCallback(missionSpecs):
    #Update Mission Specifications and Start a New Mission
    print "RECEIVED A MISSION!"
    global currentMissionID
    if currentMissionID != missionSpecs.missionID:
        global activeAgents, sleepDelay, msgSize, missionDone, msgCounter, amountSent, currentRunningMax
        currentMissionID = missionSpecs.missionID
        activeAgents = missionSpecs.activeAgents
        sleepDelay = missionSpecs.sleep_us
        msgSize = missionSpecs.message_size
        missionDone = False
        msgCounter = 0
        amountSent = 0
        currentRunningMax = agentIDint #Reset the currentRunningMax

def runningMaxUpdate(runningMax):
    print "Rcvd:", runningMax   #At some point should also send who sent the max
    global currentRunningMax
    currentRunningMax = max(runningMax,currentRunningMax)
    print "New:", currentRunningMax

if __name__ == '__main__':
    try:
        #Global Variables
        currentMissionID = 1
        activeAgents = [1,3]
        sleepDelay = 100
        msgSize = 800
        numOfMsgs = 500
        missionDone = True
        msgCounter = 0
        amountSent = 0

        sender()
    except rospy.ROSInterruptException:
        pass

######################################################
#OLD
######################################################
    #Mission Setup
    # msgSize = (800-22)
    # numOfMsgs = 500
    # numOfAgents = 9

    #Load mission parameters
    # msgSize = rospy.get_param("/msgSize",800)
    # numOfMsgs = rospy.get_param("/numMsgs",500)
    # p = rospy.get_param("pSend",1.0)
    # activeAgents = rospy.get_param("/activeSenders",[])
    # numOfAgents = rospy.get_param("/numAgents",9)
    # numTermMsgs = rospy.get_param("/numTermMsgs",1000)
