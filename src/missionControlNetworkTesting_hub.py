#!/usr/bin/env python
import rospy
import socket
import sys
import os
import datetime
import udpMessage
from std_msgs.msg import String, Bool
from adhoc_networking.msg import networkTestSpecs, networkTestResults

#Title: missionControlNetworkTesting.py
#Author: Noam Buckman, 1/2017
#Descr:  This node will both send mission specifications (before the mission) and record the experiment results (after the mission)
def missionControlNetworkTesting():
    rospy.init_node('missionControl', anonymous=True)
    rate = rospy.Rate(1000) # 1hz

    #Mission control will send mission stats to other agents
    missionSpecsPub = rospy.Publisher('missionNetworkSpecs', networkTestSpecs, queue_size=10)
    missionResultsSub = rospy.Subscriber('missionNetworkResults',networkTestResults, missionResultsCallback)
    missionDonePublisher = rospy.Publisher('missionDone', Bool, queue_size=1)
    missionDonePublisher.publish(False)
    #Mission Specifications
    currentMissionID = 0
    currentActiveAgents = []
    curentSleepTime = 0
    currentMsgSize = 0
    currentNumMsgs = 0

    #Mission Specifications:
    # sleepTimes = [100,200,1000,5000]
    # activeAgents = [[1,3],[1,3,4],[1,3,4,9]]
    # msgSizes = [500,800,900]
    # numsMsgs = [500,1000,2000]

    sleepTimes = [100]
    activeAgents = [[1,3]]
    msgSizes = [500]
    numsMsgs = [50]

    currentMissionDone=True
    rospy.sleep(3)
    missionDonePublisher.publish(True)


    for currentSleepTime in sleepTimes:
        for sleepTime in sleepTimes:
            for msgSize in msgSizes:
                for numMsgs in numsMsgs:
                    for activeAgentArray in activeAgents:
                        while not currentMissionDone and not rospy.is_shutdown():
                            #Mission is still going on, don't do anything!
                            rospy.spin()
                        #Previous mission done, move onto next one
                        currentMissionID += 1
                        print "Starting a new mission!"
                        print "ID:", currentMissionID, "Agents:", activeAgentArray, "Sleep Time (us)", sleepTime, "Msg Size: ",msgSize, "Num Msgs", numMsgs

                        newMission = networkTestSpecs()
                        newMission.missionID = currentMissionID
                        newMission.activeAgents = activeAgentArray
                        newMission.sleep_us = sleepTime
                        newMission.message_size = msgSize
                        newMission.numMsgs = numMsgs

                        missionSpecsPub.publish(newMission)
                        currentMissionDone = False
                        rospy.sleep(5) #For me
    missionSpecsPub.publish(-1,[-1],-1,-1,-1) #This tells the agents to terminate the mission!
    rate.sleep()



def missionResultsCallback(data):
    #Save the results
    currentMissionResults[data.agentID] = (data.missionID,data.sendRate,data.agentsReceiveRate,data.agentsSuccessRate)
    agentsDoneList += data.agentID

    if all(agentsDoneList):
        currentMissionDone = True
        #Save All the Data


if __name__ == '__main__':
    try:
        outputFile = "experiment.txt"

        agentsDoneList = [True]*9
        currentMissionResults = dict()
        missionControlNetworkTesting()
    except rospy.ROSInterruptException:
        pass
