#!/usr/bin/env python
import sys
import cbba,tasks
import kinematics as kn
import time
###SOCKET SEES THIS #####
class udpMessage:
	def __init__(self, msgType=None, sender=None, msg=None):
		self.type = msgType
		self.sender = sender
		self.msg = msg
		self.sendTimeStamp = -1

		#Global Converter (both must be synced)
		self.msgTypesStrToNum = {
			"Test":0,
			"SA":1,
			"PC":2,
			"MaxID":3,
			"CBBA":4,
			"TaskInfo":5,
			"CBBA1":6,
		}
		self.msgTypesNumToStr = {
			0:"Test",
			1:"SA",
			2:"PC",
			3:"MaxID",
			4:"CBBA",
			5:"TaskInfo",
			6:"CBBA1",
		}
		#self.receiver = receiver
		#Preset information?
		#self.msgSize = 500  #This is a preset maximum

	def getActualDataSize(self):
		return sys.getsizeof(self.msg)

	def getMsgLength(self):
		return self.getActualDataSize()

	def sendOnUDP(self, socket, UDP_IP, UDP_PORT):
		#First Append info about sender/type
		self.sendTimeStamp=time.time()
		newMsg = (
					str(self.msgTypesStrToNum[self.type]).zfill(1)+
					str(self.sender).zfill(2)+
					'%08.4f'%(self.sendTimeStamp%1000) +
					self.msg
				)
		# print "TS",self.sendTimeStamp
		return socket.sendto(newMsg,(UDP_IP,UDP_PORT))

	def sendOnFakeUDP(self, wifiObject=None, socket=None, UDP_IP=None, UDP_PORT=None):
		#First Append info about sender/type
		self.sendTimeStamp=time.time()

		newMsg = (
					str(self.msgTypesStrToNum[self.type]).zfill(1)+
					str(self.sender).zfill(2)+
					'%08.4f'%(self.sendTimeStamp%1000) +
					self.msg
				)
		#wifiObject.outgoingMsgQueue.append(self)
		return newMsg

	def udpToInfoObject(self, data):
	 	self.type=int(data[0])
	 	self.sender = int(data[1:3])
		self.sendTimeStamp = float(data[3:11])
	 	self.msg = data[11:]
		if self.type==self.msgTypesStrToNum["Test"]:
			IO = ConnectionTest()
		elif self.type==self.msgTypesStrToNum["SA"]:
			IO = SA()
		elif self.type==self.msgTypesStrToNum["PC"]:
			IO = PC()
		elif self.type==self.msgTypesStrToNum["MaxID"]:
			IO = MaxID()
		elif self.type==self.msgTypesStrToNum["CBBA"]:
			IO = CBBA()
		elif self.type==self.msgTypesStrToNum["TaskInfo"]:
			IO = TaskInfo()
		elif self.type==self.msgTypesStrToNum["CBBA1"]:
			IO = CBBA1()
		else:
			raise Exception("Not a valid message type")

		IO.fromUDPMessage(self.sender,self.msg)
		return IO





###########PLANNER SEE THIS ###############
# sock.SA.toUDP.data()

#General Structure of Information Object
class InformationObject(object):
	def __init__(self,typeObject=None):
		self.type = typeObject

	def toUDPMessage(self):
		#This method will return a UDP Message object that holds the IO data
		raise NotImplementedError

	def fromUDPMessage(self,sender,message):
		#This method initializes the IO from a UDP Message
		raise NotImplementedError



# Object for Concensus on MaxID
class MaxID(InformationObject):
	def __init__(self,status=None,missionID=None, sender=None,time=None,runningMax=None,msgLength=None,msgNum=None):
		InformationObject.__init__(self,"MaxID")
		self.sender = sender #Max NMB99
		self.status = status #[0=TestOn, 9=Termination]
		self.missionID = missionID
		self.time = time #Max 9999
		self.runningMax = runningMax #Max 999999 us
		self.msgLength = msgLength
		self.msgNum = msgNum

	def toUDPMessage(self):
		msgType = "MaxID"
		sender = self.sender
		msg = (
				str(self.status).zfill(1) +
				str(self.missionID).zfill(4) +
#				str(self.sender).zfill(4) +
				str(self.runningMax).zfill(8) +
				str(self.msgNum).zfill(8) +
				str(self.time).zfill(19) +
				"0"*(self.msgLength-64) # Subtract 85 to accont for other parameters
			)
		return udpMessage(msgType, sender, msg)

	def fromUDPMessage(self, sender, udpMessage):
		self.sender = sender
		self.status = int(udpMessage[0])
		self.missionID = int(udpMessage[1:5])
#		self.sender = int(udpMessage[5:9])
		self.runningMax = int(udpMessage[5:13])
		self.msgNum = int(udpMessage[13:21])
		self.time = float(udpMessage[21:40])
		self.msgLength = len(udpMessage) + 40  #3 bytes for sender,type, and 37 python overhead

#Object for CBBA
class CBBA(InformationObject):
	def __init__(self,status=None,missionID=None, sender=None,taskID = None, z_winagent=None,y_winbid=None,t_timestamp=None,msgLength=None,msgNum=None):
		InformationObject.__init__(self,"CBBA")
		self.sender = sender #Max NMB99
		self.missionID = missionID
		self.taskID = taskID
		self.msgNum = msgNum
		self.z_winagent = z_winagent
		self.y_winbid = y_winbid
		self.t_timestamp = t_timestamp


	def toUDPMessage(self):
		msgType = "CBBA"
		sender = self.sender
		msg = (
				str(self.missionID) + "," +
				str(self.msgNum) + "," +
				str(self.taskID) + "," +
				str(self.z_winagent) + ',' +
				'%0.6f' % self.y_winbid + ',' +
				'%0.6f' % self.t_timestamp + ','
			)
		return udpMessage(msgType, sender, msg)

	def fromUDPMessage(self, sender, msg):

		currentBuffer = ""
		i=0
		#msg = udpMessage.msg
		self.sender = sender
		while self.missionID == None:
			if msg[i] == ",":
				self.missionID = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.msgNum == None:
			if msg[i] == ",":
				self.msgNum = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.taskID == None:
			if msg[i] == ",":
				self.taskID = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.z_winagent == None:
			if msg[i] == ",":
				self.z_winagent = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1

		while self.y_winbid == None:
			if msg[i] == ",":
				self.y_winbid = float(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.t_timestamp == None:
			if msg[i] == ",":
				self.t_timestamp = float(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1


class TaskInfo(InformationObject):
	def __init__(self,status=None,missionID=-1, sender=99,msgNum=None,task=None):
		InformationObject.__init__(self,"TaskInfo")
		self.sender = sender #Max NMB99
		self.missionID = missionID
		self.msgNum = msgNum
		if task==None:
			#self.task = cbba.Task(None,kn.Pose(None,None),None,None,None,None)
			self.task = tasks.TimeSensitiveTask()
		else:
			self.task = task

		# self.id = taskID
		# if pose == None:
		# 	self.pose = Pose()
		# else:
		# 	self.pose = pose
		# self.reward = reward


	def toUDPMessage(self):
		msgType = "TaskInfo"
		sender = self.sender
		msg = (
				str(self.missionID) + "," +
				str(self.msgNum) + "," +
				str(self.task.id) + "," +
				'%0.5f' % self.task.pose.X + ',' +
				'%0.5f' % self.task.pose.Y + ',' +
				'%0.3f' % self.task.reward + ',' +
				'%0.4f' % self.task.ldiscount + ',' +
				'%0.5f' % self.task.tau_service + ','
			)
		return udpMessage(msgType, sender, msg)

	def fromUDPMessage(self, sender, msg):
		currentBuffer = ""
		i=0
		#msg = udpMessage.msg
		self.sender = sender
		while self.missionID == None:
			if msg[i] == ",":
				self.missionID = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.msgNum == None:
			if msg[i] == ",":
				self.msgNum = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.task.id == None:
			if msg[i] == ",":
				self.msgNum = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.task.pose.X == None:
			if msg[i] == ",":
				self.task.pose.X = float(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.task.pose.Y == None:
			if msg[i] == ",":
				self.task.pose.Y = float(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.task.reward == None:
			if msg[i] == ",":
				self.task.reward = float(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.task.ldiscount == None:
			if msg[i] == ",":
				self.task.ldiscount = float(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.task.tau_service == None:
			if msg[i] == ",":
				self.task.tau_service = float(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1


class CBBA1(InformationObject):
	def __init__(self,status=None,missionID=None, sender=None,time=None,numTasks = 0, msgLength=None,msgNum=None,z_winagents=None,y_winbids=None,t_timestamps=None):
		InformationObject.__init__(self,"CBBA1")
		self.sender = sender #Max NMB99
		self.missionID = missionID
		self.msgNum = msgNum
		self.numTasks = numTasks

		if z_winagents == None:
			self.z_winagents = []
			self.y_winbids = []
			self.t_timestamps = []
		else:
			self.z_winagents = z_winagents
			self.y_winbids = y_winbids
			self.t_timestamps = t_timestamps

	def toUDPMessage(self):
		msgType = "CBBA1"
		sender = self.sender
		msg = (
				str(self.missionID) + "," +
				str(self.msgNum) + "," +
				str(self.numTasks) + "," +
#				str(self.sender).zfill(4) +
				''.join(str(z) + ',' for z in self.z_winagents) +
				''.join('%0.6f' % y + ',' for y in self.y_winbids) +
				''.join('%0.6f' % s + ',' for s in self.t_timestamps)
			)
		return udpMessage(msgType, sender, msg)

	def fromUDPMessage(self, sender, msg):
		currentBuffer = ""
		i=0
		#msg = udpMessage.msg
		self.sender = sender
		while self.missionID == None:
			if msg[i] == ",":
				self.missionID = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while self.msgNum == None:
			if msg[i] == ",":
				self.msgNum = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		arrayBuffer = []
		while self.numTasks == 0:
			if msg[i] == ",":
				self.numTasks = int(currentBuffer)
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		arrayBuffer = []
		while len(self.z_winagents)<self.numTasks:
			if msg[i] == ",":
				self.z_winagents += [int(currentBuffer)]
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1

		while len(self.y_winbids)<self.numTasks:
			if msg[i] == ",":
				self.y_winbids += [float(currentBuffer)]
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1
		while i<len(msg):
			if msg[i] == ",":
				self.t_timestamps += [float(currentBuffer)]
				currentBuffer = ""
			else:
				currentBuffer += msg[i]
			i+=1

# Object for Connection Testing
class ConnectionTest(InformationObject):
	def __init__(self,status=None,missionID=None, msgNum=None,sender=None,totalNums=None,sleepTime=None,msgLength=None):
		InformationObject.__init__(self,"Test")
		self.status = status #[0=TestOn, 9=Termination]
		self.missionID = missionID
		self.msgNum = msgNum ##Maximum is 9999
		self.sender = sender #Max NMB99
		self.totalNums = totalNums #Max 9999
		self.sleepTime = sleepTime #Max 999999 us
		self.msgLength = msgLength #Max 1582?

	def toUDPMessage(self):
		sender = self.sender
		msgType = "Test"
		msg = (
				str(self.status) +
				str(self.missionID).zfill(4) +
				str(self.msgNum).zfill(4) +
				str(self.totalNums).zfill(4) +
				str(self.sleepTime).zfill(6) +
				"0"*(self.msgLength-52) # Subtract 10 to accont for other parameters
			)
		return udpMessage(msgType, sender, msg)

	def fromUDPMessage(self, sender, udpMessage):
		self.sender = sender
		self.status = int(udpMessage[0])
		self.missionID = int(udpMessage[1:5])
		self.msgNum = int(udpMessage[5:9])
		self.totalNums = int(udpMessage[9:13])
		self.sleepTime = int(udpMessage[13:19])


##Example usage
ct= ConnectionTest(0,111,1,4,565,400,500)
ctUDP=ct.toUDPMessage()



#TODO:  Information Object for Situational Awareness
class SA(InformationObject):
	def __init__(self,SAarray=None):
		InformationObject.__init__(self,"SA")
		self.SAarray = saArray

	def toUDPMessage(self):
		return mudpSA(sender,self.saArray)

	def fromUDPMessage(self, sender, udpMessage):
		return udpMessage(msgType, sender, msg)


#TODO:  Planned Concensus (will send the plans)
class PC(InformationObject):
	def __init__(self,PCArray=None):
		InformationObject.__init__(self,"PC")
		self.PCArray = PCArray

		#Other stuff?
		#self.bidNum = bidNum
		#self.bidTime = bidTime
		#self.bidConfidence = ##

	def toUDPMessage(self):
		msg = udpMessage()
		msg.sender = "ME"
		msg.data = saArray
		return msg

	def fromUDPMessage(self, udpMessage):
		return udpMessage(msgType, sender, msg)
