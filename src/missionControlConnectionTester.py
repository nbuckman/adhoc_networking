#!/usr/bin/env python
import rospy
import socket
import sys
import os
import time
import udpMessage
from std_msgs.msg import String, Bool, Int16
from adhoc_networking.msg import maxIDMissionSpecs, MissionSentResults, MissionRcvdResults, UdpROSMsg
import cPickle as pickle

#Title: missionControlMaxID.py
#Author: Noam Buckman, 3/2017
#Descr:  This node will both send mission specifications (before the mission) and record the experiment results (after the mission)
def missionControlMaxID():
    rospy.init_node('missionControl', anonymous=True)
    rate = rospy.Rate(1) # 1hz

    #Mission control will send mission stats to other agents
    missionSpecsPub = rospy.Publisher('missionSpecs',  maxIDMissionSpecs, queue_size=10)
    missionRunningPub = rospy.Publisher('missionRunning', Bool, queue_size=1)

    missionResultsSentSub = rospy.Subscriber('missionSentResults',MissionSentResults, missionSentResultsCallback)
    missionResultsRcvdSub = rospy.Subscriber('missionRcvdResults',MissionRcvdResults, missionRcvdResultsCallback)
    agentReadySub = rospy.Subscriber('agentReady',Int16, agentReadyCallback)
    #Mission Specifications
    currentMissionID = 0
    currentActiveAgents = []
    curentSleepTime = 0
    currentMsgSize = 0
    currentNumMsgs = 0

    #Mission Specifications:
    activeAgentArray = [1,2,3,4,5,6]
    sendRates = [1]
    msgLengths = [1000]
    numTrials = 12
    experimentLength = 120
    N = len(activeAgentArray)

    #These Are Fixed For Now
    updateIDRateArray = [0]*N
    rcvRateArray = [200]*N


    currentMissionRunning=False
    rospy.sleep(2)  #FOR SOME REASON THIS IF VERY IMPORTANT!
    experimentRunning=True
    while experimentRunning and not rospy.is_shutdown():
        for trialI in range(numTrials):
            for msgLength in msgLengths:
                for sendRate in sendRates:
                    for sendAgent in activeAgentArray:
                        while currentMissionRunning and not rospy.is_shutdown():
                            #Mission is still going on, don't do anything!
                            rospy.spin()
                        #Previous mission done, move onto next one
                        agentsSentDoneSet.clear()
                        agentsRcvdDoneSet.clear()
                        agentsReadySet.clear()
                        currentMissionID += 1
                        print "Sending a new mission! Mission #", currentMissionID
                        sendRateArray = [0]*N
                        sendRateArray[sendAgent-1] = sendRate
                        print "Agents:", activeAgentArray, "updateIDRate (Hz)", updateIDRateArray, "rcvRate (Hz)", rcvRateArray, "sendRate: ", sendRateArray, "Msg Size", msgLength
                        newMission = maxIDMissionSpecs()
                        newMission.missionID = currentMissionID
                        newMission.activeAgents = activeAgentArray
                        newMission.changeIDRates = updateIDRateArray
                        newMission.sendRates = sendRateArray
                        newMission.rcvRates = rcvRateArray
                        newMission.msgLength = msgLength
                        newMission.experimentLength = experimentLength
                        missionSpecsPub.publish(newMission)
                        while (agentsReadySet != set(activeAgentArray)) and not rospy.is_shutdown():
                            rospy.sleep(1)
                        print "Starting Mission"
                        missionRunningPub.publish(True)
                        rospy.sleep(experimentLength) #For me
                        missionRunningPub.publish(False)
                        print "Mission Ended"
                        while (agentsSentDoneSet != set(activeAgentArray) or agentsRcvdDoneSet != set(activeAgentArray)) and not rospy.is_shutdown():
                            rospy.sleep(1) #Wait to get data from everyone
                        ############################################
                        print "Mission Over: Saving Data"
                        if len(sentData)!=0 and len(rcvdData)!=0:
                            sentFileName = resultsDirectory + 'sent' + "t" + str(trialI) + "sRate" + str(sendRate) + "mLen" + str(msgLength) + "agent" + str(sendAgent) +  '.p'
                            sentFile = open(sentFileName,'wb')
                            pickle.dump(sentData,sentFile)
                            sentFile.close()
                            rcvdFileName = resultsDirectory + 'rcvd' + "t" + str(trialI) + "sRate" + str(sendRate) + "mLen" + str(msgLength) + "agent" + str(sendAgent) +  '.p'
                            rcvdFile = open(rcvdFileName,'wb')
                            pickle.dump(rcvdData,rcvdFile)
                            rcvdFile.close()
                            print "Filename:", sentFileName
                        else:
                            print "No data received"
                        rospy.sleep(2)
                        ############################################
        experimentRunning=False
        missionSpecsPub.publish(-99,[1],[1]*len(activeAgentArray),[1]*len(activeAgentArray),[1]*len(activeAgentArray),1,1) #This tells the agents to terminate the mission!
        rate.sleep()
        print "Shutting Down:  Good Night!"


def missionSentResultsCallback(data):
    #Save the results
    #currentMissionSentResults[data.agentID].append((data.missionID,data.sendRate,data.agentsReceiveRate,data.agentsSuccessRate))
    global agentsSentDoneSet, sentData
    if data.agentID not in agentsSentDoneSet:
        sentData.append(data)
        agentsSentDoneSet.add(data.agentID)
        print "Subscribed to SENT MSGS from", data.agentID

def missionRcvdResultsCallback(data):
    #Save the results
    # currentMissionRcvResults[data.agentID] = (data.missionID, data.sendRate, data.updateIDRate,data.bytesSent,data.sentMsgs)
    # agentsRcvdDoneList += data.agentID
    global agentsRcvdDoneSet, rcvdData
    if data.agentID not in agentsRcvdDoneSet:
        rcvdData.append(data)
        agentsRcvdDoneSet.add(data.agentID)
        print "Subscribed to RCVD MSGS from", data.agentID

def agentReadyCallback(data):
    global agentsReadySet
    agentRcvdID = data.data
    agentsReadySet.add(agentRcvdID)
    print "Agent", agentRcvdID, "Ready!"

if __name__ == '__main__':
    try:
        agentsSentDoneSet = set([])
        agentsRcvdDoneSet = set([])
        agentsReadySet = set([])
        currentMissionResults = dict()
        sentData = []
        rcvdData = []

        resultsDirectory = '/home/nbuckman/DropboxMIT/ACL/catkin_ws/src/adhoc_networking/results/ConnectionTestMay02/'
        missionControlMaxID()
    except rospy.ROSInterruptException:
        pass
