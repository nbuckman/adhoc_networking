#!/usr/bin/env python
import rospy
import socket
import sys
import os
import datetime
import udpMessage
from std_msgs.msg import String, Bool
from adhoc_networking.msg import networkTestSpecs, networkTestResults


#Title: connectionTestListener_pi.py
#Author: Noam Buckman, 1/2017
#Descr:  This node will broadcast UDP connection test objects

def udpListener():
    #####Generic Setup:
    rospy.init_node('udpListener', anonymous=True)
    rate = rospy.Rate(100000) # 1hz

    missionResultsPub = rospy.Publisher('missionNetworkResults',networkTestResults, queue_size=1010)

    if 'AGENT_NAME' not in os.environ.keys():
        raise Exception("You need to source the Environment.sh")
    agentIDstr=os.environ['AGENT_NAME'][-1]
    agentIDint = int(agentIDstr)
    numOfAgents = 9  #This is more of a global maximum since we don't need all the values

    ##############################################################################

    # S1) Setting up the UDP Socket
    UDP_IP = "10.10.3.255"
    UDP_PORT = 5005
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP,UDP_PORT)) #This allows us to listen
    print "UDP Socket Open.  IP:", UDP_IP, " Port:", UDP_PORT


    # S2) Initialize The Agent lists that will hold all the important information, indexed by sender
    startTimeAgents = [-1]*(numOfAgents + 1)   #time of first message rcvd / start time
    startMsgNumAgents = [0]*(numOfAgents + 1) #msg num of first message rcvd
    timeRcvdMsgsAgents = [0]*(numOfAgents+1) #time elapsed so far
    numRcvdMsgsAgents = [0]*(numOfAgents+1)  #number of msgs I've received so far from each person
    finalTimeAgents = [0]*(numOfAgents+1)    #time of first termination msg / end time
    currentMissionIDAgents = [-1]*(numOfAgents + 1)
    newAgentDone = [False]*(numOfAgents + 1) #Keeps track of which agents we've already pub results

    # S3) Initialize the udp message that holds incoming information
    udpMsg = udpMessage.udpMessage()
    counter=0
    missionDone=False
    print "Mission Starting"

    ##############################################################################

    while not rospy.is_shutdown():
        (data, addr) = sock.recvfrom(1024)
        informationObject = udpMsg.udpToInfoObject(data)
        sender = informationObject.sender
        msgNum = informationObject.msgNum
        missionID = informationObject.missionID

        if sender!=agentIDint: #Ignore if receiving from myself
            #1) New Mission: Reset Everything
            if missionID!=currentMissionIDAgents[sender]:
                startTimeAgents[sender]=rospy.get_time()
                startMsgNumAgents[sender] = msgNum
                curentMissionIDAgents[sender] = missionID
                numRcvdMsgsAgents[sender] = 0
                newAgentDone[sender] = True

            #2) Keep count of # msgs rcvd
            numRcvdMsgsAgents[sender]+=1

            #3) Mission Is Done. Record Results
            if informationObject.status==9 and newAgentDone[sender]:  #Todo: Softcode the 0/9 coding
                print "Agent", sender, " Done"
                #Save current time
                timeRcvdMsgsAgents[sender]=rospy.get_time()
                newAgentDone[sender] = False

                #3b) Save and Send Results
                agentResults = networkTestResults()
                agentResults.missionID = missionID #Set
                agentResults.senderID = sender
                agentResults.receiverID = agemtID
                agentResults.agentMsgsRcvd  = finishMsgNum[sender] - startMsgNum[sender]
                agentResults.agentDurationRcvd = timRcvdMsg[sender] - startTime[sender]
                missionResultsPub.publish(agentResults)
                print "SENT RESULTS"
        rate.sleep() #This is fixed, do this as fast as possible


if __name__ == '__main__':
    try:
        udpListener()
    except rospy.ROSInterruptException:
        pass
