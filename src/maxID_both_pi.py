#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage #Should this be from udpMessage import maxIDMessage?
import threading
import cPickle as pickle
from adhoc_networking.msg import maxIDMissionSpecs, MissionSentResults, MissionRcvdResults, UdpROSMsg #Should this be CAP?

#Title: maxID_both_pi.py
#Author: Noam Buckman, 3/2017
#Descr:  This node will broadcast and update my running max

def maxIDNode():
    rospy.init_node('maxIDNode', anonymous=True)
    print "Agent", agentIDint
    rospy.on_shutdown(onShutdownSave)
    global currentMissionID
    #This subscriber will rcv runningMax from runningMaxPublisher
    agentReadyPublisher = rospy.Publisher('agentReady',Int16,queue_size=1)
    missionSpecsSubscriber = rospy.Subscriber('missionSpecs', maxIDMissionSpecs, missionSpecsCallback, agentReadyPublisher)
    missionRunningSubscriber = rospy.Subscriber('missionRunning', Bool, missionRunningCallBack)

    while currentMissionID == -1 and not rospy.is_shutdown():
        print "Waiting for Mission Specifications..."
        rospy.sleep(1)
    ################################################3

    #Threading
    t = threading.Thread(target=Sending)
    t.daemon = True
    t.start()
    t2 = threading.Thread(target=Receiving)
    t2.daemon = True
    t2.start()
    t3 = threading.Thread(target=IDChanging)
    t3.daemon = True
    t3.start()
    ######################################################
    while experimentRunning and not rospy.is_shutdown():
        rospy.spin()

def Sending():
    if sendHz>0.000001:
        sendRate = rospy.Rate(sendHz)
    spinRateS = rospy.Rate(20)
    sentMsgs = []
    sentBytes = []
    msgNum = 0
    publishedSent = True
    missionSentResultsPublisher = rospy.Publisher('missionSentResults', MissionSentResults, queue_size=10000)
    while experimentRunning and not rospy.is_shutdown():
        if missionRunning:
            if sendHz<0.00001:
                spinRateS.sleep()
                sendRate = rospy.Rate(1)
            else:
                maxIDOutMsg = udpMessage.MaxID()
                maxIDOutMsg.status = 0
                maxIDOutMsg.missionID = currentMissionID
                maxIDOutMsg.sender = agentIDint
                maxIDOutMsg.time = rospy.get_time()
                maxIDOutMsg.runningMax = currentRunningMax
                maxIDOutMsg.msgLength = msgLength
                maxIDOutMsg.msgNum = msgNum
                msgNum +=1
                cMaxAvgUDP = maxIDOutMsg.toUDPMessage()
                #Send the msg
                try:
                    sent = cMaxAvgUDP.sendOnUDP(sock,UDP_IP,UDP_PORT)
                    sentMsgs.append(maxIDOutMsg) #Record the Data
                    sentBytes.append(sent)
                except socket.error:
                    #print "Couldn't send"
                    pass
                sendRate.sleep()
        else:
            global publishedSent
            if sendHz>0.000001:
                sendRate = rospy.Rate(sendHz)
            if newMission:
                sentMsgs = []
                sentBytes = []
                msgNum = 0
            elif not publishedSent: #End of Old Mission
                rosSentMsgs = createROSMsgList(sentMsgs)
                sentResults = MissionSentResults()
                sentResults.missionID = currentMissionID
                sentResults.agentID = agentIDint
                sentResults.missionSpecs = currentMissionSpecs
                sentResults.sentMsgs = rosSentMsgs
                sentResults.sentBytes = sentBytes
                missionSentResultsPublisher.publish(sentResults)
                print "Published Sent Messages"
                publishedSent = True
                sentMsgs = []
                sentBytes = []
                msgNum = 0
            spinRateS.sleep()



def Receiving():
    receiveRate = rospy.Rate(rcvHz)
    spinRateR = rospy.Rate(1)
    rcvdMsgs = []
    rcvdBytes = []
    publishedRcvd = True
    missionRcvdResultsPublisher = rospy.Publisher('missionRcvdResults', MissionRcvdResults, queue_size=10000)
    while experimentRunning and not rospy.is_shutdown():
        if missionRunning:
            try:
                (data, addr) = sock.recvfrom(msgLength)
                udp = udpMessage.udpMessage()
                maxIDRcvMsg = udp.udpToInfoObject(data)
                sender = maxIDRcvMsg.sender
                if sender!=agentIDint: #Ignore if receiving from myself
                    receivedRunningMax = maxIDRcvMsg.runningMax
                    maxIDRcvMsg.msgLength = sys.getsizeof(data)
                    global currentRunningMax
                    currentRunningMax = max(currentRunningMax,receivedRunningMax)
                    rcvdMsgs.append(maxIDRcvMsg)
            except socket.error:
                #print "No Data"
                pass
            receiveRate.sleep()
        else:
            global publishedRcvd
            if newMission:  #NOT SURE WHAT THIS IS DOING?!?!?!?!?!
                receiveRate = rospy.Rate(rcvHz)
                rcvdMsgs = []
                rcvdBytes = []
            elif not publishedRcvd:
                rosRcvdMsgs = createROSMsgList(rcvdMsgs)
                rcvdResults = MissionRcvdResults()
                rcvdResults.missionID = currentMissionID
                rcvdResults.agentID = agentIDint
                rcvdResults.missionSpecs = currentMissionSpecs
                rcvdResults.rcvdMsgs = rosRcvdMsgs
                #rcvdResults.sentBytes = sentBytes
                missionRcvdResultsPublisher.publish(rcvdResults)
                print "Published RCVD Messages"
                rcvdMsgs = []
                rcvdBytes = []
                publishedRcvd = True
            spinRateR.sleep()


def IDChanging():
    changingID = agentIDint
    if changeIDHz<=0.00001:
        changeIDRate = rospy.Rate(1)
    else:
        changeIDRate = rospy.Rate(changeIDHz)
    currentRunningMaxHistory = []
    #print "Update Rate:", changeIDHz, "Hz"
    while experimentRunning and not rospy.is_shutdown():
        if missionRunning:
            if changeIDHz>0.00001:
                changingID += 1
                global currentRunningMax
                currentRunningMax = max(currentRunningMax,changingID)
                currentRunningMaxHistory.append((rospy.get_time(),currentRunningMaxHistory))
            changeIDRate.sleep()
        else:
            global currentRunningMax
            currentRunningMax = 0
            changingID = agentIDint
            currentRunningMaxHistory = []
            if changeIDHz<=0.00001:
                changeIDRate = rospy.Rate(1)
            else:
                changeIDRate = rospy.Rate(changeIDHz)

def missionSpecsCallback(missionSpecs,agentReadyPub):
    #Update Mission Specifications and Start a New Mission
    if currentMissionID != missionSpecs.missionID:
        global currentMissionID, activeAgents, changeIDHz, sendHz, rcvHz, msgLength, currentRunningMax, missionRunning, currentMissionSpecs
        currentMissionSpecs = missionSpecs

        currentMissionID = missionSpecs.missionID
        activeAgents = missionSpecs.activeAgents
        changeIDHz = missionSpecs.changeIDRates[agentIDint-1]
        sendHz = missionSpecs.sendRates[agentIDint-1] # 10hz
        rcvHz = missionSpecs.rcvRates[agentIDint-1]
        msgLength = missionSpecs.msgLength
        currentRunningMax = agentIDint #Reset the currentRunningMax
        print "New Mission Specs:", currentMissionID
        print "Agents:", activeAgents, "changeIDRate (Hz)", changeIDHz, "rcvRate (Hz)", rcvHz, "sendRate: ", sendHz, "Msg Length", msgLength
        agentReadyPub.publish(agentIDint)
        if missionSpecs.missionID==-99:
            global experimentRunning           #Experiment Over
            experimentRunning=False

def missionRunningCallBack(missionRunningStatus):
    global missionRunning, newMission, publishedSent, publishedRcvd
    random.seed(agentIDint)
    if missionRunningStatus.data==True: #Random start
        rospy.sleep(random.random())
    missionRunning = missionRunningStatus.data
    if missionRunning:
        print "Starting Mission!"
        newMission = False
    else:
        print "Mission over!  Final Running Max", currentRunningMax
        publishedSent = False
        publishedRcvd = False


def createROSMsgList(udpMsgs):
    listOfSentROSMsgs = []
    for msg in udpMsgs:
        #This could be a method of the UDP Message Class

        rosMsg = UdpROSMsg() #Publish the msg
        rosMsg.status=msg.status
        rosMsg.missionID=msg.missionID
        rosMsg.sender=msg.sender
        rosMsg.receiver=-1
        rosMsg.time=msg.time
        rosMsg.runningMax=msg.runningMax
        rosMsg.length=msg.msgLength
        rosMsg.msgNum=msg.msgNum

        listOfSentROSMsgs.append(rosMsg)
    print "Size of Ros Msgs=", len(listOfSentROSMsgs)
    return listOfSentROSMsgs


def onShutdownSave():
    print "Shutting Down!"


if __name__ == '__main__':
    try:
        ##########################################
        #Global Variables
        agentIDstr=os.environ['AGENT_NAME'][-1]
        agentIDint = int(agentIDstr)
        agentInternalValue = agentIDint

        currentRunningMax = agentIDint
        msgNum = 0
        bytesSent = 0
        experimentRunning = True
        missionRunning = False
        amountSent = 0
        newMission = True
        publishedRcvd = True
        publishedRcvd = True

        #Mission Spec Initialization
        currentMissionSpecs = None
        currentMissionID = -1
        activeAgents = [1,2,3,4]
        changeIDHz = 1
        sendHz = 20 # 10hz
        receiveHz = 10

        msgLength = 800
        experimentLength = 10


        #################################
        #Setting up the UDP Socket
        UDP_IP = "10.10.3.255"
        UDP_PORT = 5005
        sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
        sock.bind((UDP_IP,UDP_PORT))
        sock.setblocking(0) #Don't block
        print "UDP target IP:", UDP_IP, "port:", UDP_PORT
        ####################################
        maxIDNode()
    except rospy.ROSInterruptException:
        pass
