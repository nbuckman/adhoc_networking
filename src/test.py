#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage #Should this be from udpMessage import maxIDMessage?
import threading
import cPickle
from adhoc_networking.msg import leaderElectionMissionSpecs #Should this be CAP?

#Title: broadcastTestNode_pi.py
#Author: Noam Buckman, 3/2017
#Descr:  This node will broadcast and update my running nax

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    
def maxIDNode():
    rospy.init_node('maxIDNode', anonymous=True)
    print "Agent", agentIDint
    random.seed(agentIDint)
    rospy.on_shutdown(onShutdownSave)

    #This subscriber will rcv runningMax from runningMaxPublisher
    runningMaxSubscriber = rospy.Subscriber('runningMaxReceived', Int16, runningMaxUpdate)
    missionSpecsSubscriber = rospy.Subscriber('missionSpecs', leaderElectionMissionSpecs, missionSpecsCallback)
    rospy.Subscriber('chatter', String, callback)
    missionDoneSubscriber = rospy.Subscriber('missionDone', Bool, missionDoneCallBack)

    #rcvdMsgPublisher = rospy.publisher('rcvdMsgList', List, queue_size=1) #These communicate to Master
    #sendMsgPublisher = rospy.publisher('sentMsgList', List, queue_size=1) #These communicate to Master
    startTime = rospy.get_time()

    rospy.sleep(5)
    #sentMsgsPublisher.pub(sentMsgs)
    saveFile1 = False #Only do this once
    ################################################3
    #Threading
    # t = threading.Thread(target=Sending)
    # t.daemon = True
    # t.start()
    # t2 = threading.Thread(target=Receiving)
    # t2.daemon = True
    # t2.start()
    ######################################################
    print "Spinning"
    while experimentRunning and not rospy.is_shutdown():
        rospy.spin()


        # while ((missionDone) or (agentIDint not in activeAgents)) and not rospy.is_shutdown():
        #     rospy.spin()

# def Sending():
#     broadcastRate = rospy.Rate(20) # 10hz
#     slowRate = rospy.Rate(0.1)
#     sentMsgs = []
#     while True and not rospy.is_shutdown():
#         if not missionDone:
#             global msgNum
#             maxIDOutMsg = udpMessage.MaxID()
#             maxIDOutMsg.status = 0
#             maxIDOutMsg.missionID = currentMissionID
#             maxIDOutMsg.sender = agentIDint
#             maxIDOutMsg.time = rospy.get_time()
#             maxIDOutMsg.runningMax = currentRunningMax
#             maxIDOutMsg.msgLength = msgSize
#             maxIDOutMsg.msgNum = msgNum
#             msgNum +=1
#             sentMsgs.append(maxIDOutMsg)
#             cMaxAvgUDP = maxIDOutMsg.toUDPMessage()
#             sent = cMaxAvgUDP.sendOnUDP(sock,UDP_IP,UDP_PORT)
#             print "Bytes", sent
#             sentMsgs.append(sent) #
#             slowRate.sleep()
#         else:
#             if saveFile1:
#                 fileName1 = 'sent' + str(missionID) + '.p'
#                 f1 = open(fileName1,'wb')
#                 cPickle.dump(rcvdMsgs,f1)
#                 f1.close()
#                 #sentMsgsPublisher.pub(sentMsgs)
#                 sentMsgs = []
#                 saveFile1 = False #Only do this once
#
#
#
# def Receiving():
#     slowRate = rospy.Rate(0.1)
#     rcvdMsgs = []
#     while True and not rospy.is_shutdown():
#         if not missionDone:
#             (data, addr) = sock.recvfrom(1024)
#             if not data:
#                 break
#             else:
#                 #print data
#                 udp = udpMessage.udpMessage()
#                 maxIDRcvMsg = udp.udpToInfoObject(data)
#                 rcvdMsgs.append(maxIDRcvMsg)
#                 sender = maxIDRcvMsg.sender
#                 if sender!=agentIDint: #Ignore if receiving from myself
#                     receivedRunningMax = maxIDRcvMsg.runningMax
#                     global currentRunningMax
#                     currentRunningMax = max(currentRunningMax,receivedRunningMax)
#                     print currentRunningMax
#         else:
#             if saveFile2:
#                 fileName = 'rcvd' + str(missionID) + '.p'
#                 f = open(fileName,'wb')
#                 cPickle.dump(rcvdMsgs,f)
#                 f.close()
#                 #rcvMsgsPublisher.pub(rcvdMsgs)
#                 rcvMsgs = []
#                 saveFile2 = False
#         slowRate.sleep()


def missionSpecsCallback(missionSpecs):
    #Update Mission Specifications and Start a New Mission
    print "RECEIVED A MISSION!"
    # global currentMissionID
    # if currentMissionID != missionSpecs.missionID:
    #     global activeAgents, sleepDelay, msgSize, missionDone, msgCounter, amountSent, currentRunningMax
    #     currentMissionID = missionSpecs.missionID
    #     activeAgents = missionSpecs.activeAgents
    #     sleepDelay = missionSpecs.sleep_us
    #     msgSize = missionSpecs.message_size
    #     missionDone = False
    #     msgCounter = 0
    #     amountSent = 0
    #     currentRunningMax = agentIDint #Reset the currentRunningMax

def runningMaxUpdate(runningMax):
    print "RunningMaxU"
    # print "Rcvd:", runningMax   #At some point should also send who sent the max
    # global currentRunningMax
    # currentRunningMax = max(runningMax,currentRunningMax)
    # print "New:", currentRunningMax

def missionDoneCallBack(missionDoneStatus):
    print "MISSION DONE!!!"
    #missionDone = missionDoneStatus
    #if missionDone:
        #saveFile1 = True
        #saveFile2 = True

def onShutdownSave():
    print "SAVING DATA!!"
    x = [1,2,3,4,5,6,8]
    fileName1 = 'shutdown' + str(len(x)) + '.p'
    f1 = open(fileName1,'wb')
    cPickle.dump(x,f1)
    f1.close()


if __name__ == '__main__':
    try:
        ##########################################
        #Global Variables
        agentIDstr=os.environ['AGENT_NAME'][-1]
        agentIDint = int(agentIDstr)
        currentMissionID = 1
        currentRunningMax = agentIDint
        activeAgents = [1,2,3,4,5,6]
        sleepDelay = 100
        msgSize = 800
        numOfMsgs = 500
        msgNum = 0
        missionDone = False
        amountSent = 0
        experimentRunning = True
        #################################
        #Setting up the UDP Socket
        UDP_IP = "10.10.3.255"
        UDP_PORT = 5005
        sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
        sock.bind((UDP_IP,UDP_PORT))
        #sock.setblocking(0) #Don't block
        print "UDP target IP:", UDP_IP, "port:", UDP_PORT
        ####################################
        maxIDNode()
    except rospy.ROSInterruptException:
        pass



######################################################
#OLD
######################################################
    #Mission Setup
    # msgSize = (800-22)
    # numOfMsgs = 500
    # numOfAgents = 9

    #Load mission parameters
    # msgSize = rospy.get_param("/msgSize",800)
    # numOfMsgs = rospy.get_param("/numMsgs",500)
    # p = rospy.get_param("pSend",1.0)
    # activeAgents = rospy.get_param("/activeSenders",[])
    # numOfAgents = rospy.get_param("/numAgents",9)
    # numTermMsgs = rospy.get_param("/numTermMsgs",1000)
