import copy
import kinematics as kn
import numpy as np
import time
import tasks
import random
"""
Created on December 2017
@author: Noam Buckman
"""

class CBBAManager:
    """
    The CBBA Manager mantains the bundles, paths, and bidding methods
    """
    def __init__(self, agentID, L_maxTasks,pathValueF,nTasks=100,nAgents=6):
        """
        agentID: agent ID
        L_maxTasks: maximum number of tasks
        pathValueF: Method that evaluates path values
        """
        #Provide:  max #of tasks for agent
        #          Function to calculate pathValue
        self.agentID = agentID
        self.L_maxTasks = L_maxTasks
        self.pathValueF = pathValueF
        self.tasksList = []
        self.nTasks = nTasks
        self.nAgents = nAgents

        self.taskAbilitySet = set(range(self.nTasks))
        self.cbbaLocalTime = 0 #Todo this should be temp
        self.resetFull = True
        self.bundleResetNum = -1

        #THESE ARE ACCSESED ONLY TO LISTENER
        self.b_bundle = [] #Ordered by which added first
        self.p_path = [] #Ordered by location in assignment
        self.z_winagents = [-1 for i in range(self.nTasks)]
        self.y_winbids = [0 for i in range(self.nTasks)]
        self.t_timestamps = [0 for i in range(self.nTasks)]

        #ACBBA Aditions
        self.listenerBufferBool = True
        self.incomingBuffers = {True: None, False: None}
        self.outgoingBuffers = {True: [], False: []}

        self.toBroadcastBuffer = []

    def initializeLists(self,tasksList,nRobots):
        '''Initializes z,y,t arrays needed for CBBA with information from simulator'''
        self.tasksList = tasksList
        self.nTasks = len(tasksList)
        self.z_winagents = [-1 for i in range(self.nTasks)] #Dict of tasks and the highest bidder so far (agent)
        self.y_winbids = [0 for i in range(self.nTasks)]   #Dict of tasks and the highest bids so far
        self.t_timestamps = [0 for i in range(self.nTasks)] #Init time stamp 0

    def updateTaskLists(self,task):
        '''Updates the task lists (y,z,t) when a new task arrives'''
        if task.id not in set([t.id for t in self.tasksList]):
            #Modify size of internal arrays
            extendN = 1
            self.tasksList.append(task)
            self.z_winagents.extend([-1]*extendN)
            self.y_winbids.extend([0]*extendN)
            self.t_timestamps.extend([0]*extendN)
            if self.bundleResetNum>-1:
                if self.bundleResetNum >= self.L_maxTasks:
                    print "RESETTING ALL CBBA BIDS, New task"
                    self.b_bundle = []
                    self.p_path = []
                    self.z_winagents = [-1 for i in range(len(self.z_winagents))] #Dict of tasks and the highest bidder so far (agent)
                    self.y_winbids = [0 for i in range(len(self.z_winagents))]   #Dict of tasks and the highest bids so far
                    self.t_timestamps = [0 for i in range(len(self.z_winagents))] #Init time stamp 0
                    self.incomingBuffers[not self.listenerBufferBool] = (copy.deepcopy(self.y_winbids),copy.deepcopy(self.z_winagents),copy.deepcopy(self.t_timestamps),copy.deepcopy(self.b_bundle),copy.deepcopy(self.p_path),copy.deepcopy(self.tasksList))
                else:
                    print "Partial RESETTING CBBA BIDS, New task"
                    for t in self.b_bundle[-self.bundleResetNum:]:
                        self.z_winagents[t] = -1
                        self.y_winbids[t] = 0
                        self.t_timestamps[t] = 0
                        self.p_path.remove(t)
                    self.b_bundle = self.b_bundle[:-self.bundleResetNum]
            elif self.globalResetNum>-1:
                print "Reset ", self.globalResetNum, "Global Tasks"
                sorted_t_y = np.argsort(self.y_winbids)
                sorted_y_y = sorted(self.y_winbids)
                maxYreset = sorted_y_y[self.globalResetNum]
                print "Reset tasks y<=", maxYreset
                for t in self.b_bundle:
                    # if self.y_winbids[t]<=self.taskResetY:
                    #     print "R","j=",t,"y=",self.y_winbids[t]
                    self.p_path.remove(t)
                # print "B,", self.b_bundle, "P", self.p_path
                # print "Y", '%0.03f'%self.y_winbids
            else:
                print "No Reset: Insertion"
            if len(self.b_bundle)!=len(self.p_path):
                print "Reset Issue, b_bundle != p_path"
            self.incomingBuffers[self.listenerBufferBool] = (copy.deepcopy(self.y_winbids),copy.deepcopy(self.z_winagents),copy.deepcopy(self.t_timestamps),copy.deepcopy(self.b_bundle),copy.deepcopy(self.p_path),copy.deepcopy(self.tasksList))

    def listenerCBBA1(self,rcvdMessages,Hz=10,printTasks=False):
        '''
        Description: Listener module running the de-conflicting of messages

        Input:  list of rcbdMessages
        Output:  void, rebroadcasts messages
        '''

        if len(rcvdMessages)>0:   #TODO: Naming conventions need to get cleaner
            incomingCBBABid = rcvdMessages.pop(0) #THIS IS FROM WIFI, ASSUME NOW ITS A BID
            k = incomingCBBABid.sender
            j = incomingCBBABid.taskID

            z_kj = incomingCBBABid.z_winagent
            y_kj = incomingCBBABid.y_winbid
            t_kj = incomingCBBABid.t_timestamp
            i=self.agentID
            newTask = False
            if j>=len(self.z_winagents): #New task,
                #I RECEIVED BID BUD I DONT KNOW THE TASK, JUST BROADCAST THE BID
                taskBidInfo = TaskBidInfo(k,j,y_kj,z_kj,t_kj)
                print "AG %i, t: %0.4f:  Recvd Bid about New Task I don't know " % (self.agentID, self.cbbaLocalTime)
                self.rebroadcast(taskBidInfo)
                return
                # extendN = 1
                # self.z_winagents.extend([-1]*extendN)
                # self.y_winbids.extend([0]*extendN)
                # self.t_timestamps.extend([0]*extendN)
                # newTask = True
            z_ij = self.z_winagents[j]
            y_ij = self.y_winbids[j]
            t_ij = self.t_timestamps[j]
            if printTasks:
                print "k,i,j", k, i, j
                print "y_kj, y_ij", y_kj, y_ij

            myTaskBidInfo = TaskBidInfo(i,j,y_ij,z_ij,t_ij)
            taskBidInfo = TaskBidInfo(k,j,y_kj,z_kj,t_kj)

            if newTask:
                if self.resetFull:
                    j = self.b_bundle[0]
                    myTaskBidInfo = TaskBidInfo(i,j,y_ij,z_ij,t_ij)
                    print "N"
                    self.resetAndRebroadcast(taskBidInfo)
                elif self.resetPartial>0:
                    j = self.b_bundle[-self.resetPartial]
                    myTaskBidInfo = TaskBidInfo(i,j,y_ij,z_ij,t_ij)
                    print "NP"
                    self.resetAndRebroadcast(taskBidInfo)

            ##########################ACBBA TABLE 1##########################
            #This is taken straight from Johnson '10 Table 1
            eps_t = 0.01 #Setting eps_t needed
            if z_kj==k:
                if z_ij==i:
                    #print "zkj=k,zij!=k"
                    if (y_kj>y_ij or (abs(y_kj-y_ij)<0.0001 and k>i)):
                        self.updateAndRebroadcast(taskBidInfo)
                    elif abs(y_kj-y_ij)<0.0001 and z_kj < z_ij:
                        self.updateAndRebroadcast(taskBidInfo)
                    elif y_kj < y_ij:
                        self.updateTimeAndRebroadcast(myTaskBidInfo)
                    else:
                        self.default(myTaskBidInfo)
                elif z_ij==k:
                    if t_kj - t_ij > eps_t:
                        self.updateAndNoRebroadcast(taskBidInfo)
                    elif abs(t_kj-t_ij)<=eps_t:
                        self.leaveAndNoRebroadcast()
                    elif t_ij - t_kj > eps_t:
                        self.leaveAndNoRebroadcast()
                    else:
                        self.default(myTaskBidInfo)
                    #Different than the paper>> #self.update(j, y_kj,z_kj)
                elif z_ij==-1:
                    self.updateAndRebroadcast(taskBidInfo)
                else: #M not i,k
                    #print "j: z_ij, z_kj, i, k"
                    #print j,":",z_ij, z_kj, i, k
                    # print "zkj=k,zij!=k"
                    if ((y_kj>y_ij) or (abs(y_kj-y_ij)<0.0001 and k>i)):
                        if t_kj - t_ij >= -eps_t:
                            self.updateAndRebroadcast(taskBidInfo)
                        else:
                            #print "1 "
                            self.resetAndRebroadcast(taskBidInfo)
                    else:
                        if t_kj<=t_ij:
                            self.leaveAndRebroadcast(myTaskBidInfo)
                        else:
                            #print "2 "
                            self.resetAndRebroadcast(taskBidInfo)
            elif z_kj==i:
                if z_ij==i:
                    if abs(t_kj-t_ij)<eps_t:
                        self.leaveAndNoRebroadcast()
                    else:
                        self.default(myTaskBidInfo)
                elif z_ij==k:
                    # print "zkj=k,zij!=k"
                    #print "3 "
                    self.resetAndRebroadcast(taskBidInfo)
                elif z_ij==-1:
                    self.leaveAndRebroadcast(myTaskBidInfo)
                else:
                    self.leaveAndRebroadcast(myTaskBidInfo)
            elif z_kj==-1:
                if z_ij==i:
                    self.leaveAndRebroadcast(myTaskBidInfo)
                elif z_ij==k:
                    self.updateAndRebroadcast(taskBidInfo)
                elif z_ij==-1:
                    self.leaveAndNoRebroadcast()
                else:
                    m=z_ij
                    if (t_kj - t_ij > eps_t):
                        self.updateAndRebroadcast(taskBidInfo)
                    else:
                        self.default(myTaskBidInfo)
            else:
                m=z_kj
                if z_ij==i:
                    if (y_kj>y_ij or (abs(y_kj-y_ij)<0.0001 and k>i)):
                        self.updateAndRebroadcast(taskBidInfo)
                    else:
                        self.updateTimeAndRebroadcast(myTaskBidInfo)
                elif z_ij==k:
                    if t_kj - t_ij >= -eps_t:
                        self.updateAndRebroadcast(taskBidInfo)
                    elif t_ij - t_kj > eps_t:
                        #print "4 "
                        self.resetAndRebroadcast(taskBidInfo)
                    else:
                        self.default(myTaskBidInfo)
                elif z_ij==m:
                    if t_kj - t_ij > eps_t:
                        self.updateAndNoRebroadcast(taskBidInfo)
                    elif abs(t_kj-t_ij)<=eps_t:
                        self.leaveAndNoRebroadcast()
                    elif t_ij - t_ij > eps_t:
                        self.leaveAndNoRebroadcast()
                    else:
                        self.default(myTaskBidInfo)
                elif z_ij==-1:
                    self.updateAndRebroadcast(taskBidInfo)
                else:
                    if (y_kj>y_ij or (abs(y_kj-y_ij)<0.0001 and k>i)):
                        if t_kj - t_ij >= -eps_t:
                            self.updateAndRebroadcast(taskBidInfo)
                        else:
                            #print "5 "
                            self.resetAndRebroadcast(taskBidInfo)
                    else:
                        if t_kj<=t_ij:
                            self.leaveAndRebroadcast(myTaskBidInfo)
                        else:
                            #print "6 "
                            self.resetAndRebroadcast(taskBidInfo)


    def updateAndRebroadcast(self,taskBidInfo):
        '''update bundle and rebroadcast bid'''
        self.update(taskBidInfo)
        self.rebroadcast(taskBidInfo)

    def updateAndNoRebroadcast(self,taskBidInfo):
        '''update bundle but don't rebroadcast'''
        self.update(taskBidInfo)

    def leaveAndRebroadcast(self,myTaskBidInfo):
        '''only rebroadcast'''
        self.rebroadcast(myTaskBidInfo)

    def leaveAndNoRebroadcast(self):
        '''Do nothing'''
        pass

    def resetAndRebroadcast(self,taskBidInfoIn):
        '''reset your own bundle'''
        self.reset(taskBidInfoIn)
        self.rebroadcast(taskBidInfoIn)

    def updateTimeAndRebroadcast(self,myTaskBidInfo):
        '''update your time and rebroadcast'''
        myTaskBidInfo.t_timestamp = self.cbbaLocalTime
        self.rebroadcast(myTaskBidInfo)

    def default(self,myTaskBidInfo):
        '''default: leave and rebroadcast'''
        self.leaveAndRebroadcast(myTaskBidInfo)

    def update(self,taskBidInfo):
        '''
        update y,z,t bundles
        input: taskBidInfo
        '''
        j = taskBidInfo.taskID

        self.y_winbids[j] = taskBidInfo.y_winbid
        self.z_winagents[j] = taskBidInfo.z_winagent
        self.t_timestamps[j] = taskBidInfo.t_timestamp
        n_bar = None
        reset = False

        for n in range(len(self.b_bundle)):
            b_in = self.b_bundle[n]
            if reset == True:
                self.y_winbids[b_in] = 0
                self.z_winagents[b_in] = -1
                self.t_timestamps[b_in] = 0
                self.p_path.remove(b_in)
            elif b_in == j:
                n_bar = n
                reset = True
                self.p_path.remove(b_in)
        if n_bar != None:
            self.b_bundle = self.b_bundle[0:n_bar]

        if len(self.b_bundle) != len(self.p_path):
            print "issU", self.agentID, self.b_bundle, self.p_path
            print n_bar
        self.incomingBuffers[self.listenerBufferBool] = (copy.deepcopy(self.y_winbids),copy.deepcopy(self.z_winagents),copy.deepcopy(self.t_timestamps),copy.deepcopy(self.b_bundle),copy.deepcopy(self.p_path),copy.deepcopy(self.tasksList))


    def reset(self,taskBidInfo):
        '''
        reset the task
        '''
        j = taskBidInfo.taskID
        #print "j", j
        #print taskBidInfo.y_winbid

        self.y_winbids[j] = 0
        self.z_winagents[j] = -1
        self.t_timestamps[j] = 0

        n_bar = None
        reset = False


        for n in range(len(self.b_bundle)):
            b_in = self.b_bundle[n]
            if reset == True:
                self.y_winbids[b_in] = 0
                self.z_winagents[b_in] = -1
                self.t_timestamps[b_in] = 0
                self.p_path.remove(b_in)
            elif b_in == j:
                n_bar = n
                reset = True
                self.p_path.remove(b_in)
        if n_bar != None:
            self.b_bundle = self.b_bundle[0:n_bar]
        if len(self.b_bundle) != len(self.p_path):
            print "issR", self.agentID, self.b_bundle, self.p_path
        self.incomingBuffers[self.listenerBufferBool] = (copy.deepcopy(self.y_winbids),copy.deepcopy(self.z_winagents),copy.deepcopy(self.t_timestamps),copy.deepcopy(self.b_bundle),copy.deepcopy(self.p_path),copy.deepcopy(self.tasksList))

    def rebroadcast(self,taskBidInfo):
        self.outgoingBuffers[self.listenerBufferBool].append(taskBidInfo)

################################################################################
    def bundleBuilder2(self,Hz=10):
        self.incomingBuffers[not self.listenerBufferBool] = None #Force use of self
        if self.incomingBuffers[not self.listenerBufferBool] == None: #Needed for before any filling of buffer
            y_winbids = copy.deepcopy(self.y_winbids)
            z_winagents = copy.deepcopy(self.z_winagents)
            t_timestamps = copy.deepcopy(self.t_timestamps)
            b_bundle = copy.deepcopy(self.b_bundle)
            p_path = copy.deepcopy(self.p_path)
            tasksList = copy.deepcopy(self.tasksList)
        else:
            (y_winbids,z_winagents,t_timestamps,b_bundle,p_path,tasksList) = self.incomingBuffers[not self.listenerBufferBool] #TODO

        newTasksSet = set(tasksList)

        while len(b_bundle) < self.L_maxTasks and len(newTasksSet)>0:
            tStarID = 50 #HARDCODED!
            if (tStarID in tasksList) and (len(b_bundle)==self.L_maxTasks-1) and (tStarID not in b_bundle):
                    newTasksSet = set([task for task in tasksList if task.id==tStarID])
            winningTask = None #Winning task (J)
            J = None
            Jvalue = 0 #Winning task marg value
            Jn = 0 #Winning task placement in path
            #First remove tasks that are in my bundle
            for task in newTasksSet:
                # j_task = self.tasksList[j]
                j = task.id
                if j in b_bundle:
                    continue    #This could possibly done first to optimize
                [c_ij, task_n] = self.max_marginal_value(j,p_path,tasksList) #7
                if j not in range(len(y_winbids)):
                    print self.agentID,j
                    print "y", [str(i) +': %.4f'%y_winbids[i] for i in range(len(y_winbids))]
                    print "y", [str(i) +': %.4f'%self.y_winbids[i] for i in range(len(self.y_winbids))]
                if c_ij>y_winbids[j]: #Just changed it from > to >= also next
                    h_ij = c_ij
                elif abs(c_ij-y_winbids[j])<0.0001 and self.agentID > z_winagents[j]:
                    h_ij = c_ij
                else:
                    h_ij = 0
                if h_ij>=Jvalue:
                    winningTask = task
                    J = winningTask.id
                    Jvalue = h_ij
                    Jn = task_n #10
            if Jvalue == 0: #This means there are NO tasks to add
                break
            # if debugBB:
            #     print "Added", J, "y", '%.4f'%Jvalue, "OUTBID", z_winagents[J], '%.4f'%y_winbids[J]
            b_bundle.append(J)#11
            p_path.insert(Jn,J) #12
            y_winbids[J] = Jvalue #13
            i = self.agentID
            z_winagents[J] = i
            self.z_winagents[J] = i #UPDATE ARRAYS
            self.y_winbids[J] = Jvalue
            self.t_timestamps[J] = self.cbbaLocalTime
            newTasksSet.remove(task) #Delete the task from "new tasks" since its assigned


        # print "BB#"+str(self.agentID)+"P", p_path
        # print "BB#"+str(self.agentID)+"B", b_bundle

        #THIS WAS IN THE IMPLEMENTATION
        # for task in self.tasksList:
        #     if task.id in b_bundle and task.timeOut!=None:
        #         if (task.timeOut - self.cbbaLocalTime)<0.9:
        #             self.y_winbids[task.id] = 9999
        #             print "TIMEOUT!", "Executed by Agent", self.agentID

        self.p_path = p_path
        self.b_bundle = b_bundle
        for task in b_bundle:
            tb = TaskBidInfo(self.agentID,task,y_winbids[task],self.agentID,self.cbbaLocalTime)
            self.outgoingBuffers[not self.listenerBufferBool].append(tb)
        self.toBroadcastBuffer.append(self.outgoingBuffers[not self.listenerBufferBool])
        self.outgoingBuffers[not self.listenerBufferBool] = []

        # if self.agentID==0:
        #     print "BB Aft", self.p_path
        self.incomingBuffers[not self.listenerBufferBool] = (copy.deepcopy(self.y_winbids),copy.deepcopy(self.z_winagents),copy.deepcopy(self.t_timestamps),copy.deepcopy(self.b_bundle),copy.deepcopy(self.p_path),copy.deepcopy(self.tasksList))
        #self.incomingBuffers[not self.listenerBufferBool] = None
        self.listenerBufferBool = not self.listenerBufferBool #Switch buffer flag

    def max_marginal_value(self,j,p_path,tasksList,startTime=0.0):
        max_n = 0
        max_value = 0
        for n in range(len(p_path)+1):
            new_path = copy.deepcopy(p_path) #<- Is there a better way to do this?
            new_path.insert(n,j)
            S_new_value = self.pathValueF(new_path,tasksList,startTime)
            if S_new_value>max_value:
                max_n = n
                max_value = S_new_value
        return (max_value-self.pathValueF(p_path,tasksList,startTime),max_n)

    def broadcast(self,outgoingBuffersbuffer):
        print "BROADCASTING BUFFER!!"

#############################################################################3
class OCBBAManager(CBBAManager):
    '''
    Original CBBA Algorithm:  Synchronized
    Inherits most things from CBBA Manager
    '''
    def __init__(self, agentID, L_maxTasks,pathValueF,nTasks=30,nAgents=3):
        CBBAManager.__init__(self,agentID,L_maxTasks,pathValueF,nTasks,nAgents)
        self.bundleBuildRound = 0
        self.bCentral = []

        self.bundleResetNum = -1
        self.globalResetNum = -1

        self.p_path_history = []

        self.precision = 0.000001
        self.timePrecision = 0.005
        self.inf = 99999999
        self.debugBB = False
        self.c_ijLookup = dict()

        self.n_serviced = 0
        self.tasksToReset = []



    def initializeLists(self,tasksList,nRobots):
        '''Initializes z,y,t arrays needed for CBBA with information from simulator'''

        self.tasksList = tasksList
        self.nTasks = len(tasksList)
        self.z_winagents = [-1 for i in range(self.nTasks)] #Dict of tasks and the highest bidder so far (agent)
        self.y_winbids = [-self.inf for i in range(self.nTasks)]   #Dict of tasks and the highest bids so far
        self.t_timestamps = [0 for i in range(nRobots+1)] #Init time stamp 0 #HACK

    def updateTaskLists(self,task):
        '''Modify size of internal arrays with new task'''
        if task.id not in set([t.id for t in self.tasksList]):
            # print " "
            # print "Updating Lists with new task", task.id
            # print "Reset Num", self.bundleResetNum, "Reset ", self.globalResetNum, "Global Tasks"
            tic = time.time()
            # print "Y", self.y_winbids
            # print "Z", self.z_winagents
            if self.bundleResetNum>0:
                for t in self.b_bundle[-self.bundleResetNum:]:
                    self.z_winagents[t] = -1
                    self.y_winbids[t] = -self.inf
                    self.p_path.remove(t)
                self.t_timestamps = [self.cbbaLocalTime for i in range(len(self.t_timestamps))] #Init time stamp 0
                self.b_bundle = self.b_bundle[:-self.bundleResetNum]
            elif self.globalResetNum>0:
                if self.globalResetNum>=len(self.y_winbids):
                    self.b_bundle = []
                    self.p_path = []
                    self.z_winagents = [-1 for t in self.y_winbids]
                    self.y_winbids = [-self.inf for t in self.y_winbids]
                else:
                    m = sorted([y for y in self.y_winbids if y>0])[self.globalResetNum]
                    print m
                    self.b_bundle = [t for t in self.b_bundle if self.y_winbids[t]>=m]
                    self.p_path = [t for t in self.p_path if t in self.b_bundle]
                    self.z_winagents = [self.z_winagents[t] if self.y_winbids[t]>=m else -1 for t in range(len(self.z_winagents)) ]
                    self.y_winbids = [self.y_winbids[t] if self.y_winbids[t]>=m else -1 for t in range(len(self.y_winbids)) ]
                self.t_timestamps = [self.cbbaLocalTime for i in range(len(self.t_timestamps))] #Init time stamp 0
                # print "B,", self.b_bundle, "P", self.p_path
            else:
                #print "Did not specify reset conditions, no tasks reset"
                pass
            if len(self.b_bundle)!=len(self.p_path):
                print "Reset Issue, b_bundle != p_path"
            extendN = 1
            self.tasksList.append(task)
            self.z_winagents.extend([-1]*extendN)
            self.y_winbids.extend([-self.inf]*extendN)
            #self.t_timestamps.extend([0]*extendN)
            self.nTasks += 1
            toc = time.time()


    def listenerCBBA1(self,rcvdMessages,Hz=10,printTasks=False):
        '''
        Consensus rules using Choi '9 Table 1

        Input:  List of rcvd messages (type OCBBABid)

        Output: Updates self.z, self.y, self.t
        '''

        if len(rcvdMessages)>0:
            incomingCBBABidArray = rcvdMessages.pop(0) #THIS IS FROM WIFI, ASSUME NOW ITS A BID
            k = incomingCBBABidArray.sender
            z_k = incomingCBBABidArray.z_winagents
            y_k = incomingCBBABidArray.y_winbids
            t_k = incomingCBBABidArray.t_timestamps

            # print "k",k, "{:.2f}".format(time.time()%1000), ','.join([str(z) if z>-1 else "-" for z in z_k])

            i = self.agentID
            z_i = self.z_winagents
            y_i = self.y_winbids
            t_i = self.t_timestamps
            # print self.cbbaLocalTime
            # print "Ci", i
            # print "i",i,z_i
            # print "k",k,z_k
            # print "i",i, "{:.2f}".format(time.time()%1000), ','.join([str(z) if z>-1 else "-" for z in z_i])

            if len(z_k) > len(z_i):
                extendN = 1
                self.z_winagents.extend([-1]*extendN)
                self.y_winbids.extend([-self.inf]*extendN)
                #self.t_timestamps.extend([0]*extendN)
                newTask = True
            if len(z_k)>len(z_i):
                print "NOT SURE WHY Z_K > Z_I"
            self.changedTasksSet = set()
            #print str(self.agentID)+"Pre-Cons: P", self.p_path, ", B", self.b_bundle
            for j in range(len(z_k)):
                z_kj = z_k[j]
                z_ij = z_i[j]

                y_kj = y_k[j]
                y_ij = y_i[j]

                debug=False
                if debug==True:
                    print "i", i
                    print z_ij,y_ij
                    print z_kj,y_kj
                    print "t_i[zij],t_k[zij]",t_i[z_ij],t_k[z_ij]
                    print self.precision, self.timePrecision


                ##########################ACBBA TABLE 1##########################
                #This is taken straight from Choi '9 Table 1
                if z_kj==k:
                    if z_ij==i:
                        if ((y_kj-y_ij > self.precision) or (abs(y_kj-y_ij)<self.precision and k>i)):
                            self.update(j, y_kj,z_kj)
                        if abs(y_kj-y_ij)<self.precision:
                            print "Small values might mess up things"
                    elif z_ij==k:
                        self.update(j, y_kj,z_kj)
                    elif z_ij==-1:
                        self.update(j, y_kj,z_kj)
                    else: #M not i,k
                        m=z_ij
                        t_km=t_k[m]
                        t_im = t_i[m]
                        if (t_km - t_im > self.timePrecision) or ((y_kj-y_ij > self.precision) or (abs(y_kj-y_ij)<self.precision and k>m)):
                            self.update(j, y_kj,z_kj)
                elif z_kj==i:
                    if z_ij==i:
                        self.leave()
                    elif z_ij==k:
                        if debug:
                            print j,z_kj,y_kj,z_ij,y_ij
                        self.reset(j,z_ij,y_ij,z_kj,y_kj)
                    elif z_ij==-1:
                        self.leave()
                    else: #M not i,k
                        m=z_ij
                        t_km = t_k[m]
                        t_im = t_i[m]
                        if (t_km-t_im > self.timePrecision):
                            if debug:
                                print "m,t_km,t_im",m,t_km, t_im
                            self.reset(j,z_ij,y_ij,z_kj,y_kj)
                elif z_kj==-1:
                    if z_ij==i:
                        self.leave()
                    elif z_ij==k:
                        self.update(j, y_kj,z_kj)
                    elif z_ij==-1:
                        self.leave()
                    else: #M not i,k
                        m=z_ij
                        t_km=t_k[m]
                        t_im=t_i[m]
                        if (t_km - t_im > self.timePrecision):
                            self.update(j, y_kj,z_kj)
                else:
                    m = z_kj
                    t_km = t_k[m]
                    t_im = t_i[m]

                    if z_ij==i:
                        if (t_km-t_im>self.timePrecision) and ((y_kj-y_ij > self.precision) or (abs(y_kj-y_ij)<self.precision and m>i)):
                            if debug:
                                print "m,t_km,t_im",m,t_km, t_im
                                print "r1",z_kj,y_kj,z_ij,y_ij
                            self.update(j, y_kj,z_kj)
                            if abs(y_kj-y_ij)<self.precision:
                                print "Small values might mess up things"
                    elif z_ij==k:
                        if (t_km-t_im>self.timePrecision):
                            self.update(j, y_kj,z_kj)
                        else:
                            if debug:
                                print "m,t_km,t_im",m,t_km, t_im
                                print "r2",z_kj,y_kj,z_ij,y_ij
                            self.reset(j,z_ij,y_ij,z_kj,y_kj)
                    elif z_ij==m:
                        if (t_km-t_im>self.timePrecision):
                            self.update(j, y_kj,z_kj)
                    elif z_ij==-1:
                        if (t_km-t_im>self.timePrecision):
                            self.update(j, y_kj,z_kj)
                    else:
                        n = z_ij
                        t_kn = t_k[n]
                        t_in = t_i[n]
                        if (t_km-t_im>self.timePrecision) and (t_kn-t_in>self.timePrecision):
                            self.update(j, y_kj,z_kj)
                        elif (t_km - t_im > self.timePrecision) and ((y_kj-y_ij > self.precision) or (abs(y_kj-y_ij)<self.precision and m>n)):
                            self.update(j, y_kj,z_kj)
                        elif (t_kn-t_in>self.timePrecision) and (t_im-t_km>self.timePrecision):
                            self.reset(j,z_ij,y_ij,z_kj,y_kj)
            self.bundleCleanup()
            # print self.agentID, self.z_winagents
            # print " "
            #Eq 5:  Update s/t_ik
            self.t_timestamps[k] = self.cbbaLocalTime #Update receive time
            self.t_timestamps = [max(t_i[i],t_k[i]) for i in range(len(t_i))]
            # print "   "+str(self.agentID)+"Conse: Z", [str(z).rjust(2) for z in self.z_winagents]
            # print "F",i, "{:.2f}".format(time.time()%1000), ','.join([str(z) if z>-1 else "-" for z in self.z_winagents])



    #Part of the decision tree when receiving message
    def update(self,j,y_kj,z_kj):
        '''
        Parameters
        ------------
        j : int
            Task number
        y_kj : int
            Winning bid of task j
        z_kj : int
            Winning agent of task j

        Returns
        ------------
        Updates self.y_winbids, self.z_winagents
        '''

        self.y_winbids[j] = y_kj
        self.z_winagents[j] = z_kj
        if y_kj < self.inf:
            self.changedTasksSet.add(j)

    def reset(self,j,z_ij=None,y_ij=None,z_kj=None,y_kj=None,t_ij=None,t_kj=None):

        self.y_winbids[j] = -self.inf
        self.z_winagents[j] = -1
        self.changedTasksSet.add(j)

    def leave(self):
        pass

    def bundleCleanup(self):
        n_reset = None
        for n in range(len(self.b_bundle)):
            if self.b_bundle[n] in self.changedTasksSet:

                b_in = self.b_bundle[n]
                # if self.agentID==3:
                #     print "Changed Task", b_in, self.z_winagents[b_in], self.y_winbids[b_in]
                if self.z_winagents[b_in]!=self.agentID:
                    n_reset = n  #n_bar
                    break
                else:
                    print "Updated but z same?"
        if n_reset == None:
            return #Nothing changed was in my bundleCleanup
        else:
            tasksToBeReset = set(self.b_bundle[n_reset:])
        for j in self.b_bundle[n_reset+1:]:
            self.z_winagents[j] = -1
            self.y_winbids[j] = -self.inf
        self.b_bundle = self.b_bundle[:n_reset]
        self.p_path = filter(lambda j: j not in tasksToBeReset,self.p_path)

    def rebroadcast(self,taskBidInfo):
        self.outgoingBuffers[self.listenerBufferBool].append(taskBidInfo)

    def bundleBuildNewTask(self,ntstar,droppedTaskStrategy="LowestY",rebuild=False,optimistic=False):
        self.bundleBuildRound+=1
        p0 = tuple(self.p_path)
        v0 = self.pathValueF(self.p_path,self.tasksList)
        bid = 0
        lowestBid = 0
        droppedTask = None
        p = p0
        if len(p0)==self.L_maxTasks:
            if droppedTaskStrategy==None:
                return 0,p0,None
            elif droppedTaskStrategy=="LowestY":
                lowestTask_pn = np.argmin([self.y_winbids[tdum] for tdum in self.p_path])
                lowestTask = self.p_path[lowestTask_pn]
                lowestBid = self.y_winbids[lowestTask]
                droppedTask = lowestTask
                p = p0[:lowestTask_pn] + p0[lowestTask_pn+1:]
            elif droppedTaskStrategy=="Random":
                randomTask_pn = np.random.randint(0,self.L_maxTasks)
                randomTask = self.p_path[randomTask_pn]
                lowestBid = self.y_winbids[randomTask]
                droppedTask = randomTask
                p = p0[:randomTask_pn] + p0[randomTask_pn+1:]
            elif droppedTaskStrategy=="Rebuild":
                pass
            else:
                raise ValueError('Invalid Dropped Task Strategy')

        if droppedTaskStrategy=="Rebuild":
            new_bundle_build = ()
            available_tasks = set(p) | set([ntstar])
            maxp = []
            while len(available_tasks)>0 and len(new_bundle_build)<self.L_maxTasks:
                maxV = -999999
                maxT = None
                for t in available_tasks:
                    ptemp,vtemp = self.max_marginal_value(t,new_bundle_build,self.tasksList)
                    if vtemp>maxV:
                        maxV=vtemp
                        maxp=ptemp
                        maxT = t
                if maxT!=None:
                    available_tasks = available_tasks - set([maxT])
                    new_bundle_build = maxp
            p = maxp
            v = self.pathValueF(p)
            if len(available_tasks)>0:
                droppedTask = available_tasks.pop()
        else:
            p,v = self.max_marginal_value(ntstar,p,self.tasksList)

        if optimistic:
            bid = v-v0
        else:
            bid = v-v0 - self.y_winbids[droppedTask]

        if droppedTask==ntstar or bid<0:
            bid = 0
            p = p0
            droppedTask = None

        self.y_winbids[ntstar] = bid
        self.z_winagents[ntstar] = self.agentID
        return bid, p, droppedTask

    def bundleReset(self,resetStrategy="None",globalResetValue=None,b=None,p=None,y=None,z=None,t=None):
        if b==None:
            b=self.b_bundle
            p=self.p_path
            y=self.y_winbids
            z=self.z_winagents
            t=self.t_timestamps
        #We need self.nr for the local ones to compute global/nr=local reset num
        nr = self.nAgents
        if resetStrategy=="NoReset":
            newB,newP,newY,newZ,newT = self.noReset(b,p,y,z,t)
        elif resetStrategy=="LowestLocal":
            newB,newP,newY,newZ,newT = self.lowestLocalReset(globalResetValue,nr,b,p,y,z,t)
        elif resetStrategy=="HighestLocal":
            newB,newP,newY,newZ,newT = self.highestLocalReset(globalResetValue,nr,b,p,y,z,t)
        elif resetStrategy=="RandomLocal":
            newB,newP,newY,newZ,newT = self.randomLocalReset(globalResetValue,nr,b,p,y,z,t)
        elif resetStrategy=="LowestGlobal":
            newB,newP,newY,newZ,newT = self.lowestGlobalReset(globalResetValue,b,p,y,z,t)
        elif resetStrategy=="HighestGlobal":
            newB,newP,newY,newZ,newT = self.highestGlobalReset(globalResetValue,b,p,y,z,t)
        elif resetStrategy=="RandomGlobal":
            newB,newP,newY,newZ,newT = self.randomGlobalReset(globalResetValue,b,p,y,z,t)
        elif resetStrategy=="FullReset":
            newB,newP,newY,newZ,newT = self.fullReset(b,p,y,z,t)
        else:
            raise Exception, "'"+resetStrategy + "' is not an allowed value"
        print b, "->", newB
        self.b_bundle = newB
        self.p_path = newP
        self.y_winbids = newY
        self.z_winagents = newZ
        self.t_timestamps = newT
        return newB,newP,newY,newZ,newT

    def lowestLocalReset(self,nGlobalreset,nr,b,p,y,z,t):
        nLocalReset = nGlobalreset/nr
        if len(b)>=nLocalReset:
            tasksToReset = set(b[-nLocalReset:])
            newB,newP,newY,newZ,newT = self.resetTasks(tasksToReset,b,p,y,z,t)
        return newB,newP,newY,newZ,newT

    def highestLocalReset(self,nGlobalreset,nr,b,p,y,z,t):
        nLocalReset = nGlobalreset/nr
        if len(b)>=nLocalReset:
            tasksToReset = set(b[:nLocalReset])
            newB,newP,newY,newZ,newT = self.resetTasks(tasksToReset,b,p,y,z,t)
        return newB,newP,newY,newZ,newT

    def randomLocalReset(self,nGlobalreset,nr,b,p,y,z,t):
        nLocalReset = nGlobalreset/nr
        if len(b)>=nLocalReset:
            taskJToReset = random.sample(range(len(b)),nLocalReset)
            tasksToReset = set([b[j] for j in taskJToReset])
            newB,newP,newY,newZ,newT = self.resetTasks(tasksToReset,b,p,y,z,t)
        return newB,newP,newY,newZ,newT

    def lowestGlobalReset(self,nGlobalReset,b,p,y,z,t):
        lowestTasks = sorted(range(len(y)),key=lambda j:y[j])[:nGlobalReset]
        tasksToReset = set([j for j in lowestTasks if j in b])
        newB,newP,newY,newZ,newT = self.resetTasks(tasksToReset,b,p,y,z,t)
        return newB,newP,newY,newZ,newT

    def highestGlobalReset(self,nGlobalReset,b,p,y,z,t):
        highestTasks = sorted(range(len(y)),key=lambda j:-y[j])[:nGlobalReset]
        tasksToReset = set([j for j in highestTasks if j in b])
        newB,newP,newY,newZ,newT = self.resetTasks(tasksToReset,b,p,y,z,t)
        return newB,newP,newY,newZ,newT

    def randomGlobalReset(self,nGlobalReset,b,p,y,z,t):
        tasksToReset = set(random.sample(range(len(z)),nGlobalReset))
        newB,newP,newY,newZ,newT = self.resetTasks(tasksToReset,b,p,y,z,t)
        return newB,newP,newY,newZ,newT

    def fullReset(self,b,p,y,z,t):
        tasksToReset = b
        newB,newP,newY,newZ,newT = self.resetTasks(tasksToReset,b,p,y,z,t)
        return newB,newP,newY,newZ,newT

    def noReset(self,b,p,y,z,t):
        return b,p,y,z,t

    def resetTasks(self,tasksToReset,b,p,y,z,t,onlyLocalReset=True):
        #Requires a list of task id/index that are globally/locally reset
        newB=copy.deepcopy(b)
        newP=copy.deepcopy(p)
        newY=copy.deepcopy(y)
        newZ=copy.deepcopy(z)
        newT=copy.deepcopy(t)
        setB = set(newB)
        newB = [j for j in newB if j not in tasksToReset]
        newP = [j for j in p if j in newB]
        for j in tasksToReset:
            if j in b:
                newY[j] = -self.inf
                newZ[j] = -1
                newT[self.agentID]=self.cbbaLocalTime
        return newB, newP, newY, newZ, newT

    def bundleKeep(self,resetStrategy="None",globalResetValue=None,b=None,p=None,y=None,z=None,t=None):
        if b==None:
            b=self.b_bundle
            p=self.p_path
            y=self.y_winbids
            z=self.z_winagents
            t=self.t_timestamps
        # nr=8
        nKeepLocal = len(b)-(globalResetValue/self.nAgents)
        nKeepGlobal = len(y)-globalResetValue
        #We need self.nr for the local ones to compute global/nr=local reset num
        # nr = self.nAgents
        if resetStrategy=="NoReset":
            tasksToKeep = set(b)
        elif resetStrategy=="LowestLocal":
            tasksToKeep = set(b[:nKeepLocal])
        elif resetStrategy=="HighestLocal":
            tasksToKeep = set(b[-nKeepLocal:])
        elif resetStrategy=="RandomLocal":
            taskJToKeep = random.sample(range(len(b)),nKeepLocal)
            tasksToKeep = set([b[j] for j in taskJToKeep])
        elif resetStrategy=="LowestGlobal":
            highestTasks = sorted(range(len(y)),key=lambda j:-y[j])[:nKeepGlobal]
            self.tasksToReset = sorted(range(len(y)),key=lambda j:-y[j])[nKeepGlobal:]
            tasksToKeep = set([j for j in highestTasks if j in b])
        elif resetStrategy=="HighestGlobal":
            lowestTasks = sorted(range(len(y)),key=lambda j:y[j])[:nKeepGlobal]
            tasksToKeep = set([j for j in lowestTasks if j in b])
        elif resetStrategy=="RandomGlobal":
            tasksToKeep = set(random.sample(range(len(z)),nKeepGlobal))
        elif resetStrategy=="FullReset":
            tasksToKeep = set([])
        else:
            raise Exception, "'"+resetStrategy + "' is not an allowed value"

        newB,newP,newY,newZ,newT = self.keepTasks(tasksToKeep,b,p,y,z,t)
        # print b, "->", newB
        self.b_bundle = newB
        self.p_path = newP
        self.y_winbids = newY
        self.z_winagents = newZ
        self.t_timestamps = newT
        return newB,newP,newY,newZ,newT

    def keepTasks(self,tasksToKeep,b,p,y,z,t,onlyLocalReset=True):
        #Requires a list of task id/index that are globally/locally reset
        newB=copy.deepcopy(b)
        newP=copy.deepcopy(p)
        newY=copy.deepcopy(y)
        newZ=copy.deepcopy(z)
        newT=copy.deepcopy(t)
        for j in b:
            if j in tasksToKeep:
                pass
                #newY[j] = self.inf
            else:
                newY[j] = -self.inf
                newZ[j] = -1
        newB = [j for j in newB if j in tasksToKeep]
        newP = [j for j in p if j in newB]
        newT[self.agentID]=self.cbbaLocalTime
        return newB, newP, newY, newZ, newT


    def bundleBuilder2(self,bundleResetStrategy,globalResetValue,bidWarped=False):
        tic = time.time()
        self.bundleBuildRound+=1
        dynamic=False
        self.bundleKeep(bundleResetStrategy,globalResetValue)

        # self.bundleReset(bundleResetStrategy,globalResetValue)

        bundleSet = frozenset(self.b_bundle)
        currentPathT = tuple(self.p_path)
        newTasksSet = set(range(len(self.tasksList))) - bundleSet #IDS
        # bidWarped=True

        newTasksSet = newTasksSet & self.taskAbilitySet
        while len(bundleSet) < self.L_maxTasks and len(newTasksSet)>0:
            tic1 = time.time()
            J = None #Winning task (J)
            Jvalue = -self.inf #Winning task marg value
            if len(self.c_ijLookup) != 0 :
                (currentPathT,currentValue) = self.c_ijLookup[bundleSet]
            else:
                currentValue = self.pathValueF(currentPathT,self.tasksList)

            for j in newTasksSet:
                newBundle = bundleSet | frozenset([j])

                if len(self.c_ijLookup) != 0:
                    (newPathT,newValue) = self.c_ijLookup[newBundle]
                else:
                    (newPathT,newValue) = self.max_marginal_value(j,currentPathT,self.tasksList)
                # [c_ij, task_n] = self.max_marginal_value(j,self.p_path,self.tasksList) #7
                newBid = newValue - currentValue
                if bidWarped==True:
                    c_ij_bar = min([newBid]+[self.y_winbids[jt] for jt in self.b_bundle])
                    if (c_ij_bar-self.y_winbids[j])>self.precision or (abs(c_ij_bar-self.y_winbids[j])<self.precision and self.agentID > self.z_winagents[j]):
                        if (newBid-Jvalue)>self.precision:
                            J = j
                            best_cij_bar = c_ij_bar
                            Jvalue = newBid
                            bestPathSet = newBundle
                            bestPathT = newPathT #10
                else:
                    if (newBid-self.y_winbids[j])>self.precision or (abs(newBid-self.y_winbids[j])<self.precision and self.agentID > self.z_winagents[j]):
                        h_ij = newBid
                        if (h_ij-Jvalue)>self.precision:
                            J = j
                            Jvalue = h_ij
                            bestPathSet = newBundle
                            bestPathT = newPathT #10
            if J != None:
                bundleSet = bestPathSet
                currentPathT = bestPathT
                self.b_bundle.append(J)#11
                self.p_path = list(bestPathT)
                if bidWarped:
                    self.y_winbids[J] = best_cij_bar
                else:
                    self.y_winbids[J] = Jvalue #13
                self.z_winagents[J] = self.agentID
                newTasksSet.remove(J) #Delete the task from "new tasks" since its assigned
            else:
                return self.p_path
        if self.debugBB:
            print self.agentID, self.z_winagents
            # print self.agentID, "rd", self.bundleBuildRound, [(t,'%0.03f'%self.y_winbids[t]) for t in self.b_bundle]
        # print "   "+str(self.agentID)+"Build: Z", [str(z).rjust(2) for z in self.z_winagents]
        # # print "Final",timeResults
        # print "PP",self.p_path
        # print "BB", self.b_bundle
        # return self.p_path, self.b_

    def max_marginal_value(self,j,p_path,tasksList=None,startTime=0.0):
        if tasksList==None:
            tasksList = self.tasksList
        max_n = 0
        max_value = -self.inf
        max_path = p_path
        for n in range(len(p_path)+1):
            new_path = p_path[:n] + (j,) + p_path[n:]
            S_new_value = self.pathValueF(new_path,tasksList,startTime)
            if S_new_value>max_value:
                max_n = n
                max_path = new_path
                max_value = S_new_value
        return (max_path,max_value)

    def max_marginal_value_append(self,j,p_path,tasksList=None,startTime=0.0):
        #Only allow appending
        if tasksList==None:
            tasksList = self.tasksList

        new_path = p_path + (j,)
        S_new_value = self.pathValueF(new_path,tasksList,startTime)
        return (new_path,S_new_value)

    def max_marginal_value_serviced(self,j,p_path,tasksList=None,startTime=0.0):
        if tasksList==None:
            tasksList = self.tasksList
        max_n = 0
        max_value = -self.inf
        max_path = p_path
        for n in range(self.n_serviced,len(p_path)+1):
            new_path = p_path[:n] + (j,) + p_path[n:]
            S_new_value = self.pathValueF(new_path,tasksList,startTime)
            if S_new_value>max_value:
                max_n = n
                max_path = new_path
                max_value = S_new_value
        return (max_path,max_value)

    def broadcast(self,outgoingBuffersbuffer):
        print "BROADCASTING BUFFER!!"



########################################################
class Bundle:
    def __init__(self,agentID,y_winbids,z_winagents,t_timestamps):
        self.agentID = agentID
        self.y_winbids = y_winbids
        self.z_winagents = z_winagents
        self.t_timestamps = t_timestamps

    def broadcast(self,sim=None):
        print "Broadcasted"
        pass
        #sim.broadcast(bundle)

    def __str__(self):
        return "y: "+str(self.y_winbids) + '\n' + "z: "+str(self.z_winagents) + '\n' + "s:"+str(self.t_timestamps)


class TaskBidInfo:
    def __init__(self,sender,taskID,y_winbid,z_winagent,t_timestamp):
        self.sender = sender
        self.taskID = taskID
        self.y_winbid = y_winbid
        self.z_winagent = z_winagent
        self.t_timestamp = t_timestamp

    def __lt__(self,other):
        return (self.y_winbid < other.y_winbid) or (abs(self.y_winbid-other.y_winbid)<0.0001 and self.agentID < other.agentID)

    def __gt__(self,other):
        return (self.y_winbid > other.y_winbid) or (abs(self.y_winbid-other.y_winbid)<0.0001 and self.agentID > other.agentID)
