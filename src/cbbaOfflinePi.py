#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket, random, os, sys, threading, time, Queue
import multiprocessing
from udpMessage import *
import cPickle as pickle
from adhoc_networking.msg import cbbaMissionSpecs

import cbba, wifi, robot
import ocbbaRobot as ocRobot
import gc
#Title: cbbaOflinePi.py
#Author: Noam Buckman,7/2017
#Descr:  This node will broadcast and update tasks using CBBA

def cbbaNode(experimentDirectory):
    rospy.init_node('cbbaNode', anonymous=True)
    print "EN", gc.isenabled()
    gc.enable()
    print "My Time", time.time()
    rospy.on_shutdown(onShutdownSave)
    global currentMissionID, changingID
    changingID = 0

    #Step 1:  Load the Experiment's List of Mission Specs)
    testingNow = False
    if not testingNow:
        entireMissionSpecsFileName = experimentDirectory + "/experimentSpecs.p"
        entireMissionSpecs = pickle.load(open(entireMissionSpecsFileName,'rb'))
    else:
        testMission = cbbaMissionSpecs()
        testMission.missionID = 99
        testMission.activeAgents = [1,2,3,4,5,6]
        testMission.trialStartTime = time.time()+2
        testMission.experimentLength = 80
        testMission.sendRates = [25.0,25.0,25.0,25.0,25.0,25.0]
        testMission.rcvRates = [25.0,25.0,25.0,25.0,25.0,25.0]
        testMission.msgLength = 200
        testMission.taskLocationsX = [-1.955, -6.613, -2.709, -0.226, 1.593, 3.944, 5.363, 6.738, 7.093, 4.964, 3.278, 1.327, -1.556, -5.947, -8.697, -9.23, -8.653, -5.947, -3.33, -2.488, -2.443, -0.713, -0.004, 0.218, 1.194, 2.613, 2.524, 2.391, 2.303]
        testMission.taskLocationsY = [4.329, 6.353, 7.9, 6.769, 4.388, 3.555, 4.15, 1.888, -0.909, -3.588, -5.493, -6.564, -6.445, -5.195, -4.6, -1.266, 2.603, 2.781, 1.591, -0.969, -3.409, -3.052, -1.564, 1.353, 2.305, 1.888, -0.969, -2.874, -4.362]
        testMission.robotLocationsX = [0,0,-5.59252142,  4.25425277,  4.83086568,0]
        testMission.robotLocationsY = [0,0,-0.07593979, -1.02832074,  8.79310783,0]
        testMission.maxTasks = [10,10,10,10,10,10]
        entireMissionSpecs = [testMission]
    ###########################.95**5#####################3
    random.seed(agentIDint)

    #cbbaManager = = OCBBAManager(self.agentID,self.L_maxTasks,self.defaultPathValueF,self.nTasks,self.nRobots)

    experimentRunning = True
    prevEnvironmentID = -999
    for currentMissionSpecs in entireMissionSpecs:
        (currentMissionID,taskEnvironmentID,experimentLength,trialStartTime,
        tAbilitySets,robotPoses,p_centrals,b_centrals,y_central,z_central,
        newTaskArrivalTime,L,nTaskReset,taskResetYn) = currentMissionSpecs

        #Step 3)  Launch the threads with these settings!
        startTime = trialStartTime
        duration = experimentLength
        endTime = startTime + duration
        if taskEnvironmentID !=prevEnvironmentID:
            del prevEnvironmentID
            preEnvironData = pickle.load(open('/home/pi/cijLookup/env'+str(taskEnvironmentID)+'.p','rb'))
            print "Done Loading Environment Information"
            prevEnvironmentID = taskEnvironmentID
        robotPose = preEnvironData[0]
        taskList = preEnvironData[1]
        newTimeSensitiveTask = preEnvironData[2]
        cijLookup = preEnvironData[3]

        if time.time()>startTime:
            print "Too late...I missed the scheduled start time for Mission #", currentMissionID
            continue
        print "Waiting for next mission to start"
        spinRateS = rospy.Rate(100)
        while time.time()<startTime-5:
            timeRemaining = startTime - time.time()
            spinRateS.sleep()
        #delay = 1 + random.random()
        #rospy.sleep(delay)
        print "Setting up mission"
        mission_stop = threading.Event()

        #tasks = taskArray
        tasks = taskList
        L_maxTasks = L
        nTasks = len(tasks) + 1 #Add one for sensitive task!!
        nRobots = len(robotPoses) #I think max is important, NOT len

        #robotPose = robotPoses[agentIDint-1]

        r = ocRobot.OCbbaRobot(agentIDint,L_maxTasks,nTasks,nRobots) #Add mission spec

        r.pose = robotPose
        r.cbbaManager.nAgents = 6
        r.cbbaManager.initializeLists(tasks,nRobots)
        r.newTaskArrivalTime = newTaskArrivalTime
        r.newTask = newTimeSensitiveTask
        r.cbbaManager.taskResetNum = nTaskReset
        r.cbbaManager.taskResetYn = taskResetYn
        r.cbbaManager.c_ijLookup = cijLookup
        r.cbbaManager.taskAbilitySet = tAbilitySets[agentIDint-3]

        #Load Predefined CBBA Solution:
        (p_0,b_0,y_0,z_0) = p_centrals[agentIDint-3],b_centrals[agentIDint-3],y_central,z_central
        r.cbbaManager.p_path = list(p_0)
        r.cbbaManager.b_bundle = list(b_0)
        r.cbbaManager.y_winbids = y_0
        r.cbbaManager.z_winagents = z_0 #Need to cutoff the last task sinze that is T*
        r.cbbaManager.t_timestamps = [0 for i in range(8)]
        r.initialConvergenceTime=0
        r.initialResults = (0,p_0,cijLookup[frozenset(b_0)][1],cijLookup[frozenset(b_0)][1])                 # print "S BB","{:.2f}".format(time.time()%1000), ','.join([str(z) if z>-1 else "-" for z in self.cbbaManager.z_winagents])

        print p_0
        print y_0
        print z_0
        print r.cbbaManager.taskResetNum, r.cbbaManager.taskResetYn
        print r.cbbaManager.taskAbilitySet

        # print r.taskAbilityArray

        (initialResults,finalResults,msgCounter,convergeCounter) = r.realLoop(startTime,duration)

        print "I",initialResults
        print "F",finalResults
        print "Mission", currentMissionID, "Done"
        resultsFileName = experimentDirectory + '/' + str(agentIDint) + 'results' + '%02d' % currentMissionID + '.p'
        resultsFile = open(resultsFileName,'wb')
        pickle.dump((initialResults,finalResults,msgCounter,convergeCounter),resultsFile)
        resultsFile.close()
        print "GC objects"
        gc.get_objects()
        gc.collect()

        del r
        # print "Saved to", resultsFileName


    # print resultsList

    # resultsFileName = experimentDirectory + '/' + str(agentIDint) + 'results' + '%02d' % currentMissionID + '.p'
    # resultsFile = open(resultsFileName,'wb')
    # pickle.dump(resultsList,resultsFile)
    #print "Sent", len(sentMsgs), "Msgs In This Mission"
    #print "Mission End, Send CM:", sendingMaxHistory[-1][1]

def onShutdownSave():
    pass
    # print "Shutting Down!"


if __name__ == '__main__':
    try:
        ##########################################
        #Global Variables
        agentIDstr=os.environ['AGENT_NAME'][-1]
        agentIDint = int(agentIDstr)
        agentInternalValue = agentIDint


        ####CBBA GLOBAL OBJECTS
        outgoingBundlesQueue = []
        incomingBundlesQueue = []

        #SETTING UP WHERE TO SAVE DATA
        arguments = rospy.myargv(argv=sys.argv)
        resultsDirectory = '/home/pi/results/'

        print time.time()
        if len(arguments)<2:
            print "Please provide a missionSpecs File"
        else:
            experimentDirectory = resultsDirectory + arguments[1]
            cbbaNode(experimentDirectory)
    except rospy.ROSInterruptException:
        pass
