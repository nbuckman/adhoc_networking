#!/usr/bin/env python
import rospy
import socket
import sys
import os
import time
import udpMessage
from std_msgs.msg import String, Bool, Int16
from adhoc_networking.msg import maxIDMissionSpecs, MissionSentResults, MissionRcvdResults, UdpROSMsg
import cPickle as pickle

#Title: missionControlMaxID.py
#Author: Noam Buckman, 3/2017
#Descr:  This node will both send mission specifications (before the mission) and record the experiment results (after the mission)
def missionControlMaxID():
    rospy.init_node('missionControl', anonymous=True)
    rate = rospy.Rate(1) # 1hz

    #Mission control will send mission stats to other agents
    missionSpecsPub = rospy.Publisher('missionSpecs',  maxIDMissionSpecs, queue_size=10)
    missionRunningPub = rospy.Publisher('missionRunning', Bool, queue_size=1)

    missionResultsSentSub = rospy.Subscriber('missionSentResults',MissionSentResults, missionSentResultsCallback)
    missionResultsRcvdSub = rospy.Subscriber('missionRcvdResults',MissionRcvdResults, missionRcvdResultsCallback)
    agentReadySub = rospy.Subscriber('agentReady',Int16, agentReadyCallback)
    #Mission Specifications
    currentMissionID = 0
    currentActiveAgents = []
    curentSleepTime = 0
    currentMsgSize = 0
    currentNumMsgs = 0

    #Mission Specifications:

    #These Are Fixed For Now
    updateIDRates = [[2,0,0,0,0,0]] #Only agent 1 is updatings its ID
    rcvRates = [[200,200,200,200,200,200]] #Everyone is listening at 200 Hz
    activeAgents = [[1,2,3,4,5,6]] #All agents are active
    numTrials = 5
    experimentLength = 120

    #Experimentation DOF
    #msgLengths = [500,1000]
    msgLengths = [1000]
    homogSendRates = [1,2,4,6,8,10,15,20,25,30,50,75,100,125,150,200]
    #homogSendRates = [2,4,10,20,50,100,150]

    #homogUpdateIDRates = [1,2,5,10,15,20,25]
    sendRates = [[sendR]*len(activeAgents[0]) for sendR in homogSendRates]
    #updateIDRates = [[updateR]+[0]*(len(activeAgents[0])-1) for updateR in homogUpdateIDRates]

    currentMissionRunning=False
    rospy.sleep(2)  #FOR SOME REASON THIS IF VERY IMPORTANT!
    experimentRunning=True
    while experimentRunning and not rospy.is_shutdown():
        for activeAgentArray in activeAgents:
            for updateIDRateArray in updateIDRates:
                for rcvRateArray in rcvRates:
                    for trialI in range(numTrials):
                        for msgLength in msgLengths:
                            for sendRateArray in sendRates:
                                while currentMissionRunning and not rospy.is_shutdown():
                                    #Mission is still going on, don't do anything!
                                    rospy.spin()
                                #Previous mission done, move onto next one
                                agentsSentDoneSet.clear()
                                agentsRcvdDoneSet.clear()
                                agentsReadySet.clear()
                                currentMissionID += 1
                                print "Sending a new mission! Mission #", currentMissionID
                                print "Agents:", activeAgentArray, "updateIDRate (Hz)", updateIDRateArray, "rcvRate (Hz)", rcvRateArray, "sendRate: ", sendRateArray, "Msg Size", msgLength
                                newMission = maxIDMissionSpecs()
                                newMission.missionID = currentMissionID
                                newMission.activeAgents = activeAgentArray
                                newMission.changeIDRates = updateIDRateArray
                                newMission.sendRates = sendRateArray
                                newMission.rcvRates = rcvRateArray
                                newMission.msgLength = msgLength
                                newMission.experimentLength = experimentLength
                                while (agentsReadySet != set(activeAgentArray)) and not rospy.is_shutdown():
                                    missionSpecsPub.publish(newMission)
                                    rospy.sleep(1)
                                print "Starting Mission"
                                rospy.sleep(5)
                                missionRunningPub.publish(True)
                                rospy.sleep(experimentLength) #For me
                                missionRunningPub.publish(False)
                                print "Mission Ended"
                                timeOutCounter = 0
                                while (agentsSentDoneSet != set(activeAgentArray) or agentsRcvdDoneSet != set(activeAgentArray)) and not rospy.is_shutdown() and timeOutCounter<60:
                                    rospy.sleep(1) #Wait to get data from everyone
                                    timeOutCounter += 1
                                ############################################
                                print "Mission Over: Saving Data"
                                if len(sentData)!=0 and len(rcvdData)!=0:
                                    sentFileName = resultsDirectory + 'sent' + str(sendRateArray[0]) + 'Homog' +'Update'+ str(updateIDRateArray[0])+'Length'+ str(msgLength) + "T" + str(trialI) + '.p'
                                    sentFile = open(sentFileName,'wb')
                                    pickle.dump(sentData,sentFile)
                                    sentFile.close()
                                    rcvdFileName = resultsDirectory + 'rcvd' + str(sendRateArray[0]) + 'Homog' +'Update'+ str(updateIDRateArray[0])+  'Length'+ str(msgLength) + "T" + str(trialI) + '.p'
                                    rcvdFile = open(rcvdFileName,'wb')
                                    pickle.dump(rcvdData,rcvdFile)
                                    rcvdFile.close()
                                    print "Filename:", sentFileName
                                else:
                                    print "No data received"
                                rospy.sleep(2)
                                ############################################
        experimentRunning=False
        missionSpecsPub.publish(-99,[1],[1]*len(activeAgentArray),[1]*len(activeAgentArray),[1]*len(activeAgentArray),1,1) #This tells the agents to terminate the mission!
        rate.sleep()
        print "Shutting Down:  Good Night!"


def missionSentResultsCallback(data):
    #Save the results
    #currentMissionSentResults[data.agentID].append((data.missionID,data.sendRate,data.agentsReceiveRate,data.agentsSuccessRate))
    global agentsSentDoneSet, sentData
    if data.agentID not in agentsSentDoneSet:
        sentData.append(data)
        agentsSentDoneSet.add(data.agentID)
        print "Subscribed to SENT MSGS from", data.agentID

def missionRcvdResultsCallback(data):
    #Save the results
    # currentMissionRcvResults[data.agentID] = (data.missionID, data.sendRate, data.updateIDRate,data.bytesSent,data.sentMsgs)
    # agentsRcvdDoneList += data.agentID
    global agentsRcvdDoneSet, rcvdData
    if data.agentID not in agentsRcvdDoneSet:
        rcvdData.append(data)
        agentsRcvdDoneSet.add(data.agentID)
        print "Subscribed to RCVD MSGS from", data.agentID

def agentReadyCallback(data):
    global agentsReadySet
    agentRcvdID = data.data
    agentsReadySet.add(agentRcvdID)
    print "Agent", agentRcvdID, "Ready!"

if __name__ == '__main__':
    try:
        agentsSentDoneSet = set([])
        agentsRcvdDoneSet = set([])
        agentsReadySet = set([])
        currentMissionResults = dict()
        sentData = []
        rcvdData = []

        resultsDirectory = '/home/nbuckman/DropboxMIT/ACL/catkin_ws/src/adhoc_networking/results/LeaderTestMay05/'
        missionControlMaxID()
    except rospy.ROSInterruptException:
        pass
