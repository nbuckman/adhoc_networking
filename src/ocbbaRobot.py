import robot
import cbba, tasks, wifi  #These are custom modules by NoamBuckman
import udpMessage as udp
import kinematics as kn
#Standard Packages
import multiprocessing
import numpy as np
import copy
import rospy, time #These are all due to pi implementation


class OCbbaRobot(robot.CbbaRobot):
    def __init__(self,agentID,pose=kn.Pose(0,0),L_maxTasks=100,nTasks=100,nRobots=10,real=False):
        robot.CbbaRobot.__init__(self,agentID,L_maxTasks,nTasks,nRobots,real)
        self.cbbaManager = cbba.OCBBAManager(self.agentID,self.L_maxTasks,self.defaultPathValueDynamic,self.nTasks,self.nRobots)
        self.nextBBTime = 0.1 #They shouldn't start at same time
        self.onlyOdd=False
        self.cbbaRound = 0
        self.pose=pose
        self.newTaskArrivalTime = -1
        self.newTask = None
        self.nextBBMsg = None
        self.nextBBMsgTime = self.currentTime() + 99999999999999999
        self.last_z_winbids = []
        self.D = 1 #In this case

        self.initialResults = None
        self.finalResults = None
        self.insertionHeuristic = None
        self.newTaskBid = None
        self.dropped = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1] #List of tasks droppped, indexed by agent, currently HACKED
        self.t_star_time = -1

        self.bundleBuildDT = 0.03
        self.postMsgDT = 0.1
        self.postBBDt = 0.1
        self.convergeCounter=0
        self.tStarConvergence = -1

        self.msgCounter = [0 for i in range(10)]
        self.startTime = 0
        self.simTime = 0

        self.globalResetNum = None
        self.bundleResetNum = None
        self.resetStrategy = "NoReset"
        self.globalResetValue = 0
        self.allowedTauReplan = -1
        self.finishedTasks = set()
        self.originalPose = kn.Pose(None,None)
        self.nextTask = None
        self.n_serviced = 0
        self.servicedTasks = dict()

        self.globalTimeDiscount = 5.0

        self.randomSendMax = 0.01 #FAKE
        self.timeToListen = 0.01
        self.bundleComputerToSend = 0.01

        self.reactiveLocking = False  #Lock after finishing servicing
        self.noMoving = False #Don't move agent at all
        self.alwaysMoving = False #Never stop, always move on to the next task

    def realLoop(self,startTime,timeout):
        self.startTime = startTime
        mission_stop = multiprocessing.Event()
        missionResults = multiprocessing.Queue()
        main_thread = multiprocessing.Process(target=self.CBBA_Main,args=(startTime,mission_stop,missionResults))
        main_thread.daemon = True
        main_thread.start()
        print "MT", main_thread.pid

        time.sleep(timeout)
        mission_stop.set()
        time.sleep(5)
        self.wifi.sock.close()
        self.wifi.wifiThreadFlag.set()
        self.wifi.pR.terminate()
        self.wifi.pS.terminate()
        del (self.wifi.pR)
        del (self.wifi.pS)
        return missionResults.get()

    def CBBA_Main(self,startTime,mission_stop,results):
        #I MADE SOME EDITS TO self.time and time.time()
        # self.time = time.time() - self.startTime
        print "Waiting for next mission to start in", -self.currentTime(), " seconds"
        while self.currentTime()<0:
            time.sleep(0.01)
        print "MISSION STARTING"
        self.nextBBTime = self.currentTime() + .01##BB Time + Comms

        print "First BB:", self.nextBBTime
        print "New Task Arrives @", self.newTaskArrivalTime
        while not mission_stop.is_set():
            if self.currentTime()>=self.newTaskArrivalTime and self.t_star==None:
                print "New Task", self.newTask.id, "@t=", self.currentTime()
                if self.initialResults==None: #Add initial results
                    print "Did not converge before T* arrival"
                    self.initialConvergenceTime = self.newTaskArrivalTime
                    initialRawPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,0)
                    self.initialResults = (-1,copy.deepcopy(self.cbbaManager.p_path),initialRawPathValue,0)                # print "S BB","{:.2f}".format(time.time()%1000), ','.join([str(z) if z>-1 else "-" for z in self.cbbaManager.z_winagents])
                # self.initialPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,0)
                # print self.initialPathValue
                taskInfo = udp.TaskInfo()
                taskInfo.misisonID=99
                taskInfo.msgNum = self.wifi.msgNumCounter
                taskInfo.task = self.newTask
                taskInfo.sender = self.agentID
                self.taskDiscovery(taskInfo)
                self.t_star_time = self.newTaskArrivalTime
                self.newTaskArrivalTime=None
            self.loop()
            # if self.finalResults!=None:
            #     print "Finished experiment, ending early"
            #     break
        # print "S BB","{:.2f}".format(time.time()%1000), ','.join([str(z) if z>-1 else "-" for z in self.cbbaManager.z_winagents])
        if self.finalResults==None:
            finalRawPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,0)
            self.finalResults = (-1,copy.deepcopy(self.cbbaManager.p_path),finalRawPathValue,0)
        results.put((self.initialResults,self.finalResults,self.msgCounter,self.convergeCounter))

    def currentTime(self):
        #Removes offset of startTime for real experiments, and uses simulated time for simulations
        if self.real:
            return time.time()-self.startTime
        else:
            return self.simTime

    def updateRobotPose(self,deltaT):
        if self.nextTask!=None:
            # nextTaskID = self.cbbaManager.p_path[0]
            # nextTask = self.cbbaManager.tasksList[nextTaskID]
            dist = self.speed*deltaT
            heading = np.arctan2(self.nextTask.pose.Y-self.pose.Y,self.nextTask.pose.X-self.pose.X)
            self.pose.X += dist*np.cos(heading)
            self.pose.Y += dist*np.sin(heading)
            self.pose.Phi = heading


    def loop(self):
        if self.originalPose.X == None:
            # print "Setting original pose"
            self.originalPose = copy.deepcopy(self.pose)
        # if consensus and len(self.cbbaManager.p_path)>0:
        #
        #     nextTaskID = self.cbbaManager.p_path[0]
        #     nextTask = self.cbbaManager.tasksList[nextTaskID]
        #     if self.pose.dist(nextTask.pose)<=0.001:
        #         self.cbbaManager.finishedTasks[newTaskID] = self.currentTime()
        #     self.cbbaManager.y_winbids[nextTaskID] = self.cbbaManager.inf
        #     self.cbbaManager.z_winagents[nextTaskID] = self.agentID
        #     self.cbbaManager.t_timestamps[self.agentID] = self.currentTime()

        #Check if current task is done

        if self.noMoving:
            self.nextTask = None
        else:
            if self.nextTask!=None:
                if self.nextTask.pose.dist(self.pose)<=0.0001:
                    nextTaskID = self.nextTask.id
                    self.servicedTasks[nextTaskID]=self.currentTime()
                    if self.reactiveLocking:
                        self.cbbaManager.y_winbids[nextTaskID]=self.cbbaManager.inf
                        self.cbbaManager.z_winagents[nextTaskID] = self.agentID
                        self.cbbaManager.t_timestamps[self.agentID] = self.currentTime()
                        self.cbbaManager.b_bundle = [nextTaskID] + [b for b in self.cbbaManager.b_bundle if b!=nextTaskID]
                        self.cbbaManager.n_serviced+=1
                    #this used to be below

                    self.nextTask = None
            if self.nextTask==None:
                if self.convergeCounter>=(2*self.D) or self.alwaysMoving==True:
                    unservicedTasks = [j for j in self.cbbaManager.p_path if self.cbbaManager.y_winbids[j]< self.cbbaManager.inf/2.0]
                    if len(unservicedTasks)>0 and not self.reactiveLocking==True:
                        self.nextTask = self.cbbaManager.tasksList[unservicedTasks[0]]
                        if not self.reactiveLocking == True:
                            nextTaskID = self.nextTask.id
                            print "HI"
                            self.cbbaManager.y_winbids[nextTaskID]=self.cbbaManager.inf
                            self.cbbaManager.z_winagents[nextTaskID] = self.agentID
                            self.cbbaManager.t_timestamps[self.agentID] = self.currentTime()
                            self.cbbaManager.b_bundle = [nextTaskID] + [b for b in self.cbbaManager.b_bundle if b!=nextTaskID]
                            self.cbbaManager.n_serviced+=1
                else:
                    pass
                    # print "Waiting to Converge"


        rcvdMessage = self.wifi.listenRobot()

        self.cbbaManager.cbbaLocalTime = self.currentTime()
        # print "LI", "{:.2f}".format(time.time()%1000)

        if rcvdMessage != None:
            incomingMsg = udp.udpMessage() #TODO: This annoys me
            incomingInfo = incomingMsg.udpToInfoObject(rcvdMessage)
            if incomingInfo.sender!=self.agentID:
                # print "Ct",incomingInfo.sender,":", self.msgCounter[incomingInfo.sender]
                self.msgCounter[incomingInfo.sender]+=1
                if type(incomingInfo) == udp.TaskInfo:
                    incomingNewTaskInfo = incomingInfo
                    if self.t_star == None:
                        print "Calling setnew T Star"
                        self.setNewTStar(incomingNewTaskInfo)
                    else:
                        return #Already saw the task
                elif type(incomingInfo) == udp.CBBA1:
                    incomingCBBABid = incomingInfo
                    if self.t_star != None and False:
                        if (incomingCBBABid.taskID == self.t_star.id) and (self.t_star_y < incomingCBBABid.y_winbid):
                            self.t_star_y = incomingCBBABid.y_winbid
                            self.t_star_z = incomingCBBABid.z_winagent
                            self.t_star_t = incomingCBBABid.t_timestamp
                    else:
                        # print self.currentTime()
                        self.cbbaManager.listenerCBBA1([incomingCBBABid],10)


                        # print "E Li ","{:.2f}".format(time.time()%1000), ','.join([str(z) if z>-1 else "-" for z in self.cbbaManager.z_winagents])

                        tocListen = self.currentTime()

                else:
                    print "Wrong datatype", type(incomingInfo)
                                                    #print "L",realLo self.agentID, "f",incomingCBBABid.sender,self.time        # T_STAR HANDLING REMOVED
                                        # if self.t_star != None: #T Task discovered and bidding
                                        #         return
                                        #if abs(self.time - self.nextBBTime + 0.5)<0.0001:
                                        #    print "Cons",self.agentID, self.time, self.cbbaManager.z_winagents
        if (self.currentTime() - self.nextBBTime)>0.0001:
            # randomSendMax = 0.05 #REAL


            self.nextBBMsgTime = self.nextBBTime + self.bundleComputerToSend + np.random.uniform(0,self.randomSendMax)
            # print "BB Called For", "{:.2f}".format(self.nextBBTime%1000)
            self.nextBBTime = self.nextBBTime + self.bundleComputerToSend + self.randomSendMax + self.timeToListen ##BB Time + Comms
            ticBB = self.currentTime()
            # print "S BB","{:.2f}".format(time.time()%1000),

            if len(self.last_z_winbids)==0:
                self.last_z_winbids = [-1 for j in self.cbbaManager.z_winagents]


            if all([self.cbbaManager.z_winagents[j] == self.last_z_winbids[j] for j in range(len(self.last_z_winbids)) if (self.cbbaManager.z_winagents[j]<self.cbbaManager.inf/2 and self.last_z_winbids[j]<self.cbbaManager.inf/2)]):
                self.convergeCounter+=1
            else:
                self.convergeCounter=0
            self.cbbaManager.bundleBuilder2(self.resetStrategy,self.globalResetValue)

            # if self.agentID==1:
            #     print "i1j13",self.cbbaManager.z_winagents[13],'%.02f'%self.cbbaManager.y_winbids[13],self.cbbaManager.p_path,self.cbbaManager.b_bundle
            self.last_z_winbids = copy.deepcopy(self.cbbaManager.z_winagents)


            # print "BB", self.agentID, ticBB, self.cbbaManager.z_winagents
            # print "BB", "{:.2f}".format(time.time()%1000), self.cbbaManager.b_bundle
            # print "Round", self.bRound
            # print "Sending at", "{:.2f}".format(self.nextBBMsgTime%1000)
            # print "Y",['%.03f'%self.cbbaManager.y_winbids[j] for j in self.cbbaManager.b_bundle]
            tocBB = self.currentTime()
            # print "E BB","{:.2f}".format(tocBB%1000), ','.join([str(z) if z>-1 else "-" for z in self.cbbaManager.z_winagents])
            # print "BB End", tocBB
            # print "BB Dur", tocBB-ticBB

            ticCBMsg = self.currentTime()
            # print "MSG CBBA", ticCBMsg
            ocbbaMsg = udp.CBBA1()
            ocbbaMsg.sender = self.agentID
            ocbbaMsg.missionID = self.missionID
            ocbbaMsg.msgNum = self.wifi.msgNumCounter
            ocbbaMsg.z_winagents = self.cbbaManager.z_winagents
            ocbbaMsg.y_winbids = self.cbbaManager.y_winbids
            ocbbaMsg.t_timestamps = self.cbbaManager.t_timestamps
            ocbbaMsg.numTasks = self.cbbaManager.nTasks
            self.nextBBMsg = ocbbaMsg

            tocCBMsg = self.currentTime()
            # print "C",self.agentID, self.convergeCounter, self.cbbaManager.z_winagents[1],  self.cbbaManager.y_winbids[1]
            #### After initial convergence and agents bid on T*, winner updates and releases any dropped tasks
            simpleConvergence=True
            if self.convergeCounter>=(2*self.D):
                if simpleConvergence==True:
                    finalConvergenceTime = self.currentTime()
                    finalRawPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,0)
                    finalPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,finalConvergenceTime - self.t_star_time)
                    finalPath = copy.deepcopy(self.cbbaManager.p_path)
                    self.finalResults = (finalConvergenceTime,finalPath,finalRawPathValue,finalPathValue)
                elif self.t_star != None:
                    #Converged state with tStar allocated!
                    convergedWinner = self.cbbaManager.z_winagents[self.t_star.id]
                    if convergedWinner == self.agentID and self.t_star.id not in self.cbbaManager.p_path:
                        # print "Agent", self.agentID, "HighestBidder On T*", self.t_star.id
                        (y, newPath, droppedTask) = self.newTaskBid
                        if droppedTask != None:
                            self.cbbaManager.y_winbids[droppedTask] = 0
                            self.cbbaManager.z_winagents[droppedTask] = -1
                            self.cbbaManager.b_bundle = self.cbbaManager.b_bundle[:-1] #Remove last item
                            self.newTaskBid = None
                            self.finalResults = None
                        self.cbbaManager.p_path = newPath
                        self.cbbaManager.b_bundle += [self.t_star.id]
                        # print self.cbbaManager.p_path
                        # print "Drop", droppedTask
                        # print self.cbbaManager.z_winagents
                    if self.tStarConvergence == -1:
                        self.tStarConvergence = self.currentTime()
                        # print "Converged on T*: t=", self.tStarConvergence
                        self.convergeCounter = 0
                        return
                    else:
                        # Converged on team wide allocation
                        finalConvergenceTime = self.currentTime()
                        finalRawPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,0)
                        finalPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,finalConvergenceTime - self.t_star_time)
                        finalPath = copy.deepcopy(self.cbbaManager.p_path)
                        self.finalResults = (finalConvergenceTime,finalPath,finalRawPathValue,finalPathValue)
                        # print self.agentID, "Converged Final", finalConvergenceTime, finalPath
                        # print "RAW score",finalRawPathValue, "Vf", finalPathValue
                        # print "RD", self.bRound
                        # print self.msgCounter
                elif self.initialResults==None:
                    # print "INITIAL CONVERGE", self.agentID, self.cbbaManager.z_winagents
                    sortZ_y = [self.cbbaManager.z_winagents[j] for j in list(reversed(np.argsort(self.cbbaManager.y_winbids)))]
                    # print "INITIAL CONVERGE", self.agentID, sortZ_y

                    self.initialConvergenceTime = self.currentTime()
                    initialRawPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,0)
                    initialPathValue = self.cbbaManager.pathValueF(self.cbbaManager.p_path,self.cbbaManager.tasksList,self.initialConvergenceTime)
                    initialPath = copy.deepcopy(self.cbbaManager.p_path)
                    self.initialResults = (self.initialConvergenceTime,initialPath, initialRawPathValue,initialPathValue)
                    # print "Converged on Initial Allocation: Ag", self.agentID, "t=", self.initialConvergenceTime, initialPath
                    # print "RAW score",initialRawPathValue, "Vf", initialPathValue
                else:
                    self.convergeCounter=0
#                self.nextBBMsgTime = 999999999 #Don't send any more info
                # return
        if (self.currentTime()-self.nextBBMsgTime)>0.0001:
            ticMsgSend = self.currentTime()
            # print "S Send","{:.2f}".format(time.time()%1000),
            cbbaUDPMsg = self.nextBBMsg.toUDPMessage()
            for rep in range(1):
                udpMsgLength = self.wifi.broadcastRobot(cbbaUDPMsg)
            # print "MSG", self.currentTime(), self.bRound
            self.cbbaRound+=1
            self.nextBBMsgTime = self.currentTime() + 9999999999


    def taskDiscovery(self,taskInfo,resetStrategy,globalResetValue): #Agent discovers and broadcasts newtask
        self.discoveryRobot = self.agentID
        if self.allowedTauReplan>-1:
            # print " "
            # print self.agentID
            # originalNTasks = 50 #Hardcoded TODO
            # tau_bundleBuild = 0.07
            # sya = list(np.argsort([self.cbbaManager.y_winbids[j] for j in range(originalNTasks) if self.cbbaManager.y_winbids[j]<=10000])
            #
            tau_rp = self.getGlobalReplanTimes()
            # print ['%.02f'%t for t in tau_rp]
            taskb_allowedReset = [x[0] for x in enumerate(tau_rp) if x[1]<self.allowedTauReplan]
            self.tasksToReset = taskb_allowedReset
            # if self.agentID==0:
            #     print "Dropped Tasks:"
            #     for task_r in taskb_allowedReset:
            #         print  task_r, self.cbbaManager.z_winagents[task_r], '%.02f'%self.cbbaManager.y_winbids[task_r]
            if len(taskb_allowedReset)>0:
                # allowedResetN = len(tau_rp)-taskb_allowedReset[0]
                # allowed_yns = [self.cbbaManager.y_winbids[self.cbbaManager.b_bundle[j]] for j in taskb_allowedReset]
                # print allowed_yns
                maxAllowedYns = max([self.cbbaManager.y_winbids[j] for j in range(len(self.cbbaManager.y_winbids)) if (tau_rp[j]<self.allowedTauReplan and self.cbbaManager.y_winbids[j]<=self.cbbaManager.inf/2)])
                maxAllowedYns = float(maxAllowedYns)
            else:
                maxAllowedYns = None
            # print maxAllowedYns
            # print maxAllowedYns
            self.bundleResetNum = None
            self.globalResetNum = maxAllowedYns
            self.setNewTStar(taskInfo)
        elif self.bundleResetNum>-1 or self.globalResetNum>-1:

            # for b_n in reversed(range(len(self.cbbaManager.b_bundle))):
            #     p_n = self.cbbaManager.p_path.index(self.cbbaManager.b_bundle[b_n])
            #     tau_left_j = tau_left[p_n]
            #     tau_rp_j = tau_rp[b_n]
            #     tau_extra = tau_left_j-tau_rp_j
            #     if tau_extra<0:
            #         print b_n

            self.setNewTStar(taskInfo)
        else:
            self.setNewTStar_328(taskInfo)


    def setNewTStar(self,incomingNewTaskInfo):
        if type(incomingNewTaskInfo)==udp.TaskInfo:
            newTask = incomingNewTaskInfo.task
            incomingNewTaskInfo.sender=self.agentID
        else:
            newTask=incomingNewTaskInfo

        if newTask.id not in set([t.id for t in self.cbbaManager.tasksList]):
            # udpMsg = incomingNewTaskInfo.toUDPMessage()
            # for repeat in range(2):
            #     udpMsgLength = self.wifi.broadcastRobot(udpMsg)
            self.t_star = newTask
            # self.cbbaManager.updateTaskLists(newTask)

            self.cbbaManager.tasksList.append(newTask)
            self.cbbaManager.z_winagents.append(-1)
            self.cbbaManager.y_winbids.append(-self.cbbaManager.inf)
            #self.t_timestamps.extend([0]*extendN)
            self.cbbaManager.nTasks += 1
            self.convergeCounter = 0
            self.nextBBTime = self.currentTime()+.01
            # self.cbbaManager.bundleReset(resetStrategy,globalResetValue)
            # self.cbbaManager.bundleBuilder2(self.bundleResetNum,self.globalResetNum)



            #self.t_star = newTask
            #print "Set a New Task", self.t_star.id, self.t_star.reward
            self.nextBBMsgTime = self.currentTime() + 999999999

    def setNewTStar_328(self,incomingNewTaskInfo):
        newTask = incomingNewTaskInfo.task
        if newTask.id not in set([t.id for t in self.cbbaManager.tasksList]):
            incomingNewTaskInfo.sender=self.agentID
            # udpMsg = incomingNewTaskInfo.toUDPMessage()
            # for repeat in range(2):
            #     udpMsgLength = self.wifi.broadcastRobot(udpMsg)
            self.t_star = newTask
            # self.cbbaManager.updateTaskLists(newTask)
            self.cbbaManager.tasksList.append(task)
            self.cbbaManager.z_winagents.append([-1])
            self.cbbaManager.y_winbids.extend([-self.cbbaManager.inf])
            #self.t_timestamps.extend([0]*extendN)
            self.cbbaManager.nTasks += 1

            y, newPath, droppedTask = self.cbbaManager.bundleBuildNewTask(newTask.id)
            VT_star = self.cbbaManager.pathValueF((newTask.id,))
            self.insertionHeuristic = (y,VT_star)
            self.newTaskBid = (y, newPath, droppedTask)
            # print "BID", y, newPath, droppedTask
            self.nextBBMsgTime = self.currentTime() + 0.001 #RISKY
            #self.t_star = newTask
            #print "Set a New Task", self.t_star.id, self.t_star.reward
            self.nextBBTime = self.currentTime() + 0.02
            self.nextBBMsgTime = self.currentTime() + 999999999
            self.convergeCounter = 0


    def defaultPathValueStatic(self,path,tasksList=None,startTime=0.0):
            #Value function is based on distance, reward,ldiscount,ability
            if tasksList==None:
                tasksList = self.cbbaManager.tasksList
            pathValue = 0
            totalTime = startTime

            previous_pose = self.pose

            for j in path:
                task = tasksList[j]
                totalTime = totalTime + previous_pose.dist(task.pose)/self.speed
                tReward = task.reward * (task.ldiscount ** (totalTime/self.globalTimeDiscount))                #print task.id, task.timeOut, time

                pathValue += tReward

                previous_pose = task.pose
                # totalTime += task.tau_service
            return pathValue

    def defaultPathValueDynamic(self,path,tasksList=None,startTime=0.0):
            #Value function is based on distance, reward,ldiscount,ability
            if tasksList==None:
                tasksList = self.cbbaManager.tasksList
            pathValue = 0
            totalTime = startTime
            if self.originalPose.X==None:
                self.originalPose = copy.deepcopy(self.pose)
            previous_pose = self.originalPose

            for j in path:
                task = tasksList[j]
                totalTime = totalTime + previous_pose.dist(task.pose)/self.speed
                tReward = task.reward * (task.ldiscount ** (totalTime/self.globalTimeDiscount))                #print task.id, task.timeOut, time

                pathValue += tReward

                previous_pose = task.pose
                # totalTime += task.tau_service
            return pathValue

    def getGlobalReplanTimes(self,y_winbids=None,taucomm=None):
        if y_winbids==None:
            y_winbids = self.cbbaManager.y_winbids
        tau_bundleBuild = self.bundleComputerToSend + self.randomSendMax + self.timeToListen #s from hardware
        sya = list(np.argsort(y_winbids))
        tau_rp = [0 for j in range(len(sya))]
        for n in range(len(sya)):
            j = sya[n]
            tau_rp_j = ((n+1)*self.D + 2*self.D ) * tau_bundleBuild
            tau_rp[j] = tau_rp_j
        return tau_rp
