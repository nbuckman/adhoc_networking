#!/usr/bin/env python
import rospy
import socket
import sys
import os
import datetime
import udpMessage
from std_msgs.msg import String, Bool, Int16

def missionStatusCallback(data):
    print "RECEIVING A SUBSCRIPTION THINGY"
    print data

def udpListener():
    pub = rospy.Publisher('runningMaxReceived', Int16, queue_size=1)
    missionDoneSub = rospy.Subscriber("missionStatus", Bool, missionStatusCallback)

    rospy.init_node('listener', anonymous=True)
    rate = rospy.Rate(100000) # 1hz
    if 'AGENT_NAME' not in os.environ.keys():
        raise Exception("You need to source the Environment.sh")
    agentIDstr=os.environ['AGENT_NAME'][-1]
    agentIDint = int(agentIDstr)

    # S1) Setting up the UDP Socket
    UDP_IP = "10.10.3.255"
    UDP_PORT = 5005
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP,UDP_PORT)) #This allows us to listen
    print "UDP Socket Open.  IP:", UDP_IP, " Port:", UDP_PORT

    # S2) Get Mission Parameters
    # msgSize = rospy.get_param("/msgSize",800)
    # numOfMsgs = rospy.get_param("/numMsgs",500)
    # p = rospy.get_param("pSend",1.0)
    # activeAgents = rospy.get_param("/activeSenders",[1,3])
    # numOfAgents = rospy.get_param("/numAgents",9)
    # print "Mission Parameters:"
    # print "Active Agents: ", activeAgents
    # print "Message Size: %d, Total # Msgs: %d" % (msgSize,numOfMsgs)

    # # S3) Setup the Log File
    # currentTime=datetime.datetime.now()
    # output_filename = os.environ['ADHOC_ROOT'] + "/catkin_ws/src/adhoc_networking/results/test%02d%02d_%02d%02d.txt" % (currentTime.month,currentTime.day,currentTime.hour,currentTime.minute)
    # open(output_filename, "w").close()
    # print "Outputting results to:", output_filename

    # # S4) Initialize The Agent lists that will hold all the important information, indexed by sender j
    # startTimeAgents = [-1]*(numOfAgents+1)   #time of first message rcvd / start time
    # startMsgNumAgents = [0]*(numOfAgents + 1) #msg num of first message rcvd
    # timeRcvdMsgsAgents = [0]*(numOfAgents+1) #time elapsed so far
    # numRcvdMsgsAgents = [0]*(numOfAgents+1)  #number of msgs I've received so far from each person
    # finalTimeAgents = [0]*(numOfAgents+1)    #time of first termination msg / end time
    # stillSendingAgents = [False]*(numOfAgents+1) #status of whether agent j is still sending
    # for agent in activeAgents:
    #     stillSendingAgents[agent]=True
    # print "Agent Arrays Initialized", stillSendingAgents

    # S5) Initialize the udp message
    udp = udpMessage.udpMessage()
    counter=0
    missionDone=False
    print "Mission Starting"
    while not rospy.is_shutdown() and not missionDone:
        (data, addr) = sock.recvfrom(1024)
        maxIDMessage = udp.udpToInfoObject(data)
        sender = maxIDMessage.sender
        #print sender, msgNum
        if sender!=agentIDint: #Ignore if receiving from myself
            receivedRunningMax = maxIDMessage.runningMax
            pub.publish(receivedRunningMax)
            # if startTimeAgents[sender]<0: #Set start time for agent j
            #     startTimeAgents[sender]=rospy.get_time()
            #     startMsgNumAgents[sender] = msgNum
            # #timeRcvdMsgsAgents[sender]=rospy.get_time() #Save the time received msg
            # numRcvdMsgsAgents[sender]+=1 #Counter of msgs

            # #Mission Is Done. Record Results
            # if informationObject.status==9:  #Todo: Softcode the 0/9 coding
            #     print "Agent", sender, " Done"
            #     timeRcvdMsgsAgents[sender]=rospy.get_time()
            #     actualMsgSize = sys.getsizeof(data)
            #     stillSendingAgents[sender] = False

            #     #All Agents Are Done
            #     if not any(stillSendingAgents):
            #         missionDone=True
            #         # Experiment Post Processing
            #         successRateAgents = [numRcvdMsgsAgents[i]*100.0/numOfMsgs for i in range(numOfAgents+1)]
            #         dataRateAgents = [numRcvdMsgsAgents[i]*actualMsgSize*8.0/(1000000*(timeRcvdMsgsAgents[i]-startTimeAgents[i])) for i in range(numOfAgents+1)]
            #         print successRateAgents, "%% received"
            #         print dataRateAgents, "mbps"
            #         with open(output_filename, "a") as f:
            #             for i in range(numOfAgents+1):
            #                 f.write("Agent: %d, Success Rate: %d, Data Rate: %3.3f \n" % (i,successRateAgents[i],dataRateAgents[i]))
            #         sock.close()
        rate.sleep()


if __name__ == '__main__':
    try:
        udpListener()
    except rospy.ROSInterruptException:
        pass
