#!/usr/bin/env python
import cPickle as pickle
import rospy
from std_msgs.msg import String, Bool, Int16
import numpy as np
import matplotlib.pyplot as plt
import time
import argparse
from adhoc_networking.msg import maxIDMissionSpecs2, MissionSentResults, MissionRcvdResults, UdpROSMsg
import os, sys
import pysftp
import copy

piResultsDirectory = '/home/pi/results/'
noamResultsDirectory = '/home/nbuckman/DropboxMIT/ACL/results/'


if len(sys.argv)<2:
    print "Provide name of experiment directory to create"
else:
    #Make Directory with Experiment Name
    experimentName = sys.argv[1]
    missionDirectory = noamResultsDirectory + experimentName
    missionSpecsFileName = missionDirectory + "/experimentSpecs.p"
    if not os.path.exists(missionDirectory):
        os.makedirs(missionDirectory)
    # else:
    #     experimentName = experimentName + str(time.time())
    #     missionDirectory = noamResultsDirectory + experimentName
    #     missionSpecsFileName = missionDirectory + "/experimentSpecs.p"
    #     os.makedirs(missionDirectory)

currentMissionID = 0
trialStartTime = time.time() + 30
originalStart = trialStartTime
allMissionSpecs = []


updateIDRates = [[1,0,0,0,0,0]] #Only agent 1 is updatings its ID
rcvRates = [[200,200,200,200,200,200]] #Everyone is listening at 200 Hz
activeAgents = [[1,2,3,4,5,6]] #All agents are active

currentMissionRunning=False
experimentRunning=True

connectionSendRates = [1,5,20]
connectionMsgLengths = [64,1000]
connectionTestTrials = 3
connectionExperimentLength = 100
# for iTrial in range(connectionTestTrials):
#     for connectionSendRate in connectionSendRates:
#         for msgLength in connectionMsgLengths:
#             for agent in activeAgents[0]:
#                 sendRateArray = [0 for i in activeAgents[0]]
#                 sendRateArray[agent-1] = connectionSendRate
#                 currentMissionID += 1
#                 newMission = maxIDMissionSpecs2()
#                 newMission.missionID = currentMissionID
#                 newMission.activeAgents = activeAgents[0]
#                 newMission.changeIDRates = [0 for i in activeAgents[0]]
#                 newMission.sendRates = sendRateArray
#                 newMission.rcvRates = [200 for i in activeAgents[0]]
#                 newMission.msgLength = msgLength
#                 newMission.experimentLength = connectionExperimentLength
#                 newMission.trialStartTime = trialStartTime
#                 print "CT#", currentMissionID, "sendRate: ", sendRateArray, "Msg Size", msgLength
#                 allMissionSpecs.append(newMission)
#                 #Calculate the startTime of the next trial
#                 trialStartTime = trialStartTime + connectionExperimentLength + timeBetweenTrials
#


#dataRates = [25,100,500,1000,5000]  #kbps
timeBetweenTrials = 10 #s

test=False
#percentIDChange = [0.1,0.5,0.9]
#msgLengths = [1000]
#nBroadcasts = [1,2,5,10]
#sendRateSet = set([i*j for i in updateRates for j in nBroadcasts])

updateRates = [1,5]
sendRates = [1,2,5,7,10,20,25,50,75,100]
maxSend = max(sendRates)
MaxRcvRate = 4*maxSend

msgLengths = [1000] #bytes
numTrials = 3
experimentLength = 100
connectionExperimentLength = 0
connectionTimeBetweenTrials = 10
distinctMissions = 0
#Calculate the sendRate from dataRate and msgLengths
for activeAgentArray in activeAgents:
    for trialI in range(numTrials):
        for msgLength in msgLengths:
            for updateRate in updateRates:
                for sendRate in sendRates:
                    if trialI==0:
                        distinctMissions+=1
                    currentMissionID += 1
                    if test and currentMissionID>1:
                        continue
                    #print "#", currentMissionID, "updateIDRate (Hz)", updateIDRateArray, "rcvRate (Hz)", rcvRateArray, "sendRate: ", sendRateArray, "Msg Size", msgLength

                    newMission = maxIDMissionSpecs2()
                    newMission.missionID = currentMissionID
                    newMission.activeAgents = activeAgentArray

                    #sendRate = nB*updateRate
                    #sendRateSet.add(sendRate)
                    newMission.sendRates = [sendRate for i in activeAgentArray]
                    newMission.changeIDRates = [updateRate,0,0,0,0,0]
                    newMission.dataRates = [8.0*msgLength*sendRate/1000 for i in activeAgentArray]
                    newMission.rcvRates = [MaxRcvRate for i in activeAgentArray]
                    newMission.msgLength = msgLength
                    newMission.experimentLength = experimentLength
                    newMission.trialStartTime = trialStartTime
                    # newMission.trialNumber = trialI
                    #print "#", currentMissionID, "dataRate:",dataRate, " sendRate:", sendRate, " Msg Size", msgLength
                    allMissionSpecs.append(newMission)
                    #Calculate the startTime of the next trial
                    trialStartTime = trialStartTime + experimentLength + timeBetweenTrials
                    if connectionExperimentLength>0:
                        for agent in activeAgents[0]:
                            currentMissionID += 1
                            ctSendRateArray = [0 for i in activeAgents[0]]
                            ctSendRateArray[agent-1] = sendRate

                            connectionMission = copy.deepcopy(newMission)
                            connectionMission.missionID = currentMissionID
                            connectionMission.sendRates = ctSendRateArray
                            connectionMission.experimentLength = connectionExperimentLength
                            connectionMission.trialStartTime = trialStartTime
                            #
                            # newMission = maxIDMissionSpecs2()
                            # newMission.missionID = currentMissionID
                            # newMission.activeAgents = activeAgents[0]
                            # newMission.changeIDRates = [0 for i in activeAgents[0]]
                            # newMission.rcvRates = [200 for i in activeAgents[0]]
                            # newMission.msgLength = msgLength
                            #newMission.experimentLength = connectionExperimentLength
                            #newMission.trialStartTime = trialStartTime
                            print "CT#", currentMissionID, "sendRate: ", sendRate, "Msg Size", msgLength, "Agent", agent
                            allMissionSpecs.append(connectionMission)
                            #Calculate the startTime of the next trial
                            trialStartTime = trialStartTime + connectionExperimentLength + connectionTimeBetweenTrials
    # if connectionExperimentLength>0:
    #     msgLength=1000
    #     for sRate in sendRates:
    #         for agent in activeAgents[0]:
    #             ctSendRateArray = [0 for i in activeAgents[0]]
    #             ctSendRateArray[agent-1] = sRate
    #             currentMissionID += 1
    #             newMission = maxIDMissionSpecs2()
    #             newMission.missionID = currentMissionID
    #             newMission.activeAgents = activeAgents[0]
    #             newMission.changeIDRates = [0 for i in activeAgents[0]]
    #             newMission.sendRates = ctSendRateArray
    #             newMission.rcvRates = [MaxRcvRate for i in activeAgents[0]]
    #             newMission.msgLength = msgLength
    #             newMission.experimentLength = connectionExperimentLength
    #             newMission.trialStartTime = trialStartTime
    #             print "CT#", currentMissionID, "sendRate: ", ctSendRateArray, "Msg Size", msgLength
    #             allMissionSpecs.append(newMission)
    #             #Calculate the startTime of the next trial
    #             trialStartTime = trialStartTime + connectionExperimentLength + timeBetweenTrials



# resultsDirectory = '/home/nbuckman/DropboxMIT/ACL/catkin_ws/src/adhoc_networking/results/'
# missionSpecsFileName = resultsDirectory + "experimentNameTest.p"
specsFile = open(missionSpecsFileName,'wb')
pickle.dump(allMissionSpecs,specsFile)
specsFile.close()

print "DONE CREATE MISSION SPECS.  UPLOAD TO PI's AND RUN CODE!"


#IP_ADDRESSES = ['128.31.37.188','128.31.33.44','128.31.34.123','128.31.39.122','128.31.34.187','128.31.37.77']
IP_ADDRESSES = ['128.31.38.137','128.31.37.149','128.31.34.176','128.31.35.224','128.31.35.116','128.31.37.223']

#piResultsDirectory = '/home/pi/catkin_ws/src/adhoc_networking/results/'

for IP in IP_ADDRESSES:
    print "UPLOADING TO:", IP
    with pysftp.Connection(IP, username='pi', password='uavswarm') as sftp:
        with sftp.cd(piResultsDirectory):             # temporarily chdir to public
            print "MAKING DIRECTORY"
            if not sftp.isdir(piResultsDirectory + experimentName):
                sftp.mkdir(experimentName,mode=755)
            lDir = noamResultsDirectory + experimentName
            rDir = piResultsDirectory + experimentName
            print "UPLOADING"
            sftp.put_r(lDir,rDir)  # upload file to public/ on remote
            #sftp.get('remote_file')         # get a remote file

print "UPLOADED ALL THE MISSION SPECS"
print "MISSION WILL START AT", time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(originalStart))
totalDuration = (allMissionSpecs[-1].trialStartTime - allMissionSpecs[0].trialStartTime ) #s
print totalDuration, "s"
print "TOTAL EXPERIMENTS", currentMissionID
print "TOTAL APPROX TIME OF EXPERIMENTS:", int(totalDuration/3600), "H", int((totalDuration - 3600*int(totalDuration/3600))/60), "M"
print "Experiment Located:", missionDirectory
print "Starting in", allMissionSpecs[0].trialStartTime - time.time(), "seconds"
print "RUN ON EACH PI"
print "roslaunch adhoc_networking offlineleaderElection.launch expDir:=" + experimentName
