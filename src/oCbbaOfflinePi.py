#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket, random, os, sys, threading, time
from udpMessage import *
import cPickle as pickle
from adhoc_networking.msg import cbbaMissionSpecs
from helpObjectCBBA import CBBAManager, Bundle, Pose, Task
#Title: oCbbaOflinePi.py
#Author: Noam Buckman,12/2017
#Descr:  This node will run a sync version of cbba (original CBBA)

def cbbaNode(experimentDirectory):
    rospy.init_node('oCbbaNode', anonymous=True)
    print "My Time", time.time()
    rospy.on_shutdown(onShutdownSave)
    global currentMissionID, changingID
    changingID = 0

    #Step 1:  Load the Experiment's List of Mission Specs)
    entireMissionSpecsFileName = experimentDirectory + "/experimentSpecs.p"
    entireMissionSpecs = pickle.load(open(entireMissionSpecsFileName,'rb'))
    ################################################3
    random.seed(agentIDint)

    #Step 3)  Launch the threads with these settings!
    t = threading.Thread(target=Sending,args=[entireMissionSpecs])
    t.daemon = True
    t.start()
    t2 = threading.Thread(target=Receiving,args=[entireMissionSpecs])
    t2.daemon = True
    t2.start()
    t3 = threading.Thread(target=IDChanging,args=[entireMissionSpecs])
    t3.daemon = True
    t3.start()
    ######################################################
    while experimentRunning and not rospy.is_shutdown():
        rospy.spin()

def Sending(entireMissionSpecs):
    experimentRunning = True
    while experimentRunning and not rospy.is_shutdown():
        for currentMissionSpecs in entireMissionSpecs:
            #1)  Unpack the mission specs
            currentMissionID = currentMissionSpecs.missionID
            sendHz = currentMissionSpecs.sendRates[agentIDint-1] # 10hz
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime
            duration = currentMissionSpecs.experimentLength
            endTime = startTime + duration

            #2)  Set our send rate
            if sendHz>0.000001:
                sendRate = rospy.Rate(sendHz)
            spinRateS = rospy.Rate(100)
            sentMsgs = []
            sentBytes = []

            sendingMaxHistory = []
            msgNum = 0

            #THIS IS JUST FOR TESTING!!!
            testing=True
            if testing==True:
                startTime = time.time()+2
                endTime = startTime + 20

            #3) Wait for start time
            if time.time()>startTime:
                print "Too late...I missed the scheduled start time for Mission #", currentMissionID
                continue
            print "Waiting for next mission to start"
            while time.time()<startTime:
                timeRemaining = startTime - time.time()
                spinRateS.sleep()
            delay = 1 + random.random()
            rospy.sleep(delay)

            #4)  Mission will start now
            global outgoingBundlesQueue
            print "Mission Start"


            #MAIN SENDING SCRIPT RUN ON THE PI
            while time.time()<endTime:
                if sendHz<0.00001:
                    spinRateS.sleep()
                    #sendRate = rospy.Rate(1)
                else:
                    if len(outgoingBundlesQueue)>0:
                        cbbaOutgoingMsg = outgoingBundlesQueue.pop()
                        try:
                            sent = cbbaOutgoingMsg.sendOnUDP(sock,UDP_IP,UDP_PORT)
                            print sent
                        except socket.error:
                            pass
                        msgNum +=1
                    sendRate.sleep()
            #5) Mission is Done, Save the Data and Clear Data
            # sentResults = MissionSentResults()
            # sentResults.missionID = currentMissionID
            # sentResults.agentID = agentIDint
            # sentResults.missionSpecs = currentMissionSpecs
            # sentResults.sentMsgs = sentMsgs
            # sentResults.sentBytes = sentBytes
            # sentFileName = experimentDirectory + '/' + str(agentIDint) + 'sent' + '%02d' % currentMissionID + '.p'
            # sentFile = open(sentFileName,'wb')
            # pickle.dump(sentResults,sentFile)
            # print "Saving ", len(sentMsgs), "SENT msgs to: ", sentFileName
            sentMsgs = []
            sentBytes = []
            msgNum = 0
        print "EXPERIMENT DONE!"
        print "Results Found:" + experimentDirectory

        global experimentRunning
        experimentRunning=False


def Receiving(entireMissionSpecs):
    neighbors = {
        1:{2,3},
        2:{1,3},
        3:{1,2,4,5},
        4:{1,2,3,5},
        5:{3,4,6},
        6:{4,5,6}
    }
    if simNeighbors and not adhoc:
        myNeighbors = neighbors[agentIDint]
    else:
        myNeighbors = {1,2,3,4,5,6} - {agentIDint}
    global currentRunningMax
    experimentRunning = True
    while experimentRunning and not rospy.is_shutdown():
        for currentMissionSpecs in entireMissionSpecs:
            #1)  Unpack the mission specs
            currentMissionID = currentMissionSpecs.missionID
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added
            endTime = startTime + duration

            #2)  Set variables
            receiveRate = rospy.Rate(rcvHz)
            spinRateR = rospy.Rate(1000)
            rcvdMsgs = []
            rcvdBytes = []

            receivingMaxHistory = []
            currentMaxHistory = []

            currentRunningMax = 0

            #THIS IS JUST FOR TESTING!!!
            testing=True
            if testing==True:
                startTime = time.time()+2
                endTime = startTime + 10
            print startTime
            if time.time()>startTime:
                print "RCVD: Too late...I missed the scheduled start time"
                continue

            #3) Wait for start time  (add the randomizer here)
            lastMessage = 0
            lastSocketError = 0
            while time.time()<startTime:
                timeRemaining = startTime-time.time()
                try:
                    (data, addr) = sock.recvfrom(msgLength)
                    lastMessage = time.time()
                except socket.error:
                    lastSocketError = time.time()
                currentRunningMax = 0
                spinRateR.sleep()

            currentRunningMax = 0
            receivedRunningMax = 0

            #4)  Mission will start now
            if lastSocketError - lastMessage < 3:
                print "Warning: Last Clean Buffer vs. Rcvd=", lastSocketError - lastMessage, "s"
            while time.time()<endTime:
                try:
                    (data, addr) = sock.recvfrom(msgLength)
                    udp = udpMessage()
                    cbbaBundleInfo = udp.udpToInfoObject(data)

                    cbbaBundleIncoming = Bundle(cbbaBundleInfo.sender,cbbaBundleInfo.y_winbids,cbbaBundleInfo.z_winagents,cbbaBundleInfo.s_timestamps)
                    print "RCV from", cbbaBundleIncoming.agentID
                    print "y:", cbbaBundleIncoming.y_winbids
                    print "z", cbbaBundleIncoming.z_winagents
                    sender = cbbaBundleInfo.sender
                    # if sender in myNeighbors: #Ignore if not in neighbors
                    tau_r = time.time() #This is the time I received the msg
                    global incomingBundlesQueue
                    incomingBundlesQueue.append(cbbaBundleIncoming)
                    print "Incoming queue length", len(incomingBundlesQueue)
                except socket.error:
                    pass
                receiveRate.sleep()

            #5)  Save results to some mission results
            #Reset local/global variables
        experimentRunning = False


def newListener(entireMissionSpecs):
    neighbors = {
        1:{2,3},
        2:{1,3},
        3:{1,2,4,5},
        4:{1,2,3,5},
        5:{3,4,6},
        6:{4,5,6}
    }
    if simNeighbors and not adhoc:
        myNeighbors = neighbors[agentIDint]
    else:
        myNeighbors = {1,2,3,4,5,6} - {agentIDint}
    global currentRunningMax
    global msgStack
    experimentRunning = True
    while experimentRunning and not rospy.is_shutdown():
        for currentMissionSpecs in entireMissionSpecs:
            #1)  Unpack the mission specs
            currentMissionID = currentMissionSpecs.missionID
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added
            endTime = startTime + duration

            #2)  Set variables
            receiveRate = rospy.Rate(rcvHz)
            spinRateR = rospy.Rate(1000)
            rcvdMsgs = []
            rcvdBytes = []

            receivingMaxHistory = []
            currentMaxHistory = []

            currentRunningMax = 0

            #THIS IS JUST FOR TESTING!!!
            testing=True
            if testing==True:
                startTime = time.time()+2
                endTime = startTime + 10
            print startTime
            if time.time()>startTime:
                print "RCVD: Too late...I missed the scheduled start time"
                continue

            #3) Wait for start time  (add the randomizer here)
            lastMessage = 0
            lastSocketError = 0
            while time.time()<startTime:
                (data, addr) = sock.recvfrom(msgLength)
                msgStack.append(data)



            currentRunningMax = 0
            receivedRunningMax = 0

            #4)  Mission will start now
            if lastSocketError - lastMessage < 3:
                print "Warning: Last Clean Buffer vs. Rcvd=", lastSocketError - lastMessage, "s"
            while time.time()<endTime:
                try:
                    incomingB = incomingBundlesQueue.pop()
                    (updated_z_array,updated_y_array,updated_t_array) = cbba.listenerCBBA1([incomingB])
                    #update a global list taht can be used in bunble builder
                except socket.error:
                    pass

            #5)  Save results to some mission results
            #Reset local/global variables
        experimentRunning = False


def bundleBuild(entireMissionSpecs):
    while True:
        (b,p,z,y,t) = cbba.bundleBuilder2(b,p,z,y,t,taskList)
        udpZYT = broadcast(z,y,t)
        toSendArray.append(udpZYT)


#TODO Change the name of IDCHanging
def IDChanging(entireMissionSpecs):
    experimentRunning = True
    while experimentRunning and not rospy.is_shutdown():
        for currentMissionSpecs in entireMissionSpecs:
            #1)  Unpack the mission specs
            currentMissionID = currentMissionSpecs.missionID
            activeAgents = currentMissionSpecs.activeAgents
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added
            endTime = startTime + duration


            changeIDHz = 10
            if changeIDHz<=0.00001:
                changeIDRate = rospy.Rate(50)
            else:
                changeIDRate = rospy.Rate(changeIDHz)

            #2)  Let's setup our cbbaManager
            tasks = []
            for i in range(len(currentMissionSpecs.taskLocationsX)):
                t_reward = 1
                t_pose = Pose(currentMissionSpecs.taskLocationsX[i],currentMissionSpecs.taskLocationsY[i])
                t_id = i
                tasks += [Task(t_reward,t_pose,t_id)]
            print "Tasks:", tasks

            L_maxTasks = currentMissionSpecs.maxTasks[agentIDint-1]

            robotPose = Pose()
            robotPose.X= currentMissionSpecs.robotLocationsX[agentIDint-1]
            robotPose.Y= currentMissionSpecs.robotLocationsY[agentIDint-1]


            cbbaManager = CBBAManager(agentIDint,robotPose,L_maxTasks) #THE CONSTRUCTER NEEDS TO BE CHANGED
            cbbaManager.nAgents = len(currentMissionSpecs.activeAgents)
            cbbaManager.initializeLists(tasks)

            #THIS IS JUST FOR TESTING!!!
            testing=False
            if testing==True:
                startTime = time.time()+2
                endTime = startTime + 10

            #2)  Wait for mission to start
            if time.time()>startTime:
                print "Too late...I missed the scheduled start time"
                continue
            while time.time()<startTime:
                rospy.sleep(0.02)
            roundNumber = 0
            converged = False
            convergeCounter = 0
            #4)  Mission will start now
            while time.time()<endTime and not converged:
                phase2=False
                roundNumber+=1
                cbbaManager.phase1BuildBundle(None)
                print "P1P", cbbaManager.p_path
                p1Z = cbbaManager.z_winagents
                global incomingBundlesQueue
                while len(incomingBundlesQueue)>0:
                    bundle = incomingBundlesQueue.pop()
                    cbbaManager.s_timestamps = cbbaManager.update_time_array(bundle)
                    print "Entering Phase II"
                    phase2 = True
                    cbbaManager.phase2ConflictResolution(bundle)
                    p2Z = cbbaManager.z_winagents
                if phase2==True:
                    if {0} in set(p2Z):
                        convergeCounter = 0
                    else:
                        for i in range(len(p2Z)):
                            if p1Z[i] != p2Z[i]: #Efficient way of comparing two lists
                                convergeCounter = 0
                                continue
                        convergeCounter +=1
                if convergeCounter >= 5:
                    converged = True
                cbbaUDPMsg = cbbaManager.makeCBBAMsg(agentIDint,currentMissionID)
                global outgoingBundlesQueue
                outgoingBundlesQueue.append(cbbaUDPMsg)
                changeIDRate.sleep()
            print "Converged after ", roundNumber-convergeCounter, "rounds"
            print "Total Time=", time.time()-startTime
            #5)  Save results to some mission results
            #Reset local/global variables
        experimentRunning = False

def onShutdownSave():
    print "Shutting Down!"


if __name__ == '__main__':
    try:
        ##########################################
        #Global Variables
        agentIDstr=os.environ['AGENT_NAME'][-1]
        agentIDint = int(agentIDstr)
        agentInternalValue = agentIDint


        ####CBBA GLOBAL OBJECTS
        outgoingBundlesQueue = []
        incomingBundlesQueue = []

        #################################
        #Setting up the UDP Socket
        adhoc=True
        simNeighbors = False  #Set this true if you want to enforce neighbors
        if adhoc:
            UDP_IP = "10.10.3.255"
        else:
            UDP_IP = "128.31.39.255"
        UDP_PORT = 5005
        sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
        sock.bind((UDP_IP,UDP_PORT))
        sock.setblocking(0) #Don't block
        print "UDP target IP:", UDP_IP, "port:", UDP_PORT
        ####################################

        #SETTING UP WHERE TO SAVE DATA
        arguments = rospy.myargv(argv=sys.argv)
        resultsDirectory = '/home/pi/results/'

        if len(arguments)<2:
            print "Please provide a missionSpecs File"
        else:
            experimentDirectory = resultsDirectory + arguments[1]
            cbbaNode(experimentDirectory)
    except rospy.ROSInterruptException:
        pass
