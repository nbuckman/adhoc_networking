#!/usr/bin/env python
import cPickle as pickle
import time, pysftp, copy, os, sys, argparse
#from std_msgs.msg import String, Bool, Int16
import numpy as np
import cbba
import kinematics as kn
import random
from helperMethods import *
###################3
piResultsDirectory = '/home/pi/results/'
noamResultsDirectory = '/home/nbuckman/DropboxMIT/ACL/results/'

if len(sys.argv)<2:
    print "Provide name of experiment directory to create"
    raise Exception
else:
    #Make Directory with Experiment Name
    experimentName = sys.argv[1]
    missionDirectory = noamResultsDirectory + experimentName
    missionSpecsFileName = missionDirectory + "/experimentSpecs.p"
    masterSpecsFileName = missionDirectory + "/masterSpecs.p"
    predictedResultsFileName = missionDirectory + "/predictedResults.p"
    if not os.path.exists(missionDirectory):
        os.makedirs(missionDirectory)
    else:
        print missionDirectory, "already exists"
#################################################################3
#Experiment Specifications
numTrials = 1
timeBetweenTrials = 10 #s
cijLoadTime = 60
#DO NOT EDIT
test=False
distinctMissions = 0
trialStartTime = time.time() + 140 + cijLoadTime
originalStart = trialStartTime
currentMissionID = 0
taskEnvironmentID = 0
allMissionSpecs = []
masterSpecs = []
predictedResults = []
######################################################################
var1ranges = [1] #R_all
var2ranges = [20] #R_t
var3ranges = [0.95] #l_all
var4ranges = [0.85] #l_T
var5ranges = [1] #rho
var6ranges = [0,1,2] #lbuffer
var7ranges = [0.5] #alltaskTauS
var8ranges = [0.5] #newTaskTauS
yNRanges = [0,1,2,3,5,10,19]
taskEnvironmentIDs = [0,0]
tAbilityTrials = 1
#########################

trialStartTime = time.time() + len(taskEnvironmentIDs)*10 + cijLoadTime

for taskEnvironmentID in taskEnvironmentIDs:
    for var1 in var1ranges:
        for var2 in var2ranges:
            for var3 in var3ranges:
                for var4 in var4ranges:
                    for var5 in var5ranges:
                        for var6 in var6ranges:
                            for var7 in var7ranges:
                                for var8 in var8ranges:
                            # currentMissionID += 1
                                    experimentLength = 20
                                    trialStartTime = trialStartTime

                                #     nt = 19
                                #     nr = 4
                                #     (allTasksR,allTasksL) = (var1,var3)
                                # #         (newTaskX,newTaskY) = (3,3)CONVER
                                #     (newTaskR,newTaskL) = (var2,var4)
                                #     allTaskTauS = var7
                                #     newTaskTauS =  var8
                                #
                                #     #Init robots
                                #     activeAgents = [1,2,3,4,5,6]
                                #     robotPoses = [kn.Pose(0,0),kn.Pose(0,0),kn.Pose(-2.5,0.5),kn.Pose(1.2,-2),kn.Pose(-7.5,-1.4),kn.Pose(4.1,2.5),]
                                #     # nr = len(robotPoses)
                                #
                                #
                                #     (allTasksR,allTasksL) = (var1,var3)
                                # #         (newTaskX,newTaskY) = (3,3)
                                    pAbility = var5
                                    ltBuffer = var6
                                #     taskPoses = [kn.Pose(np.random.uniform(-10,10),np.random.uniform(-10,10)) for t in range(nt)]
                                #     taskArray = [cbba.TimeSensitiveTask(allTasksR,taskPoses[i],allTasksL,i,allTaskTauS) for i in range(len(taskPoses))]
                                #
                                #     newTimeSensitiveTask = cbba.TimeSensitiveTask(newTaskR,kn.Pose(np.random.uniform(-10,10),np.random.uniform(-10,10)),newTaskL,nt,allTaskTauS)
                                #     nt = nt +1
                                #
                                #     nr = 4

                                    tL,tT,robotPoses,c_ijList = getAllCij(taskEnvironmentID,[3,4,5,6])

                                    maximumBundleSizeAllowed = max([len(c_ijList[0][c][0]) for c in c_ijList[0].keys()])
                                    nt = len(tL)+1
                                    nr = len(robotPoses)
                                    L = int(np.ceil((nt)/nr)) + ltBuffer
                                    if L>maximumBundleSizeAllowed:
                                        print "L (",L,") is larger than allowed Lmax (", maximumBundleSizeAllowed, ")!"
                                        continue

                                    for tTrial in range(tAbilityTrials):
                                        if tTrial>len(savedTaskAbility[pAbility]):
                                            print "ERROR:  Need more trials for pAbility>0"
                                        if pAbility==1:
                                            tAbilitySets = savedTaskAbility[pAbility][0]
                                        else:
                                            tAbilitySets = savedTaskAbility[pAbility][tTrial]
                                        teamValue,p_centrals,b_centrals,y_central,z_central = SGA(L,-1,nt,c_ijList,tAbilitySets)

                                        print tAbilitySets
                                        print "EXP INITIAL"
                                        print p_centrals
                                        # print z_central
                                        # print ['%0.03f'%y for y in y_central]

                                        newTaskArrivalTime = 1
                                        nTaskReset = -1

                                        for taskResetYn in yNRanges:
                                            currentMissionID+=1
                                            (teamValueY,p_centralsY,b_centralsY,y_centralY,z_centralY) = SGA(L,taskResetYn,nt,c_ijList,tAbilitySets)
                                            print "EXP", currentMissionID, "Yn",taskResetYn
                                            print p_centralsY
                                            allMissionSpecs.append((currentMissionID,taskEnvironmentID,experimentLength,trialStartTime,tAbilitySets,robotPoses,p_centrals,b_centrals,y_central,z_central,newTaskArrivalTime,L,nTaskReset,taskResetYn))
                                            masterSpecs.append((currentMissionID,taskEnvironmentID,pAbility,tTrial,taskResetYn))
                                            predictedResults.append((teamValueY,p_centralsY,b_centralsY,y_centralY,z_centralY))
                                            trialStartTime = trialStartTime + experimentLength + timeBetweenTrials
                                        #taskEnvironmentID += 1
                                    trialStartTime = trialStartTime + cijLoadTime
specsFile = open(missionSpecsFileName,'wb')
pickle.dump(allMissionSpecs,specsFile)
specsFile.close()

IP_ADDRESSES = ['18.111.110.22','18.111.111.43','18.111.122.107','18.111.123.158',]

# IP_ADDRESSES = ['18.111.99.69']

#IP_ADDRESSES = ['128.31.38.137','128.31.37.149','128.31.34.176']
#piResultsDirectory = '/home/pi/catkin_ws/src/adhoc_networking/results/'
totalDuration = (allMissionSpecs[-1][3] - allMissionSpecs[0][3] + experimentLength) #s
print totalDuration, "s"
print "TOTAL #TRIALS",numTrials,"# EXPERIMENTS", currentMissionID
print "TOTAL APPROX TIME OF EXPERIMENTS:", int(totalDuration/3600), "H", int((totalDuration - 3600*int(totalDuration/3600))/60), "M"

for IP in IP_ADDRESSES:
    print "UPLOADING TO:", IP
    with pysftp.Connection(IP, username='pi', password='uavswarm') as sftp:
        with sftp.cd(piResultsDirectory):             # temporarily chdir to public
            if not sftp.isdir(piResultsDirectory + experimentName):
                sftp.mkdir(experimentName,mode=755)
            lDir = noamResultsDirectory + experimentName
            rDir = piResultsDirectory + experimentName
            sftp.put_r(lDir,rDir)  # upload file to public/ on remote

# print "MISSION WILL START AT", time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(originalStart))

print "Mission End", time.ctime(int(allMissionSpecs[-1][3] +experimentLength))
print "UPLOADED ALL, THE MISSION SPECS"
print "TOTAL #TRIALS",numTrials,"# EXPERIMENTS", currentMissionID
print "TOTAL APPROX TIME OF EXPERIMENTS:", int(totalDuration/3600), "H", int((totalDuration - 3600*int(totalDuration/3600))/60), "M"
print "Experiment Located:", missionDirectory
print "Starting in", allMissionSpecs[0][3] - time.time(), "seconds"
print "RUN ON EACH PI"
print "roslaunch adhoc_networking offlineCBBA.launch expDir:=" + experimentName

masterSpecsFile = open(masterSpecsFileName,'wb')
pickle.dump(masterSpecs,masterSpecsFile)
masterSpecsFile.close()

prF = open(predictedResultsFileName,'wb')
pickle.dump(predictedResults,prF)
prF.close()
