import kinematics as kn
import numpy as np

class Task:
    def __init__(self,reward=-1,pose=None,taskID=-1,timeOut=None,r_id=0.00,t_D=0.00,ldiscount=0.95,tau_service=0):
        self.reward = reward
        if pose is None:
            self.pose = kn.Pose(None,None)
        else:
            self.pose = pose
        self.id = taskID
        self.timeOut = timeOut
        self.r_id = r_id
        self.t_D = t_D
        self.ldiscount=ldiscount
        self.tau_service = tau_service

    def __str__(self):
        return ("Task #" + str(self.id) + " Reward:" + str(self.reward) + '\n' +
                "Pose:" + "(" + str(self.pose.X) + "," + str(self.pose.Y) + ")" + "\n")

    def __hash__(self):
        return hash(self.id)

    def __repr__(self):
        return str(self.id)

class TimeSensitiveTask(Task):
    def __init__(self,reward=-1,pose=None,ldiscount=0.96,taskID=-1,tau_service=0):
        Task.__init__(self,reward,pose,taskID,None,0,0,ldiscount,tau_service)


####### Here are tools for creating tasks
def createRandomTasks(self,nTasks,xRange,yRange,rRange=[1,1],lRange=[0.95,0.95],tauSRange=[0.01,0.5]):
    assert type(xRange) is list, "Not list"
    assert type(yRange) is list, "Not list"
    assert type(rRange) is list, "Not list"
    assert type(lRange) is list, "Not list"
    assert type(lRange) is list, "Not list"
    newTaskArray = []
    for i in range(self.nTasks):
        x = np.random.uniform(xRange[0],xRange[1])
        y = np.random.uniform(yRange[0],yRange[1])
        if type(lRange) is list:
            l = np.random.uniform(lRange[0],lRange[1])
        else:
            l = lRange
        if type(rRange) is list:
            r = np.random.uniform(rRange[0],rRange[1])
        else:
            r = rRange
        if type(tauSRange) is list:
            tauS = np.random.uniform(tauSRange[0],tauSRange[1])
        else:
            tauS = tauSRange
        newTaskArray += [TimeSensitiveTask(r,kn.Pose(x,y),l,i,tauS)]
    return newTaskArray


def createAbilityArraysUniRand(nr,nt,upper):
    abilityMatrix = {}
    #this is the initial allocation
    for i in range(nr):
        abilityArray = [np.random.uniform(0,upper) for j in range(nt)]
        abilityMatrix[i]=abilityArray
        #r.taskAbilityArray = abilityArray
    return abilityMatrix

    #
    # def createUniTasks(self,n=5,halfcircle=False):
    #     for i in range(n):
    #         t_reward = 1
    #         theta = i*2*np.pi/n #Default on unit radius equally spaced apart
    #         if halfcircle==True:
    #             theta = theta / 2.0
    #         R= 1.5
    #         [x,y] = [np.cos(theta)*R,np.sin(theta)*R]
    #         t_pose = kn.Pose(x,y)
    #         t_id = i
    #         self.tasksArray += [cbba.Task(t_reward,t_pose,t_id)]
    #         for robot in self.totacentralSGANewTasklRobotArray:
    #             robot.cbbaManager.initializeLists(copy.deepcopy(self.tasksArray))
